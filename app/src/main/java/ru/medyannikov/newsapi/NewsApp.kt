package ru.medyannikov.newsapi

import android.app.Activity
import android.app.Application
import android.content.Context
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.di.injector.AppInjector
import javax.inject.Inject

class NewsApp : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    override fun attachBaseContext(base: Context) {
        LocaleManager.setupLanguage(base)
        super.attachBaseContext(LocaleManager.wrap(base))
    }

    override fun onCreate() {
        super.onCreate()
        _instance = this

        AndroidThreeTen.init(this)
        AppInjector.init(this)
    }

    companion object {

        private lateinit var _instance: NewsApp

        fun getInstance(): NewsApp = _instance
    }
}