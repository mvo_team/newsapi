package ru.medyannikov.newsapi.filter.core.domain.provider.language.impl

import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.filter.core.domain.model.LanguageModel
import ru.medyannikov.newsapi.filter.core.domain.provider.language.AvailableLanguageProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AvailableLanguageProviderImpl
    @Inject constructor(private val localeManager: LocaleManager): AvailableLanguageProvider {

    private val languages: List<LanguageModel> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) { generateLanguages() }

    override fun getAvailableLanguages(): List<LanguageModel> = languages

    private fun generateLanguages() : List<LanguageModel> {
        return listOf(
                LanguageModel(localeManager.getString(R.string.language_ar), "ar"),
                LanguageModel(localeManager.getString(R.string.language_de), "de"),
                LanguageModel(localeManager.getString(R.string.language_en), "en"),
                LanguageModel(localeManager.getString(R.string.language_es), "es"),
                LanguageModel(localeManager.getString(R.string.language_fr), "fr"),
                LanguageModel(localeManager.getString(R.string.language_he), "he"),
                LanguageModel(localeManager.getString(R.string.language_it), "it"),
                LanguageModel(localeManager.getString(R.string.language_nl), "nl"),
                LanguageModel(localeManager.getString(R.string.language_no), "no"),
                LanguageModel(localeManager.getString(R.string.language_pt), "pt"),
                LanguageModel(localeManager.getString(R.string.language_ru), "ru"),
                LanguageModel(localeManager.getString(R.string.language_se), "se"),
                LanguageModel(localeManager.getString(R.string.language_uk), "uk"),
                LanguageModel(localeManager.getString(R.string.language_zh), "zh")
        )
    }
}