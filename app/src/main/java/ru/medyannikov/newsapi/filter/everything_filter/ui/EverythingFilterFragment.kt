package ru.medyannikov.newsapi.filter.everything_filter.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import kotlinx.android.synthetic.main.fragment_everything_filter.*
import org.threeten.bp.LocalDate
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.feed.everything.domain.params.SortBy
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.core.ui.date_bottom.DatePickerBottomSheet
import ru.medyannikov.newsapi.filter.core.domain.model.LanguageModel
import ru.medyannikov.newsapi.filter.core.ui.spinner.SpinnerAdapter
import ru.medyannikov.newsapi.filter.core.ui.spinner.SpinnerBottomSheet
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.AdapterDelegate
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.BaseAdapter
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.filter.core.ui.add_button.AddButtonDelegate
import ru.medyannikov.newsapi.filter.core.ui.add_button.AddButtonItem
import ru.medyannikov.newsapi.filter.core.ui.decorator.BetweenItemsDividerDecoration
import ru.medyannikov.newsapi.filter.core.ui.language.LanguageAdapter
import ru.medyannikov.newsapi.filter.core.ui.selected.SelectedAdapterDelegate
import ru.medyannikov.newsapi.utils.date.formatSimple

class EverythingFilterFragment : BaseFragment<EverythingFilterViewModel>(), Injectable {

    private lateinit var spinnerSortAdapter: SpinnerAdapter<SortBy>
    private val languagesAdapter: LanguageAdapter = LanguageAdapter { vm.selectLanguage(it) }

    private val adapterSources by lazy {
        BaseAdapter<Any>().apply {
            addDelegate(AddButtonDelegate { vm.onClickAddSources() })
            addDelegate(SelectedAdapterDelegate<SourceModel>(
                    listener = { vm.removeSource(it) },
                    titleGetter = { it.name ?: "" }
            ) as AdapterDelegate<Any>)
            setData(listOf(AddButtonItem))
        }
    }

    private val adapterDomains by lazy {
        BaseAdapter<Any>().apply {
            addDelegate(AddButtonDelegate { vm.onClickAddDomains() })
            addDelegate(SelectedAdapterDelegate<String>(listener = { vm.removeDomain(it) }) as AdapterDelegate<Any>)
            setData(listOf(AddButtonItem))
        }
    }

    private val adapterExcludeDomains by lazy {
        BaseAdapter<Any>().apply {
            addDelegate(AddButtonDelegate { vm.onClickAddExcludeDomains() })
            addDelegate(SelectedAdapterDelegate<String>(listener = { vm.removeExcludeDomain(it) }) as AdapterDelegate<Any>)
            setData(listOf(AddButtonItem))
        }
    }

    private val spinnerBottomSheet by lazy {
        SpinnerBottomSheet<SortBy>(requireContext(),
                getString(R.string.filter_sort_by),
                onDoneListener = { vm.setSortType(it) })
    }

    private val spinnerDateFrom by lazy {
        DatePickerBottomSheet(requireContext(), getString(R.string.date_from)) {
            vm.setDateFrom(it)
        }
    }

    private val spinnerDateTo by lazy {
        DatePickerBottomSheet(requireContext(), getString(R.string.date_to)) {
            vm.setDateTo(it)
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_everything_filter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        vm = provideVm()
        vm.attachBaseListeners()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(configuration = ActivityConfiguration(
                title = getString(R.string.title_everything_filter), hasBackButton = true), toolbar = toolbar)

        initViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        vm.hideNavigationMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_filter_remove, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_remove -> vm.resetFilter()
        }

        return true
    }

    private fun initViews() {
        containerLanguage.layoutManager = FlexboxLayoutManager(requireContext(), FlexDirection.ROW, FlexWrap.WRAP)
        containerLanguage.adapter = languagesAdapter
        containerLanguage.addItemDecoration(BetweenItemsDividerDecoration(requireContext(), RecyclerView.HORIZONTAL, R.drawable.spacing_divider))
        containerLanguage.addItemDecoration(BetweenItemsDividerDecoration(requireContext(), RecyclerView.VERTICAL, R.drawable.spacing_divider))

        initSources()
        initDomains()
        initExcludeDomains()
        initSortField()
        initDateFiled()
        btnApplyFilter.setOnClickListener { vm.applyFilter() }
    }

    private fun initSources() {
        rvSources.layoutManager = LinearLayoutManager(requireContext())
        rvSources.adapter = adapterSources
    }

    private fun initDomains() {
        rvDomains.layoutManager = LinearLayoutManager(requireContext())
        rvDomains.adapter = adapterDomains
    }

    private fun initExcludeDomains() {
        rvExcludeDomains.layoutManager = LinearLayoutManager(requireContext())
        rvExcludeDomains.adapter = adapterExcludeDomains
    }

    private fun initDateFiled() {
        clFrom.setOnClickListener { spinnerDateFrom.show() }
        clTo.setOnClickListener { spinnerDateTo.show() }
    }

    private fun initSortField() {
        spinnerSortAdapter = SpinnerAdapter<SortBy>().apply {
            items = SortBy.values
            titleGetter = { sortBy -> getString(sortBy.title) }
        }
        spinnerBottomSheet.setAdapter(spinnerSortAdapter)
        clSortSpinner.setOnClickListener { spinnerBottomSheet.show() }
    }

    private fun observeViewModel() {
        vm.languages.observe(this, Observer {
            languagesAdapter.submitList(it)
        })

        vm.filterParams.observe(this, Observer {
            parseSortBy(it.sortBy)
            sortLanguage(it.language)
            parseDateFrom(it.from)
            parseDateTo(it.to)
            parseSources(it.sources)
            parseDomains(it.domains)
            parseExcludeDomains(it.excludeDomains)
        })

        vm.isChangeFilter.observe(this, Observer { btnApplyFilter.isVisible = it })
    }

    private fun parseSources(sources: List<SourceModel>?) {
        val list = sources ?: emptyList()
        adapterSources.setData(list.plus(AddButtonItem))
    }

    private fun parseDomains(domains: List<String>?) {
        val list = domains ?: emptyList()
        adapterDomains.setData(list.plus(AddButtonItem))
    }

    private fun parseExcludeDomains(domains: List<String>?) {
        val list = domains ?: emptyList()
        adapterExcludeDomains.setData(list.plus(AddButtonItem))
    }

    private fun parseDateFrom(date: LocalDate?) {
        tvFromValue.text = date?.formatSimple() ?: getString(R.string.select_date)
    }

    private fun parseDateTo(date: LocalDate?) {
        tvToValue.text = date?.formatSimple() ?: getString(R.string.select_date)
    }

    private fun sortLanguage(language: LanguageModel?) {
        val list = language?.let { listOf(language) } ?: listOf()
        languagesAdapter.setSelectedItems(list)
    }

    private fun parseSortBy(sortBy: SortBy?) {
        val type = sortBy ?: SortBy.PUBLISHED_AT

        val value = getString(type.title)
        tvSortValue.text = value
        spinnerSortAdapter.selectedItem = type
    }

    companion object {

        fun newInstance() = EverythingFilterFragment()
    }
}