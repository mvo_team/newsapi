package ru.medyannikov.newsapi.filter.core.domain.provider.category

import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel

interface AvailableCategoryProvider {

    fun getAvailableCategories(): List<CategoryModel>
}