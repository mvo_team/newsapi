package ru.medyannikov.newsapi.filter.top_headlines.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import kotlinx.android.synthetic.main.fragment_top_headline_filter.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.AdapterDelegate
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.BaseAdapter
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.filter.core.ui.add_button.AddButtonDelegate
import ru.medyannikov.newsapi.filter.core.ui.add_button.AddButtonItem
import ru.medyannikov.newsapi.filter.core.ui.category.CategoryAdapter
import ru.medyannikov.newsapi.filter.core.ui.country.CountryAdapter
import ru.medyannikov.newsapi.filter.core.ui.decorator.BetweenItemsDividerDecoration
import ru.medyannikov.newsapi.filter.core.ui.selected.SelectedAdapterDelegate

class TopHeadlinesFilterFragment : BaseFragment<TopHeadlinesFilterViewModel>(), Injectable {

    private val countryAdapter: CountryAdapter = CountryAdapter { vm.selectCountry(it) }
    private val categoriesAdapter: CategoryAdapter = CategoryAdapter { vm.selectCategory(it) }

    private val adapterSources by lazy {
        BaseAdapter<Any>().apply {
            addDelegate(AddButtonDelegate { vm.onClickAddSources() })
            addDelegate(SelectedAdapterDelegate<SourceModel>(
                    listener = { vm.removeSource(it) },
                    titleGetter = { it.name ?: "" }
            ) as AdapterDelegate<Any>)
            setData(listOf(AddButtonItem))
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_top_headline_filter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        vm = provideVm()
        vm.attachBaseListeners()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(
                configuration = ActivityConfiguration(
                        title = getString(R.string.title_top_headline_filter), hasBackButton = true
                ), toolbar = toolbar
        )

        initViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        vm.hideNavigationMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_filter_remove, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_remove -> vm.resetFilter()
        }

        return true
    }

    private fun initViews() {
        initCountry()
        initCategory()
        initSources()

        btnApplyFilter.setOnClickListener { vm.applyFilter() }
    }

    private fun initCountry() {
        containerCountry.layoutManager = FlexboxLayoutManager(requireContext(), FlexDirection.ROW, FlexWrap.WRAP)
        containerCountry.adapter = countryAdapter
        containerCountry.addItemDecoration(BetweenItemsDividerDecoration(requireContext(), RecyclerView.HORIZONTAL, R.drawable.spacing_divider))
        containerCountry.addItemDecoration(BetweenItemsDividerDecoration(requireContext(), RecyclerView.VERTICAL, R.drawable.spacing_divider))
    }

    private fun initCategory() {
        containerCategory.layoutManager = FlexboxLayoutManager(requireContext(), FlexDirection.ROW, FlexWrap.WRAP)
        containerCategory.adapter = categoriesAdapter
        containerCategory.addItemDecoration(BetweenItemsDividerDecoration(requireContext(), RecyclerView.HORIZONTAL, R.drawable.spacing_divider))
        containerCategory.addItemDecoration(BetweenItemsDividerDecoration(requireContext(), RecyclerView.VERTICAL, R.drawable.spacing_divider))
    }

    private fun initSources() {
        rvSources.layoutManager = LinearLayoutManager(requireContext())
        rvSources.adapter = adapterSources
    }


    private fun observeViewModel() {
        vm.countries.observe(this, Observer {
            countryAdapter.submitList(it)
        })

        vm.categories.observe(this, Observer {
            categoriesAdapter.submitList(it)
        })

        vm.filterParams.observe(this, Observer {
            parseSources(it.sources)
            parseCategory(it.category)
            parseCountry(it.county)
        })

        vm.isChangeFilter.observe(this, Observer { btnApplyFilter.isVisible = it })
    }

    private fun parseSources(sources: List<SourceModel>?) {
        val list = sources ?: emptyList()
        adapterSources.setData(list.plus(AddButtonItem))
    }

    private fun parseCountry(country: CountryModel?) {
        val list = country?.let { listOf(country) } ?: listOf()
        countryAdapter.setSelectedItems(list)
    }

    private fun parseCategory(category: CategoryModel?) {
        val list = category?.let { listOf(category) } ?: listOf()
        categoriesAdapter.setSelectedItems(list)
    }

    companion object {

        fun newInstance() = TopHeadlinesFilterFragment()
    }
}