package ru.medyannikov.newsapi.filter.core.ui

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class SelectableAdapter<T, VH: RecyclerView.ViewHolder> : ListAdapter<T, VH>(DiffCallback<T>()) {

    private val selectedItems: MutableList<T> = arrayListOf()

    fun setSelectedItems(items: List<T>) {
        selectedItems.clear()
        selectedItems.addAll(items.toList())
        notifyDataSetChanged()
    }

    fun toggleItem(item: T) {
        val itemPosition = currentList.indexOf(item)
        if (selectedItems.contains(item)) {
            selectedItems.remove(item)
        } else {
            selectedItems.add(item)
        }
        notifyItemChanged(itemPosition)
    }

    fun getSelectedItems(): List<T> = selectedItems.toList()

    fun containInSelected(item: T): Boolean = selectedItems.contains(item)

    class DiffCallback<T> : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
            return oldItem == newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
            return oldItem == newItem
        }
    }
}