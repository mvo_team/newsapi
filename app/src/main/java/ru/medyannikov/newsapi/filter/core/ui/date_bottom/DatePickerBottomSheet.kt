package ru.medyannikov.newsapi.filter.core.ui.date_bottom

import android.content.Context
import android.view.LayoutInflater
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.layout_calendar_dialog_bottom_sheet.view.*
import org.threeten.bp.LocalDate
import ru.medyannikov.newsapi.R

class DatePickerBottomSheet(context: Context,
                            title: String?,
                            private val onDoneListener: (LocalDate) -> Unit) {

    private var bottomSheetDialog: BottomSheetDialog? = null

    init {
        val bottomSheet = LayoutInflater.from(context)
                .inflate(R.layout.layout_calendar_dialog_bottom_sheet, null, false)

        bottomSheetDialog = BottomSheetDialog(context).apply {
            setContentView(bottomSheet)
            setCancelable(false)
        }

        bottomSheet.tvCalendarTitle.text = title

        bottomSheet.btnDone.setOnClickListener {
            val year = bottomSheet.datePicker.year
            val month = bottomSheet.datePicker.month + 1
            val day = bottomSheet.datePicker.dayOfMonth
            val date = LocalDate.of(year, month, day)

            onDoneListener.invoke(date)
            dismiss()
        }
    }

    fun show() {
        bottomSheetDialog?.takeIf { !it.isShowing }?.show()
    }

    fun dismiss() {
        bottomSheetDialog?.dismiss()
    }
}