package ru.medyannikov.newsapi.filter.core.ui.selected

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_list_filter_item.*

class SelectedItemViewHolder<T>(override val containerView: View,
                                private val listener: (item: T) -> Unit,
                                private val titleGetter: (item: T) -> String,
                                renderParams: SelectedAdapterDelegate.SelectedRenderParams?) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    init {
        renderParams?.let {
            val itemLayoutParams = containerView.layoutParams as RecyclerView.LayoutParams
            itemLayoutParams.marginStart = it.startMargin
            itemLayoutParams.marginEnd = it.endMargin
            containerView.layoutParams = itemLayoutParams
        }
    }

    fun onBind(item: T) {
        tvTitle.text = titleGetter(item)
        ivRemove.setOnClickListener { listener(item) }
    }
}