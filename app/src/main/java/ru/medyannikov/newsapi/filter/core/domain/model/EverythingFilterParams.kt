package ru.medyannikov.newsapi.filter.core.domain.model

import org.threeten.bp.LocalDate
import ru.medyannikov.newsapi.feed.everything.domain.params.SortBy
import ru.medyannikov.newsapi.core.domain.model.SourceModel

data class EverythingFilterParams(val query: String? = null,
                                  val sources: List<SourceModel>? = null,
                                  val domains: List<String>? = null,
                                  val excludeDomains: List<String>? = null,
                                  val from: LocalDate? = null,
                                  val to: LocalDate? = null,
                                  val language: LanguageModel? = null,
                                  val sortBy: SortBy = SortBy.PUBLISHED_AT) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EverythingFilterParams

        if (query != other.query) return false
        if (sources != other.sources) return false
        if (domains != other.domains) return false
        if (excludeDomains != other.excludeDomains) return false
        if (from != other.from) return false
        if (to != other.to) return false
        if (language != other.language) return false
        if (sortBy != other.sortBy) return false

        return true
    }

    override fun hashCode(): Int {
        var result = query?.hashCode() ?: 0
        result = 31 * result + (sources?.hashCode() ?: 0)
        result = 31 * result + (domains?.hashCode() ?: 0)
        result = 31 * result + (excludeDomains?.hashCode() ?: 0)
        result = 31 * result + (from?.hashCode() ?: 0)
        result = 31 * result + (to?.hashCode() ?: 0)
        result = 31 * result + (language?.hashCode() ?: 0)
        result = 31 * result + sortBy.hashCode()
        return result
    }
}