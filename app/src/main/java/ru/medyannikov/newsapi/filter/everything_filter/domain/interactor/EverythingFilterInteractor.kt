package ru.medyannikov.newsapi.filter.everything_filter.domain.interactor

import ru.medyannikov.newsapi.filter.core.domain.model.LanguageModel

interface EverythingFilterInteractor {

    fun getAvailableLanguages(): List<LanguageModel>
}