package ru.medyannikov.newsapi.filter.everything_filter.domain.interactor.impl

import ru.medyannikov.newsapi.filter.core.domain.provider.language.AvailableLanguageProvider
import ru.medyannikov.newsapi.filter.everything_filter.domain.interactor.EverythingFilterInteractor
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.impl.EverythingFilterProviderImpl
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EverythingFilterInteractorImpl
    @Inject constructor(private val filterProvider: EverythingFilterProviderImpl,
                        private val languageProvider: AvailableLanguageProvider): EverythingFilterInteractor {

    override fun getAvailableLanguages() = languageProvider.getAvailableLanguages()
}