package ru.medyannikov.newsapi.filter.core.ui.decorator

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt

class BetweenItemsDividerDecoration(
        context: Context,
        private val orientation: Int,
        @DrawableRes res: Int)
    : androidx.recyclerview.widget.DividerItemDecoration(context, orientation) {

    init {
        val drawable = ContextCompat.getDrawable(context, res)
        drawable?.let { setDrawable(drawable) }
    }

    private val mBounds = Rect()

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (orientation == VERTICAL) {
            super.onDraw(c, parent, state)
            return
        }

        drawHorizontal(c, parent)
    }

    private fun drawHorizontal(canvas: Canvas, parent: RecyclerView) {
        val mDivider = drawable ?: return
        val layoutManager = parent.layoutManager ?: return
        canvas.save()
        val top: Int
        val bottom: Int

        if (parent.clipToPadding) {
            top = parent.paddingTop
            bottom = parent.height - parent.paddingBottom
            canvas.clipRect(parent.paddingLeft, top,
                    parent.width - parent.paddingRight, bottom)
        } else {
            top = 0
            bottom = parent.height
        }
        val realTop = top + (bottom - top - mDivider.intrinsicHeight) / 2
        val realBottom = realTop + mDivider.intrinsicHeight
        val childCount = parent.childCount
        for (i in 0 until childCount - 1) {
            val child = parent.getChildAt(i)
            layoutManager.getDecoratedBoundsWithMargins(child, mBounds)
            val right = mBounds.right + child.translationX.roundToInt()
            val left = right - mDivider.intrinsicWidth
            mDivider.setBounds(left, realTop, right, realBottom)
            mDivider.draw(canvas)
        }
        canvas.restore()
    }
}