package ru.medyannikov.newsapi.filter.core.domain.provider.country

import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel

interface AvailableCountryProvider {

    fun getAvailableCountries(): List<CountryModel>
}