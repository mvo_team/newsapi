package ru.medyannikov.newsapi.filter.everything_filter.ui

import androidx.lifecycle.MutableLiveData
import org.threeten.bp.LocalDate
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.domains.SelectorDomainType
import ru.medyannikov.newsapi.feed.everything.domain.params.SortBy
import ru.medyannikov.newsapi.filter.core.domain.model.EverythingFilterParams
import ru.medyannikov.newsapi.filter.core.domain.model.LanguageModel
import ru.medyannikov.newsapi.filter.everything_filter.domain.interactor.EverythingFilterInteractor
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.EverythingFilterProvider
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.utils.aac.SingleLiveEvent
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject

class EverythingFilterViewModel
@Inject constructor(private val filterProvider: EverythingFilterProvider,
                    private val schedulerProvider: SchedulerProvider,
                    private val mainRouter: MainRouter,
                    private val localeManager: LocaleManager,
                    private val navigationMenuController: NavigationMenuController,
                    private val everythingFilterInteractor: EverythingFilterInteractor) : BaseViewModel() {

    val filterParams: MutableLiveData<EverythingFilterParams> = MutableLiveData()
    val languages: MutableLiveData<List<LanguageModel>> = MutableLiveData()
    val isChangeFilter by lazy { SingleLiveEvent<Boolean>() }

    init {
        initProvider()
        filterProvider.clearFilter()

        filterProvider.tempState()
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .baseSubscribe { parseFilterParams(it) }

        filterProvider.stateChanges()
                .baseSubscribe { isChangeFilter.value = it }
    }

    private fun parseFilterParams(filterParams: EverythingFilterParams) {
        this.filterParams.value = filterParams
    }

    private fun initProvider() {
        languages.postValue(everythingFilterInteractor.getAvailableLanguages())
    }

    fun selectLanguage(model: LanguageModel) {
        filterProvider.setLanguage(model)
    }

    fun setSortType(sortBy: SortBy?) {
        sortBy?.let { filterProvider.sortBy(it) }
    }

    fun setDateFrom(date: LocalDate) {
        filterProvider.setFromDate(date)
    }

    fun setDateTo(date: LocalDate) {
        filterProvider.setToDate(date)
    }

    fun hideNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }

    fun onClickAddSources() {
        val preSelected = filterParams.value?.sources ?: emptyList()
        mainRouter.openEverythingSourceSelector(preSelected)
    }

    fun onClickAddDomains() {
        val preSelected = filterParams.value?.domains ?: emptyList()
        mainRouter.openDomainSelector(preSelected, SelectorDomainType.INCLUDE)
    }

    fun onClickAddExcludeDomains() {
        val preSelected = filterParams.value?.excludeDomains ?: emptyList()
        mainRouter.openDomainSelector(preSelected, SelectorDomainType.EXCLUDE)
    }

    fun removeSource(item: SourceModel) {
        filterProvider.removeSource(item)
    }

    fun removeDomain(domain: String) {
        filterProvider.removeDomain(domain)
    }

    fun removeExcludeDomain(domain: String) {
        filterProvider.removeExcludeDomain(domain)
    }

    fun applyFilter() {
        filterProvider.applyFilter()
        mainRouter.exit()
    }

    fun resetFilter() {
        filterProvider.resetFilter()
    }
}