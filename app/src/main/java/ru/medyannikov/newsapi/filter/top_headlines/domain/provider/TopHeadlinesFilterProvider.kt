package ru.medyannikov.newsapi.filter.top_headlines.domain.provider

import io.reactivex.Observable
import io.reactivex.Single
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel
import ru.medyannikov.newsapi.filter.core.domain.model.TopHeadlinesFilterParams

interface TopHeadlinesFilterProvider {

    fun tempState(): Observable<TopHeadlinesFilterParams>
    fun currentState(): Observable<TopHeadlinesFilterParams>
    fun stateChanges(): Observable<Boolean>
    fun setQuery(query: String?)
    fun setSources(sources: Collection<SourceModel>)
    fun setCountry(country: CountryModel)
    fun setCategory(category: CategoryModel)
    fun applyFilter()
    fun clearFilter()
    fun resetFilter()
    fun filterParamsAsync(): Single<TopHeadlinesFilterParams>
    fun removeSource(item: SourceModel)
}