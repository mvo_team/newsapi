package ru.medyannikov.newsapi.filter.core.ui.add_button

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_add_item.*

class AddButtonViewHolder(override val containerView: View,
                          private val onClickListener: () -> Unit): RecyclerView.ViewHolder(containerView), LayoutContainer {

    init {
        btnApplyFilter.setOnClickListener { onClickListener() }
    }

    fun onBind() {

    }
}