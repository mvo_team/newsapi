package ru.medyannikov.newsapi.filter.core.ui.spinner

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.layout_spinner_bottom_sheet.view.*
import ru.medyannikov.newsapi.R

class SpinnerBottomSheet<T>(private val context: Context,
                            title: String?,
                            private val onDoneListener: (T?) -> Unit,
                            private val visibleDoneButton: Boolean = false) {

    private lateinit var adapter: SpinnerAdapter<T>
    private var bottomSheetDialog: BottomSheetDialog? = null
    private var bottomSheet: View = LayoutInflater.from(context)
            .inflate(R.layout.layout_spinner_bottom_sheet, null, false)

    init {

        bottomSheetDialog = BottomSheetDialog(context).apply {
            setContentView(bottomSheet)
            setCancelable(true)
        }

        bottomSheet.btnDone.isVisible = visibleDoneButton
        bottomSheet.tvTitle.text = title
    }

    fun show() {
        bottomSheetDialog?.takeIf { !it.isShowing }?.show()
    }

    fun dismiss() {
        bottomSheetDialog?.dismiss()
    }

    fun setAdapter(adapter: SpinnerAdapter<T>) {
        this.adapter = adapter
        bottomSheet.recyclerView.layoutManager = LinearLayoutManager(context)
        bottomSheet.recyclerView.adapter = adapter
        adapter.onClickListener = {
            onDoneListener.invoke(adapter.selectedItem)
            dismiss()
        }

        checkEmpty()

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                checkEmpty()
            }
        })
    }

    private fun checkEmpty() {
        bottomSheet.recyclerView.isVisible = adapter.items.isNotEmpty()
        bottomSheet.emptyView.isVisible = adapter.items.isEmpty()
        bottomSheetDialog?.setCancelable(adapter.items.isEmpty())
    }

    fun getSelectedItemTitle(): CharSequence? = if (::adapter.isInitialized) {
        adapter.selectedItem?.let { adapter.titleGetter(it) }
    } else null

    fun getSelectedItem(): T? = if (::adapter.isInitialized) {
        adapter.selectedItem
    } else null
}