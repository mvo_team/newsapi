package ru.medyannikov.newsapi.filter.top_headlines.domain.interactor.impl

import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel
import ru.medyannikov.newsapi.filter.core.domain.provider.category.AvailableCategoryProvider
import ru.medyannikov.newsapi.filter.core.domain.provider.country.AvailableCountryProvider
import ru.medyannikov.newsapi.filter.top_headlines.domain.interactor.TopHeadlinesFilterInteractor
import javax.inject.Inject

class TopHeadlinesFilterInteractorImpl
    @Inject constructor(private val categoryProvider: AvailableCategoryProvider,
                        private val countryProvider: AvailableCountryProvider): TopHeadlinesFilterInteractor {

    override fun getAvailableCountries(): List<CountryModel> = countryProvider.getAvailableCountries()

    override fun getAvailableCategories(): List<CategoryModel> = categoryProvider.getAvailableCategories()
}