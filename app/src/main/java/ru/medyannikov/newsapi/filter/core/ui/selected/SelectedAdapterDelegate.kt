package ru.medyannikov.newsapi.filter.core.ui.selected

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.AdapterDelegate

class SelectedAdapterDelegate<T>(private val listener: (item: T) -> Unit,
                                 private val titleGetter: (item: T) -> String = { item -> item.toString() },
                                 private val renderParams: SelectedRenderParams? = null) : AdapterDelegate<T>() where T : Any {

    override fun isForViewType(item: T, position: Int): Boolean = item is T

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_list_filter_item, parent, false)
        return SelectedItemViewHolder(view, listener, titleGetter, renderParams)
    }

    override fun onBindViewHolder(item: T, position: Int, holder: RecyclerView.ViewHolder) {
        holder as SelectedItemViewHolder<T>

        holder.onBind(item)
    }

    class SelectedRenderParams(val startMargin: Int, val endMargin: Int) {

        companion object {

            fun getRenderParams(resources: Resources, @DimenRes startMargin: Int, @DimenRes endMargin: Int): SelectedRenderParams {
                val newStartMargin = resources.getDimensionPixelSize(startMargin)
                val newEndMargin = resources.getDimensionPixelSize(endMargin)

                return SelectedRenderParams(newStartMargin, newEndMargin)
            }
        }
    }
}