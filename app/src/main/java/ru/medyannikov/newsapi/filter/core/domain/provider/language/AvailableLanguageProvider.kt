package ru.medyannikov.newsapi.filter.core.domain.provider.language

import ru.medyannikov.newsapi.filter.core.domain.model.LanguageModel

interface AvailableLanguageProvider {

    fun getAvailableLanguages(): List<LanguageModel>
}