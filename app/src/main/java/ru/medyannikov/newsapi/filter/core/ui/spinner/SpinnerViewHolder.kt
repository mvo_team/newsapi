package ru.medyannikov.newsapi.filter.core.ui.spinner

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_spinner_item.*
import ru.medyannikov.newsapi.R

class SpinnerViewHolder<T>(override val containerView: View,
                           private val listener: (T) -> Unit) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun onBind(item: T, title: CharSequence, isChecked: Boolean = false) {
        tvSpinnerTitle.text = title
        val bgResource = when (isChecked) {
            true -> R.color.color_selected
            else -> android.R.color.white
        }
        rbSpinner.isChecked = isChecked
        clSpinnerContainer.setBackgroundResource(bgResource)
        clSpinnerContainer.setOnClickListener { listener(item) }
    }
}