package ru.medyannikov.newsapi.filter.core.domain.model

data class SelectedParameter<T>(val item: T, var isSelected: Boolean = false)