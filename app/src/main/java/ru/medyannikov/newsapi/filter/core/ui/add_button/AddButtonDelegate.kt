package ru.medyannikov.newsapi.filter.core.ui.add_button

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.AdapterDelegate

class AddButtonDelegate(private val onClickListener: () -> Unit) : AdapterDelegate<Any>() {
    override fun isForViewType(item: Any, position: Int): Boolean = item is AddButtonItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_add_item, parent, false)
        return AddButtonViewHolder(view, onClickListener)
    }

    override fun onBindViewHolder(item: Any, position: Int, holder: RecyclerView.ViewHolder) {

    }
}