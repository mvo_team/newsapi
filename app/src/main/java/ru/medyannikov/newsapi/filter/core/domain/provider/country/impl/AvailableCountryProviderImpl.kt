package ru.medyannikov.newsapi.filter.core.domain.provider.country.impl

import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel
import ru.medyannikov.newsapi.filter.core.domain.provider.country.AvailableCountryProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AvailableCountryProviderImpl
@Inject constructor(private val localeManager: LocaleManager) : AvailableCountryProvider {

    private val countries: List<CountryModel> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) { generateCountries() }

    override fun getAvailableCountries(): List<CountryModel> = countries

    private fun generateCountries(): List<CountryModel> {
        return listOf(
                CountryModel(localeManager.getString(R.string.country_ae), "ae"),
                CountryModel(localeManager.getString(R.string.country_ar), "ar"),
                CountryModel(localeManager.getString(R.string.country_at), "at"),
                CountryModel(localeManager.getString(R.string.country_au), "au"),
                CountryModel(localeManager.getString(R.string.country_be), "be"),
                CountryModel(localeManager.getString(R.string.country_bg), "bg"),
                CountryModel(localeManager.getString(R.string.country_br), "br"),
                CountryModel(localeManager.getString(R.string.country_ca), "ca"),
                CountryModel(localeManager.getString(R.string.country_ch), "ch"),
                CountryModel(localeManager.getString(R.string.country_cn), "cn"),
                CountryModel(localeManager.getString(R.string.country_co), "co"),
                CountryModel(localeManager.getString(R.string.country_cu), "cu"),
                CountryModel(localeManager.getString(R.string.country_cz), "cz"),
                CountryModel(localeManager.getString(R.string.country_de), "de"),
                CountryModel(localeManager.getString(R.string.country_eg), "eg"),
                CountryModel(localeManager.getString(R.string.country_fr), "fr"),
                CountryModel(localeManager.getString(R.string.country_gb), "gb"),
                CountryModel(localeManager.getString(R.string.country_gr), "gr"),

                CountryModel(localeManager.getString(R.string.country_hk), "hk"),
                CountryModel(localeManager.getString(R.string.country_hu), "hu"),
                CountryModel(localeManager.getString(R.string.country_id), "id"),
                CountryModel(localeManager.getString(R.string.country_ie), "ie"),
                CountryModel(localeManager.getString(R.string.country_il), "il"),
                CountryModel(localeManager.getString(R.string.country_in), "in"),
                CountryModel(localeManager.getString(R.string.country_it), "it"),
                CountryModel(localeManager.getString(R.string.country_jp), "jp"),
                CountryModel(localeManager.getString(R.string.country_kr), "kr"),
                CountryModel(localeManager.getString(R.string.country_lt), "lt"),
                CountryModel(localeManager.getString(R.string.country_lv), "lv"),
                CountryModel(localeManager.getString(R.string.country_ma), "ma"),
                CountryModel(localeManager.getString(R.string.country_mx), "mx"),
                CountryModel(localeManager.getString(R.string.country_my), "my"),
                CountryModel(localeManager.getString(R.string.country_ng), "ng"),
                CountryModel(localeManager.getString(R.string.country_nl), "nl"),
                CountryModel(localeManager.getString(R.string.country_no), "no"),
                CountryModel(localeManager.getString(R.string.country_nz), "nz"),

                CountryModel(localeManager.getString(R.string.country_ph), "ph"),
                CountryModel(localeManager.getString(R.string.country_pl), "pl"),
                CountryModel(localeManager.getString(R.string.country_pt), "pt"),
                CountryModel(localeManager.getString(R.string.country_ro), "ro"),
                CountryModel(localeManager.getString(R.string.country_rs), "rs"),
                CountryModel(localeManager.getString(R.string.country_ru), "ru"),
                CountryModel(localeManager.getString(R.string.country_sa), "sa"),
                CountryModel(localeManager.getString(R.string.country_se), "se"),
                CountryModel(localeManager.getString(R.string.country_sg), "sg"),
                CountryModel(localeManager.getString(R.string.country_si), "si"),
                CountryModel(localeManager.getString(R.string.country_sk), "sk"),
                CountryModel(localeManager.getString(R.string.country_th), "th"),
                CountryModel(localeManager.getString(R.string.country_tr), "tr"),
                CountryModel(localeManager.getString(R.string.country_tw), "tw"),
                CountryModel(localeManager.getString(R.string.country_ua), "ua"),
                CountryModel(localeManager.getString(R.string.country_us), "us"),
                CountryModel(localeManager.getString(R.string.country_ve), "ve"),
                CountryModel(localeManager.getString(R.string.country_za), "za")
        )
    }
}