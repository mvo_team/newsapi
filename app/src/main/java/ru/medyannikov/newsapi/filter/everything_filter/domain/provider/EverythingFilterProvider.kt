package ru.medyannikov.newsapi.filter.everything_filter.domain.provider

import io.reactivex.Observable
import io.reactivex.Single
import org.threeten.bp.LocalDate
import ru.medyannikov.newsapi.feed.everything.domain.params.SortBy
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.core.domain.model.EverythingFilterParams
import ru.medyannikov.newsapi.filter.core.domain.model.LanguageModel

interface EverythingFilterProvider {
    fun tempState(): Observable<EverythingFilterParams>
    fun currentState(): Observable<EverythingFilterParams>
    fun stateChanges(): Observable<Boolean>
    fun setQuery(query: String?)
    fun setSources(sources: Collection<SourceModel>)
    fun setDomains(domains: Collection<String>)
    fun setExcludeDomains(domains: Collection<String>)
    fun setFromDate(from: LocalDate)
    fun setToDate(to: LocalDate)
    fun setLanguage(language: LanguageModel)
    fun sortBy(sortBy: SortBy)
    fun applyFilter()
    fun clearFilter()
    fun resetFilter()
    fun filterParamsAsync(): Single<EverythingFilterParams>
    fun removeSource(item: SourceModel)
    fun removeDomain(domain: String)
    fun removeExcludeDomain(domain: String)
}