package ru.medyannikov.newsapi.filter.core.domain.model

data class CountryModel(val name: String, val isoValue: String) {

    override fun hashCode(): Int {
        return isoValue.hashCode()
    }
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CountryModel

        if (isoValue != other.isoValue) return false

        return true
    }

    override fun toString(): String = name
}