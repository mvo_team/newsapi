package ru.medyannikov.newsapi.filter.core.domain.model

import ru.medyannikov.newsapi.core.domain.model.SourceModel

data class TopHeadlinesFilterParams(val query: String? = null,
                                    val sources: List<SourceModel>? = null,
                                    val county: CountryModel? = null,
                                    val category: CategoryModel? = null) {
}