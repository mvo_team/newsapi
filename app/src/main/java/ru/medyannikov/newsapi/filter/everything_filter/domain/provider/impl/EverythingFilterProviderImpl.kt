package ru.medyannikov.newsapi.filter.everything_filter.domain.provider.impl

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import org.threeten.bp.LocalDate
import ru.medyannikov.newsapi.feed.everything.domain.params.SortBy
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.core.domain.model.EverythingFilterParams
import ru.medyannikov.newsapi.filter.core.domain.model.LanguageModel
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.EverythingFilterProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EverythingFilterProviderImpl
@Inject constructor() : EverythingFilterProvider {

    private val tempState: Subject<EverythingFilterParams> = BehaviorSubject.createDefault(EverythingFilterParams())
    private val currentState: Subject<EverythingFilterParams> = BehaviorSubject.createDefault(EverythingFilterParams())
    private val isChangeState = BehaviorSubject.createDefault(false)

    private var query: String? = null
    private var from: LocalDate? = null
    private var to: LocalDate? = null
    private var language: LanguageModel? = null
    private var sortBy: SortBy = SortBy.PUBLISHED_AT
    private var selectedSources: MutableCollection<SourceModel> = mutableListOf()
        set(value) {
            field.clear()
            field.addAll(value)
        }

    private var selectedExcludeDomains: MutableList<String> = mutableListOf()
        set(value) {
            field.clear()
            field.addAll(value)
        }

    private var selectedDomains: MutableList<String> = mutableListOf()
        set(value) {
            field.clear()
            field.addAll(value)
        }

    override fun tempState(): Observable<EverythingFilterParams> = tempState

    override fun currentState(): Observable<EverythingFilterParams> = currentState

    override fun stateChanges(): Observable<Boolean> = isChangeState

    override fun setQuery(query: String?) {
        this.query = query
        applyFilter()
    }

    override fun setSources(sources: Collection<SourceModel>) {
        this.selectedSources = sources.toMutableList()
        updateFilterData()
    }

    override fun setDomains(domains: Collection<String>) {
        this.selectedDomains = domains.toMutableList()
        updateFilterData()
    }

    override fun setExcludeDomains(domains: Collection<String>) {
        this.selectedExcludeDomains = domains.toMutableList()
        updateFilterData()
    }

    override fun setFromDate(from: LocalDate) {
        this.from = from
        updateFilterData()
    }

    override fun setToDate(to: LocalDate) {
        this.to = to
        updateFilterData()
    }

    override fun setLanguage(language: LanguageModel) {
        if (this.language?.isoValue == language.isoValue) {
            this.language = null
        } else {
            this.language = language
        }
        updateFilterData()
    }

    override fun sortBy(sortBy: SortBy) {
        this.sortBy = sortBy
        updateFilterData()
    }

    override fun applyFilter() {
        val params = fillData()
        currentState.onNext(params)
    }

    override fun clearFilter() {
        val params = currentState.blockingFirst(EverythingFilterParams())
        fillWith(params)
        tempState.onNext(params)
    }

    override fun resetFilter() {
        val params = EverythingFilterParams()
        fillWith(params)
        tempState.onNext(params)
    }

    override fun filterParamsAsync(): Single<EverythingFilterParams> {
        val params = currentState.blockingFirst(EverythingFilterParams())
        return Single.just(params)
    }

    override fun removeSource(item: SourceModel) {
        val newList = selectedSources.toMutableList()
        newList.remove(item)

        setSources(newList)
    }

    override fun removeDomain(domain: String) {
        val newList = selectedDomains.toMutableList()
        newList.remove(domain)

        setDomains(newList)
    }

    override fun removeExcludeDomain(domain: String) {
        val newList = selectedExcludeDomains.toMutableList()
        newList.remove(domain)

        setExcludeDomains(newList)
    }

    private fun updateFilterData() {
        val oldParams = currentState.blockingFirst(EverythingFilterParams())
        val newParams = fillData()
        tempState.onNext(newParams)

        isChangeState.onNext(newParams != oldParams)
    }

    private fun fillData(): EverythingFilterParams {
        return EverythingFilterParams(query = query,
                domains = selectedDomains.toList(),
                excludeDomains = selectedExcludeDomains.toList(),
                sources = selectedSources.toList(),
                language = language,
                from = from,
                to = to,
                sortBy = sortBy)
    }

    private fun fillWith(params: EverythingFilterParams) {
        selectedDomains.apply {
            clear()
            params.domains?.let { addAll(it) }
        }

        selectedExcludeDomains.apply {
            clear()
            params.excludeDomains?.let { addAll(it) }
        }

        selectedSources.apply {
            clear()
            params.sources?.let { addAll(it) }
        }

        query = params.query
        language = params.language
        from = params.from
        to = params.to
        sortBy = params.sortBy
    }
}