package ru.medyannikov.newsapi.filter.core.domain.model

data class CategoryModel(val name: String, val value: String) {

    override fun hashCode(): Int {
        return value.hashCode()
    }
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CategoryModel

        if (value != other.value) return false

        return true
    }

    override fun toString(): String = name
}