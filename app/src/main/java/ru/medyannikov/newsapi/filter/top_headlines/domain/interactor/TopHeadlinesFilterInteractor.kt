package ru.medyannikov.newsapi.filter.top_headlines.domain.interactor

import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel

interface TopHeadlinesFilterInteractor {

    fun getAvailableCountries(): List<CountryModel>

    fun getAvailableCategories(): List<CategoryModel>
}