package ru.medyannikov.newsapi.filter.top_headlines.ui

import androidx.lifecycle.MutableLiveData
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel
import ru.medyannikov.newsapi.filter.core.domain.model.TopHeadlinesFilterParams
import ru.medyannikov.newsapi.filter.top_headlines.domain.interactor.TopHeadlinesFilterInteractor
import ru.medyannikov.newsapi.filter.top_headlines.domain.provider.TopHeadlinesFilterProvider
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.utils.aac.SingleLiveEvent
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject

class TopHeadlinesFilterViewModel
@Inject constructor(private val filterProvider: TopHeadlinesFilterProvider,
                    private val schedulerProvider: SchedulerProvider,
                    private val mainRouter: MainRouter,
                    private val localeManager: LocaleManager,
                    private val navigationMenuController: NavigationMenuController,
                    private val interactor: TopHeadlinesFilterInteractor) : BaseViewModel() {

    val filterParams: MutableLiveData<TopHeadlinesFilterParams> = MutableLiveData()
    val countries: MutableLiveData<List<CountryModel>> = MutableLiveData()
    val categories: MutableLiveData<List<CategoryModel>> = MutableLiveData()
    val isChangeFilter by lazy { SingleLiveEvent<Boolean>() }

    init {
        initProvider()
        filterProvider.clearFilter()

        filterProvider.tempState()
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .baseSubscribe { parseFilterParams(it) }

        filterProvider.stateChanges()
                .baseSubscribe { isChangeFilter.value = it }
    }

    fun selectCountry(country: CountryModel) {
        filterProvider.setCountry(country)
    }

    fun selectCategory(category: CategoryModel) {
        filterProvider.setCategory(category)
    }

    fun hideNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }

    fun onClickAddSources() {
        val preSelected = filterParams.value?.sources ?: emptyList()
        mainRouter.openTopHeadlinesSourceSelector(preSelected)
    }

    fun removeSource(item: SourceModel) {
        filterProvider.removeSource(item)
    }

    fun applyFilter() {
        filterProvider.applyFilter()
        mainRouter.exit()
    }

    fun resetFilter() {
        filterProvider.resetFilter()
    }

    private fun parseFilterParams(filterParams: TopHeadlinesFilterParams) {
        this.filterParams.value = filterParams
    }

    private fun initProvider() {
        countries.postValue(interactor.getAvailableCountries())
        categories.postValue(interactor.getAvailableCategories())
    }
}