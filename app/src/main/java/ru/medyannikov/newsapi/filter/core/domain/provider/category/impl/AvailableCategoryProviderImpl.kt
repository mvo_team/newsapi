package ru.medyannikov.newsapi.filter.core.domain.provider.category.impl

import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.filter.core.domain.provider.category.AvailableCategoryProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AvailableCategoryProviderImpl
    @Inject constructor(private val localeManager: LocaleManager): AvailableCategoryProvider {

    private val categories: List<CategoryModel> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) { generateCategories() }

    override fun getAvailableCategories(): List<CategoryModel> = categories

    private fun generateCategories(): List<CategoryModel> {
        return listOf(
                CategoryModel(localeManager.getString(R.string.category_business), "business"),
                CategoryModel(localeManager.getString(R.string.category_entertainment), "entertainment"),
                CategoryModel(localeManager.getString(R.string.category_general), "general"),
                CategoryModel(localeManager.getString(R.string.category_health), "health"),
                CategoryModel(localeManager.getString(R.string.category_science), "science"),
                CategoryModel(localeManager.getString(R.string.category_sports), "sports"),
                CategoryModel(localeManager.getString(R.string.category_technology), "technology")
        )
    }
}