package ru.medyannikov.newsapi.filter.top_headlines.domain.provider.impl

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.filter.core.domain.model.CountryModel
import ru.medyannikov.newsapi.filter.core.domain.model.TopHeadlinesFilterParams
import ru.medyannikov.newsapi.filter.top_headlines.domain.provider.TopHeadlinesFilterProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TopHeadlinesFilterProviderImpl
@Inject constructor() : TopHeadlinesFilterProvider {

    private val tempState: Subject<TopHeadlinesFilterParams> = BehaviorSubject.createDefault(TopHeadlinesFilterParams())
    private val currentState: Subject<TopHeadlinesFilterParams> =
        BehaviorSubject.createDefault(TopHeadlinesFilterParams())
    private val isChangeState = BehaviorSubject.createDefault(false)

    private var query: String? = null
    private var country: CountryModel? = null
    private var category: CategoryModel? = null
    private var selectedSources: MutableCollection<SourceModel> = mutableListOf()
        set(value) {
            field.clear()
            field.addAll(value)
        }

    override fun tempState(): Observable<TopHeadlinesFilterParams> = tempState

    override fun currentState(): Observable<TopHeadlinesFilterParams> = currentState

    override fun stateChanges(): Observable<Boolean> = isChangeState

    override fun setQuery(query: String?) {
        this.query = query

        applyFilter()
    }

    override fun setSources(sources: Collection<SourceModel>) {
        this.selectedSources = sources.toMutableList()
        updateFilterData()
    }

    override fun setCountry(country: CountryModel) {
        if (this.country?.isoValue == country.isoValue) {
            this.country = null
        } else {
            this.country = country
        }

        updateFilterData()
    }

    override fun setCategory(category: CategoryModel) {
        if (this.category?.value == category.value) {
            this.category = null
        } else {
            this.category = category
        }

        updateFilterData()
    }


    override fun applyFilter() {
        val params = fillData()
        currentState.onNext(params)
    }

    override fun clearFilter() {
        val filterValue = currentState.blockingFirst(TopHeadlinesFilterParams())
        fillWith(filterValue)
        tempState.onNext(fillData())
    }

    override fun resetFilter() {
        val params = TopHeadlinesFilterParams()
        fillWith(params)
        tempState.onNext(params)
    }

    override fun filterParamsAsync(): Single<TopHeadlinesFilterParams> {
        val params = currentState.blockingFirst(TopHeadlinesFilterParams())
        return Single.just(params)
    }

    override fun removeSource(item: SourceModel) {
        val newList = selectedSources.toMutableList()
        newList.remove(item)

        setSources(newList)
    }

    private fun updateFilterData() {
        val oldParams = currentState.blockingFirst(TopHeadlinesFilterParams())
        val newParams = fillData()
        tempState.onNext(newParams)

        isChangeState.onNext(newParams != oldParams)
    }

    private fun fillData(): TopHeadlinesFilterParams {
        return TopHeadlinesFilterParams(
            query = query,
            sources = selectedSources.toList(),
            category = category,
            county = country
        )
    }

    private fun fillWith(params: TopHeadlinesFilterParams) {
        selectedSources.apply {
            clear()
            params.sources?.let { addAll(it) }
        }

        query = params.query
        category = params.category
        country = params.county
    }
}