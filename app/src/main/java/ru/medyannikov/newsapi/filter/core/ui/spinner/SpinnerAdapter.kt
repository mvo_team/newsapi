package ru.medyannikov.newsapi.filter.core.ui.spinner

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.medyannikov.newsapi.R

class SpinnerAdapter<T> : RecyclerView.Adapter<SpinnerViewHolder<T>>() {

    private var selectedIndex: Int = -1

    var onClickListener: ((T) -> Unit)? = null

    var titleGetter: (T) -> CharSequence = { it.toString() }
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var items: List<T> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var selectedItem: T?
        get() {
            return if (selectedIndex != -1) {
                items[selectedIndex]
            } else {
                null
            }
        }
        set(value) {
            val newIndex = items.indexOf(value)

            if (newIndex != selectedIndex) {
                selectedIndex = newIndex
                notifyDataSetChanged()
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpinnerViewHolder<T> {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_spinner_item, parent, false)
        return SpinnerViewHolder(v) {
            selectedItem = it
            onClickListener?.invoke(it)
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SpinnerViewHolder<T>, position: Int) {
        val item = items[position]
        holder.onBind(item, titleGetter(item), position == selectedIndex)
    }
}