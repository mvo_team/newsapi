package ru.medyannikov.newsapi.filter.core.ui.category

import android.view.ViewGroup
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_selecteble.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.filter.core.domain.model.CategoryModel
import ru.medyannikov.newsapi.core.ui.adapter.BaseViewHolder
import ru.medyannikov.newsapi.filter.core.ui.SelectableAdapter

class CategoryAdapter(private val clickListener: (item: CategoryModel) -> Unit) :
    SelectableAdapter<CategoryModel, CategoryAdapter.SelectableViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectableViewHolder =
        SelectableViewHolder(parent)

    override fun onBindViewHolder(holder: SelectableViewHolder, position: Int) =
        holder.onBind(getItem(position))

    inner class SelectableViewHolder(parent: ViewGroup) :
        BaseViewHolder<CategoryModel>(R.layout.item_selecteble, parent) {

        override fun onBind(item: CategoryModel) {
            val title = item.toString()
            tvTitle.text = title
            val isSelected = containInSelected(item)
            if (isSelected) {
                itemView.setBackgroundResource(R.drawable.bg_selecteble_active)
                tvTitle.setTextColor(ContextCompat.getColor(itemView.context, R.color.color_white))
            } else {
                itemView.setBackgroundResource(R.drawable.bg_selecteble)
                tvTitle.setTextColor(ContextCompat.getColor(itemView.context, R.color.color_text))
            }

            itemView.setOnClickListener { clickListener(item) }
        }
    }
}