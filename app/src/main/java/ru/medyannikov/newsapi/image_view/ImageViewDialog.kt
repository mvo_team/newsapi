package ru.medyannikov.newsapi.image_view

import android.app.Dialog
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.dialog_view_photo.*
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.view_common_loading.*
import ru.medyannikov.newsapi.R

class ImageViewDialog : DialogFragment() {

    private var url: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_view_photo, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.NewsApiTheme)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.attributes?.windowAnimations = android.R.style.Animation_Dialog

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        val url = arguments?.getString(URL)
        showPhoto(url)
    }


    private fun showPhoto(url: String?) {
        url?.let {
            pvPhotoDetail.loadImage(it)
            errorWrapper.isVisible = false
        } ?: run {
            showError()
        }
    }

    private fun initViews() {
        pvPhotoDetail.setOnPhotoTapListener { _, _, _ -> finishDialog() }
        clTouchGuard.setOnClickListener { finishDialog() }
        initStatusBar()
        initErrorPlaceHolder()
    }

    private fun showLoading(isLoading: Boolean) {
        progressBar?.isVisible = isLoading
    }

    private fun showError() {
        errorWrapper?.isVisible = true
    }

    private fun finishDialog() = this.dismiss()

    private fun initStatusBar() {
        dialog?.window?.decorView?.apply {
            val uiOptions = View.SYSTEM_UI_FLAG_VISIBLE
            systemUiVisibility = uiOptions
        }
    }

    private fun initErrorPlaceHolder() {
        btnRetry.setOnClickListener {
            errorWrapper?.isVisible = false
            reloadImage()
        }
    }

    private fun reloadImage() {
        showPhoto(url)
    }

    private fun ImageView.loadImage(imageUri: String) {
        Glide.with(context)
            .load(imageUri)
            .listener(object : RequestListener<Drawable?> {
                override fun onResourceReady(
                    resource: Drawable?, model: Any?, target: Target<Drawable?>?,
                    dataSource: DataSource?, isFirstResource: Boolean
                ): Boolean {
                    showLoading(false)
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?, model: Any?, target: Target<Drawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    showLoading(false)
                    showError()
                    return false
                }
            })
            .into(this)
    }

    companion object {
        const val TAG = "VIEW_PHOTO_DIALOG"

        private const val URL = "url_extra"

        private fun formBundle(url: String?): Bundle = Bundle().apply {
            putString(URL, url)
        }

        fun newInstance(url: String?): ImageViewDialog = ImageViewDialog().apply {
            arguments = formBundle(url)
        }
    }
}