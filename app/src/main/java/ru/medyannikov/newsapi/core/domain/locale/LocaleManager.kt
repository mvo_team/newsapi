package ru.medyannikov.newsapi.core.domain.locale

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences
import ru.medyannikov.newsapi.utils.*
import java.util.*
import javax.inject.Inject

class LocaleManager
@Inject constructor(private val context: Context,
                    private val appPreference: AppPreferences) {

    private val newContext: Context get() = prepareLocale()

    var language
        get() = appPreference.userLanguage
        set(value) {
            appPreference.userLanguage = value
        }

    fun getString(@StringRes res: Int, vararg arguments: Any = emptyArray()): String {
        return newContext.getString(res, *arguments)
    }

    fun getQuantityString(@PluralsRes res: Int, quantity: Int, vararg arguments: Any = emptyArray()): String {
        return newContext.resources.getQuantityString(res, quantity, *arguments)
    }

    private fun prepareLocale(): Context {
        val config = context.resources.configuration
        val sysLocale: Locale? = config.getCurrentLocale()
        return try {
            if (language != "" && sysLocale?.language != language) {
                val locale = Locale(language)
                Locale.setDefault(locale)
                val res = context.resources
                val cfg = Configuration(res.configuration)
                cfg.setSystemLocale(language)
                context.createConfigurationContext(cfg)
            } else {
                context
            }
        } catch (e: Exception) {
            context
        }
    }

    fun setNewLocale(language: String): String {
        this.language = language
        val config = context.resources.configuration
        val sysLocale: Locale? = config.getCurrentLocale()
        if (language != "" && sysLocale?.language != language) {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val res = context.resources
            val cfg = Configuration(res.configuration)
            cfg.setSystemLocale(language)
            context.createConfigurationContext(cfg)
            return language
        }
        return this.language ?: "en"
    }

    companion object {

        fun setupLanguage(context: Context) {
            val preference = context.getSharedPreferences(Const.PREFS_NAME, Context.MODE_PRIVATE)
            if (!preference.contains("user_language")) {
                val config = Resources.getSystem().configuration
                val sysLocale: Locale? = config.getCurrentLocale()
                val code = when (sysLocale?.language) {
                    Locale("ru").language -> "ru"
                    Locale("en").language -> "en"
                    Locale("es").language -> "es"
                    else -> "en"
                }
                preference.edit().putString("user_language", code).apply()
            }
        }

        @SuppressLint("ObsoleteSdkInt")
        fun wrap(context: Context?): Context? = if (context != null) {
            val language = context.getUserLanguage()
            val config = context.resources.configuration
            config.setSystemLocale(language)
            config?.setupDensity(context)
            context.createConfigurationContext(config)
        } else {
            context
        }
    }
}