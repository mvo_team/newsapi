package ru.medyannikov.newsapi.core.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes

abstract class BaseViewHolder<T>(@LayoutRes itemLayout: Int, parent: ViewGroup)
    : BaseLayoutContainer(LayoutInflater.from(parent.context).inflate(itemLayout, parent, false)) {

    abstract fun onBind(item: T)
}