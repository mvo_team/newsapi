package ru.medyannikov.newsapi.core.data.network.api.sources

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap
import ru.medyannikov.newsapi.core.data.network.response.SourceReponse

interface SourcesApi {

    @GET(SOURCES)
    fun getAvailableSources(@QueryMap params: Map<String, String>): Single<SourceReponse>

    companion object {

        private const val SOURCES = "/v2/sources"
    }
}