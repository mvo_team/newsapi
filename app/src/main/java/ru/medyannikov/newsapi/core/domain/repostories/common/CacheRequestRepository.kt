package ru.medyannikov.newsapi.core.domain.repostories.common

abstract class CacheRequestRepository<KEY, VALUE> {

    private val cache: HashMap<KEY, VALUE> = hashMapOf()

    fun existsData(key: KEY) : Boolean = cache.contains(key)

    fun putData(key: KEY, value: VALUE) {
        cache[key] = value
    }

    fun getCached(key: KEY) = cache[key]
}