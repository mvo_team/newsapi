package ru.medyannikov.newsapi.core.ui.adapter.delegates.article

import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_article_card.*
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.utils.date.toReadableDate
import ru.medyannikov.newsapi.utils.ui.loadImageRoundedCorners

class ArticleCardViewHolder(override val containerView: View,
    private val listener: (item: ArticleModel) -> Unit) : RecyclerView.ViewHolder(containerView), LayoutContainer {

  fun onBind(item: ArticleModel) {
    ivArticlePreview.loadImageRoundedCorners(item.urlToImage)
    item.source?.name?.let { source ->
      tvArticleSource.isVisible = true
      tvArticleSource.text = source
    } ?: run { tvArticleSource.isVisible = false }

    tvArticleTitle.text = item.title
    tvArticleDescription.text = item.description
    tvArticleAuthor.text = item.author
    tvArticlePublishDate.text = item.publishedAt?.toReadableDate()

    itemView.setOnClickListener { listener(item) }
  }
}