package ru.medyannikov.newsapi.core.ui.aac

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import ru.medyannikov.newsapi.di.dagger.injectSingleton
import ru.medyannikov.newsapi.utils.aac.SingleLiveEvent

abstract class BaseViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val errorMessageFactory by injectSingleton { errorMessageFactory }

    protected val errorMessage by lazy { SingleLiveEvent<String>() }
    protected val isLoading by lazy { MutableLiveData<Boolean>() }
    private val showRequestErrorDialog by lazy { SingleLiveEvent<String>() }
    private val showUnauthorizedErrorDialog by lazy { SingleLiveEvent<Unit>() }
    private val showNeedUpgradeErrorDialog by lazy { SingleLiveEvent<Unit>() }
    private val showManyRequestErrorDialog by lazy { SingleLiveEvent<Unit>() }

    private val errorConsumer: (t: Throwable) -> Unit = {
        it.printStackTrace()
        parseThrowable(it) {
            errorMessage.postValue(it.message)
        }
    }

    fun getLoading(): LiveData<Boolean> = isLoading

    fun getErrorMessage(): LiveData<String> = errorMessage

    fun showRequestErrorDialog(): LiveData<String> = showRequestErrorDialog

    fun showManyRequestErrorDialog(): LiveData<Unit> = showManyRequestErrorDialog

    fun showUnauthorizedErrorDialog(): LiveData<Unit> = showUnauthorizedErrorDialog

    fun showNeedUpgradeErrorDialog(): LiveData<Unit> = showNeedUpgradeErrorDialog

    override fun onCleared() {
        compositeDisposable.clear()
    }

    protected fun parseThrowable(throwable: Throwable?, defaultAction: () -> Unit) {
        when (throwable) {
            is HttpException -> parseByCode(throwable)
            else -> defaultAction()
        }
    }

    private fun parseByCode(throwable: HttpException) {
        when (throwable.code()) {
            400 -> showRequestError(throwable)
            401 -> showUnauthorized()
            426 -> showNeedUpgrade()
            429 -> showToManyRequest()
        }
    }

    private fun showToManyRequest() {
        showManyRequestErrorDialog.value = Unit
    }

    private fun showNeedUpgrade() {
        showNeedUpgradeErrorDialog.value = Unit
    }

    private fun showUnauthorized() {
        showUnauthorizedErrorDialog.value = Unit
    }

    protected fun runOnLoadingBlock(action: () -> Unit) {
        isLoading.value = true
        action()
        isLoading.value = false
    }

    protected open fun showRequestError(throwable: Throwable) {
        val message = errorMessageFactory.getErrorMessage(throwable)
        showRequestErrorDialog.value = message
    }

    protected fun Disposable.addToDisposable() {
        compositeDisposable.add(this)
    }

    protected fun <T> Single<T>.withProgress() =
        doOnSubscribe { isLoading.postValue(true) }
            .doOnSuccess { isLoading.postValue(false) }
            .doOnError { isLoading.postValue(false) }

    /** Implement doOnSuccess action, errors will be printed, disposes with view model */
    protected fun <T> Observable<T>.baseSubscribe(doOnSuccess: (T) -> Unit = {}) {
        subscribe({ doOnSuccess(it) }, errorConsumer).addToDisposable()
    }

    /** Implement doOnSuccess action, errors will be printed, disposes with view model */
    protected fun <T> Single<T>.baseSubscribe(doOnSuccess: (T) -> Unit = {}) {
        subscribe({ doOnSuccess(it) }, errorConsumer).addToDisposable()
    }

    protected fun Completable.baseSubscribe(doOnSuccess: () -> Unit = {}) {
        subscribe({ doOnSuccess() }, errorConsumer).addToDisposable()
    }
}