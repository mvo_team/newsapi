package ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.medyannikov.newsapi.filter.core.ui.SelectableAdapter


open class BaseAdapter<T> : ListAdapter<T, RecyclerView.ViewHolder>(SelectableAdapter.DiffCallback()) where T : Any {

    private val adapterDelegateManager: AdapterDelegateManager<T> = AdapterDelegateManager()

    fun addDelegate(del: AdapterDelegate<*>): BaseAdapter<T> {
        adapterDelegateManager.addDelegate(del as AdapterDelegate<T>)
        return this
    }

    fun setData(list: List<T>) = submitList(list)

    fun addData(list: List<T>) {
        val newList = currentList.plus(list)
        submitList(newList)
    }

    fun updateItem(item: T, position: Int) {
        val temp = currentList.toMutableList()
        temp[position] = item
        val newList = temp.toList()
        submitList(newList)
    }

    fun remove(position: Int) {
        val newList = currentList.toMutableList()
        newList.removeAt(position)
        submitList(newList)
    }

    open fun indexOf(item: T): Int = currentList.indexOf(item)

    fun getItems() = currentList.toList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return adapterDelegateManager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        adapterDelegateManager.onBindViewHolder(getItem(position), position, holder)
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        adapterDelegateManager.onViewRecycled(holder)
    }

    override fun getItemViewType(position: Int): Int {
        return adapterDelegateManager.getItemViewType(getItem(position), position)
    }

    fun getItemByPosition(position: Int): T? = currentList?.toList()?.getOrNull(position)
}

typealias DiffUtilCreator<Any> = (oldData: List<Any>, newData: List<Any>) -> DiffUtil.Callback