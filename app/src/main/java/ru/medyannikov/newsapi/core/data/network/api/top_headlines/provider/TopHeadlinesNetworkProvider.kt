package ru.medyannikov.newsapi.core.data.network.api.top_headlines.provider

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.response.ArticlesResponse

interface TopHeadlinesNetworkProvider {

    fun getTopHeadlines(page: Int = 1, params: Map<String, String>) : Single<ArticlesResponse>
}