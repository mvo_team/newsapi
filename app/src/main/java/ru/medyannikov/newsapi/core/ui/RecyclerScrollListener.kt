package ru.medyannikov.newsapi.core.ui

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class RecyclerScrollListener : RecyclerView.OnScrollListener() {
        private val scrollBus = PublishSubject.create<Int>()
        var lastVisibleItem: Int = 0
        var totalItem: Int = 0

        val scrolledToTheEndObservable: Observable<Int>
            get() = scrollBus
                    .distinctUntilChanged()
                    .filter { totalItem > 0 && lastVisibleItem >= totalItem - 3 }
                    .throttleFirst(300, TimeUnit.MILLISECONDS)

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            lastVisibleItem = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
            totalItem = recyclerView.adapter?.itemCount ?: 0
            if (dy > 0) {
                scrollBus.onNext(lastVisibleItem)
            }
        }
    }