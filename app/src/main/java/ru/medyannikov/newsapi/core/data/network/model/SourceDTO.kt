package ru.medyannikov.newsapi.core.data.network.model

import com.google.gson.annotations.SerializedName

data class SourceDTO(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: String? = null
)