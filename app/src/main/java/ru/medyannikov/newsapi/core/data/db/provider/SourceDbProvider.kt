package ru.medyannikov.newsapi.core.data.db.provider

import io.reactivex.Completable
import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.db.entity.SourceEntity

interface SourceDbProvider {

  fun getSources(): Single<List<SourceEntity>>

  fun insertSources(sources: List<SourceEntity>)

  fun clearSources(): Completable
}