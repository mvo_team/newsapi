package ru.medyannikov.newsapi.core.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ru.medyannikov.newsapi.core.data.db.entity.SourceEntity
import ru.medyannikov.newsapi.core.data.network.model.FullSourceDTO
import ru.medyannikov.newsapi.core.data.network.model.SourceDTO

@Parcelize
data class SourceModel(
        val id: String? = null,
        val name: String? = null,
        val description: String? = null,
        val url: String? = null,
        val category: String? = null,
        val language: String? = null,
        val country: String? = null) : Parcelable

fun SourceDTO.toModel(): SourceModel = SourceModel(id = this.id, name = this.name)

fun FullSourceDTO.toModel(): SourceModel = SourceModel(id = this.id,
        name = this.name,
        description = this.description,
        language = this.language,
        country = this.country,
        url = this.url,
        category = this.category)

fun SourceModel.toEntity(): SourceEntity {
  return SourceEntity(id = this.id ?: "",
          name = this.name,
          description = this.description,
          language = this.language,
          country = this.country,
          url = this.url,
          category = this.category)
}