package ru.medyannikov.newsapi.core.data.network.response

import com.google.gson.annotations.SerializedName
import ru.medyannikov.newsapi.core.data.network.model.ArticleDTO

data class ArticlesResponse(
    @SerializedName("status")
    val status: String?,
    @SerializedName("totalResults")
    val totalResults: Int = 0,
    @SerializedName("articles")
    val articles: List<ArticleDTO>
)