package ru.medyannikov.newsapi.core.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.medyannikov.newsapi.core.domain.model.SourceModel

@Entity(tableName = "source_table")
data class SourceEntity(
    @ColumnInfo(name = "id")
    @PrimaryKey val id: String,
    @ColumnInfo(name = "name") val name: String? = null,
    @ColumnInfo(name = "description") val description: String? = null,
    @ColumnInfo(name = "url") val url: String? = null,
    @ColumnInfo(name = "category") val category: String? = null,
    @ColumnInfo(name = "language") val language: String? = null,
    @ColumnInfo(name = "country") val country: String? = null)

fun SourceEntity.toModel(): SourceModel = SourceModel(id = this.id,
    name = this.name,
    description = this.description,
    language = this.language,
    country = this.country,
    url = this.url,
    category = this.category)