package ru.medyannikov.newsapi.core.domain.error

interface ErrorMessageFactory {

    /**
     * Возвращает сообщение на основании вызванного исключения
     */
    fun getErrorMessage(throwable: Throwable?) : String
}