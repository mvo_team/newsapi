package ru.medyannikov.newsapi.core.ui.android

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.core.ui.ViewModelFactory
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.di.dagger.injectSingleton
import ru.medyannikov.newsapi.main.navigation.MainScreenNavigator
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.utils.ui.hideKeyboard
import ru.medyannikov.newsapi.utils.ui.toast
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

abstract class BaseActivity: DaggerAppCompatActivity() {
    @Inject
    lateinit var vmFactory: ViewModelFactory

    @Inject
    lateinit var navigationMenuController: NavigationMenuController

    protected open val navigatorHolder: NavigatorHolder by injectSingleton { mainNavigatorHolder }
    protected open val navigator: SupportAppNavigator by lazy { MainScreenNavigator(this, R.id.container) }
    protected val compositeDisposable by lazy { CompositeDisposable() }
    protected val tag: String = javaClass.simpleName

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    override fun attachBaseContext(newBase: Context?) {
        val contextWrapper = LocaleManager.wrap(newBase)
        super.attachBaseContext(contextWrapper)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        hideKeyboard()
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        hideProgress()
        compositeDisposable.clear()
    }

    protected fun Disposable.disposeWithActivity() {
        addTo(compositeDisposable)
    }

    /** Implement doOnSuccess action, errors will be printed, disposes with activity */
    protected fun <T> Observable<T>.baseSubscribe(doOnSuccess: (T) -> Unit) {
        subscribe({ doOnSuccess(it) }, { it.printStackTrace() }).disposeWithActivity()
    }

    protected fun BaseViewModel.attachBaseListeners() {
        getLoading().subscribeProgress()
        getErrorMessage().observe(this@BaseActivity, Observer { toast(it) })
    }

    fun showProgress() {
    }

    fun hideProgress() {
    }

    private fun LiveData<Boolean>.subscribeProgress() {
        observe(this@BaseActivity, Observer {
            if(it) {
                showProgress()
            } else {
                hideProgress()
            }
        })
    }

    protected inline fun <reified VM : ViewModel> provideVm() = ViewModelProviders.of(this, vmFactory)[VM::class.java]
}