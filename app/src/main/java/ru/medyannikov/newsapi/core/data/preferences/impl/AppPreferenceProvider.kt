package ru.medyannikov.newsapi.core.data.preferences.impl

import android.content.Context
import android.content.SharedPreferences
import ru.medyannikov.newsapi.BuildConfig
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences
import ru.medyannikov.newsapi.utils.getCurrentLocale
import java.util.*
import javax.inject.Inject
import kotlin.reflect.KProperty

class AppPreferenceProvider
@Inject constructor(private val context: Context,
                    private val preference: SharedPreferences) : AppPreferences {

    init {

        if (! preference.contains("user_language")) {
            val config = context.resources.configuration
            val sysLocale: Locale? = config.getCurrentLocale()
            val code = when (sysLocale?.language) {
                Locale("ru").language -> {"ru"}
                Locale("en").language -> {"en"}
                Locale("es").language -> {"es"}
                else -> "en"
            }
            preference.edit().putString("user_language", code).apply()
        }

        if (!preference.contains("api_key")) {
            preference.edit().putString("api_key", BuildConfig.API_KEY).apply()
        }
    }

    override var userLanguage: String? by stringPref("user_language", "en")

    override var apiKey: String? by stringPref("api_key", BuildConfig.API_KEY)

    private fun stringPref(name: String, defaultValue: String? = null): SettingsDelegate<String?> {
        return SettingsDelegate(
                getter = { preference.getString(name, defaultValue) },
                setter = { preference.edit().putString(name, it).apply() }
        )
    }

    private inner class SettingsDelegate<T>(private val getter: () -> T,
                                            private val setter: (T) -> Unit) {

        operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
            return getter()
        }

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            setter(value)
        }
    }
}