package ru.medyannikov.newsapi.core.data.db.provider.impl

import io.reactivex.Completable
import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.db.NewsDatabase
import ru.medyannikov.newsapi.core.data.db.entity.SourceEntity
import ru.medyannikov.newsapi.core.data.db.provider.SourceDbProvider
import javax.inject.Inject

class SourceDbProviderImpl
@Inject constructor(private val db: NewsDatabase) : SourceDbProvider {
  override fun getSources(): Single<List<SourceEntity>> {
    return db.sourceDao().getAllSources()
  }

  override fun insertSources(sources: List<SourceEntity>) {
    return db.sourceDao().insertAll(sources)
  }

  override fun clearSources(): Completable {
    return db.sourceDao().deleteAll()
  }
}