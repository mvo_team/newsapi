package ru.medyannikov.newsapi.core.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.db.entity.SourceEntity

@Dao
interface SourceDao {

  @Query("SELECT * FROM source_table")
  fun getAllSources(): Single<List<SourceEntity>>

  @Insert
  fun insertAll(sources: List<SourceEntity>)

  @Query("DELETE FROM source_table")
  fun deleteAll(): Completable
}