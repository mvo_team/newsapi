package ru.medyannikov.newsapi.core.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ru.medyannikov.newsapi.core.data.db.dao.SourceDao
import ru.medyannikov.newsapi.core.data.db.entity.SourceEntity

@Database(entities = [SourceEntity::class], version = 1, exportSchema = true)
abstract class NewsDatabase: RoomDatabase() {

  companion object {
    private const val DB_NAME = "news_db"
    private var instance: NewsDatabase? = null

    @Synchronized
    fun getInstance(context: Context): NewsDatabase {
      if (instance == null) {
        instance = Room
            .databaseBuilder(context, NewsDatabase::class.java, DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
      }
      return instance as NewsDatabase
    }
  }

  abstract fun sourceDao(): SourceDao
}