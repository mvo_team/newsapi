package ru.medyannikov.newsapi.core.ui.android

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.ViewModelFactory
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.error_dialog.SimpleErrorDialog
import ru.medyannikov.newsapi.error_dialog.WrongRequestErrorDialog
import ru.medyannikov.newsapi.utils.ui.snackbar
import javax.inject.Inject

abstract class BaseFragment<VM> : Fragment() where VM : BaseViewModel {

    private val compositeDisposable by lazy { CompositeDisposable() }
    @Inject
    protected lateinit var vmFactory: ViewModelFactory
    protected lateinit var vm: VM

    private var upgradeDialog: SimpleErrorDialog? = null
    private var toManyRequestDialog: SimpleErrorDialog? = null
    private var wrongRequestDialog: WrongRequestErrorDialog? = null
    private var unauthorizedDialog: SimpleErrorDialog? = null

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    protected inline fun <reified VM : ViewModel> provideVm() = ViewModelProviders.of(this, vmFactory).get(VM::class.java)

    fun Disposable.disposeWithFragment() {
        compositeDisposable.add(this)
    }

    fun BaseViewModel.attachBaseListeners() {
        val fragment = this@BaseFragment
        getLoading().observe(this@BaseFragment, Observer {
            val activity = activity
            if (activity is BaseActivity) {
                if (it) {
                    activity.showProgress()
                } else {
                    activity.hideProgress()
                }
            }
        })

        showManyRequestErrorDialog().observe(this@BaseFragment, Observer {
            fragment.showManyRequestErrorDialog()
        })
        showNeedUpgradeErrorDialog().observe(this@BaseFragment, Observer {
            fragment.showNeedUpgradeErrorDialog()
        })
        showRequestErrorDialog().observe(this@BaseFragment, Observer {
            fragment.showRequestErrorDialog(it)
        })
        showUnauthorizedErrorDialog().observe(this@BaseFragment, Observer {
            fragment.showUnauthorizedErrorDialog()
        })

        getErrorMessage().observe(this@BaseFragment, Observer {
            activity?.snackbar(it)
        })
    }

    protected open fun openFilter() {}
    protected open fun openSettings() {}

    protected open fun showUnauthorizedErrorDialog() {
        if (unauthorizedDialog == null) {
            unauthorizedDialog = SimpleErrorDialog.Builder()
                    .setTitle(getString(R.string.dialog_need_authorize_title))
                    .setMessage(getString(R.string.dialog_need_authorize_message))
                    .setDescription(getString(R.string.dialog_need_authorize_description))
                    .setActionText(getString(R.string.open_settings))
                    .setAction { openSettings() }
                    .build()
        }
        if (unauthorizedDialog?.isVisible == false) {
            unauthorizedDialog?.show(childFragmentManager, "WRONG_AUTHORIZE")
        }
    }

    protected open fun showRequestErrorDialog(text: String?) {
        if (wrongRequestDialog == null) {
            wrongRequestDialog = WrongRequestErrorDialog.newInstance(text) { openFilter() }
        }
        if (wrongRequestDialog?.isVisible == false) {
            wrongRequestDialog?.setDescription(text)
            wrongRequestDialog?.show(childFragmentManager, "WRONG_REQUEST")
        }
    }

    protected open fun showNeedUpgradeErrorDialog() {
        if (upgradeDialog == null) {
            upgradeDialog = SimpleErrorDialog.Builder()
                    .setTitle(getString(R.string.dialog_need_upgrade_plan_title))
                    .setMessage(getString(R.string.dialog_need_upgrade_message))
                    .setDescription(getString(R.string.dialog_need_upgrade_description))
                    .setActionText(getString(R.string.dialog_need_upgrade_open_settings))
                    .setAction { openSettings() }
                    .build()
        }
        if (upgradeDialog?.isVisible == false) {
            upgradeDialog?.show(childFragmentManager, "UPGRADE_DIALOG")
        }
    }

    protected open fun showManyRequestErrorDialog() {
        if (toManyRequestDialog == null) {
            toManyRequestDialog = SimpleErrorDialog.Builder()
                    .setTitle(getString(R.string.dialog_to_many_request_title))
                    .setMessage(getString(R.string.dialog_to_many_request_message))
                    .setDescription(getString(R.string.dialog_to_many_request_description))
                    .setActionText(getString(R.string.dialog_to_many_request_open_settings))
                    .setAction { openSettings() }
                    .build()
        }
        if (toManyRequestDialog?.isVisible == false) {
            toManyRequestDialog?.show(childFragmentManager, "UPGRADE_DIALOG")
        }
    }
}