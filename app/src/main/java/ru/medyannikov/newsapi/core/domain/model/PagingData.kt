package ru.medyannikov.newsapi.core.domain.model

data class PagingData<T>(val items: List<T> = emptyList(),
                         val page: Int = 0,
                         val pageSize: Int = 20,
                         val totalCount: Int = 0,
                         val lastVisibleIndex: Int = -1,
                         val isFilterActivated: Boolean = false) {

    fun isEmpty(): Boolean = items.isEmpty() || totalCount == 0

    val totalPage: Int get() = totalCount / 20

    val isLast: Boolean get() = page == totalPage || page > totalPage
}