package ru.medyannikov.newsapi.core.data.network.api.top_headlines

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import ru.medyannikov.newsapi.core.data.network.response.ArticlesResponse

interface TopHeadlinesApi {

    @GET(TOP_HEADLINES)
    fun getHeadLines(@Query("page") page: Int = 1, @QueryMap params: Map<String, String>): Single<ArticlesResponse>

    companion object {
        const val TOP_HEADLINES = "/v2/top-headlines"
    }
}