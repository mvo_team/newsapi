package ru.medyannikov.newsapi.core.data.network.api.sources.provider.impl

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.api.sources.SourcesApi
import ru.medyannikov.newsapi.core.data.network.api.sources.provider.SourceNetworkProvider
import ru.medyannikov.newsapi.core.data.network.response.SourceReponse
import javax.inject.Inject

class SourceNetworkProviderImpl
@Inject constructor(private val api: SourcesApi) : SourceNetworkProvider {

    override fun getAvailableSources(params: Map<String, String>): Single<SourceReponse> {
        return api.getAvailableSources(params)
    }
}