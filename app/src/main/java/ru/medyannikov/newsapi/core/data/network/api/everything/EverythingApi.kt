package ru.medyannikov.newsapi.core.data.network.api.everything

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import ru.medyannikov.newsapi.core.data.network.response.ArticlesResponse

interface EverythingApi {

    @GET(EVERYTHING)
    fun getEveryting(@Query("page") page: Int, @QueryMap params: Map<String, String>): Single<ArticlesResponse>

    companion object {
        const val EVERYTHING = "/v2/everything"
    }
}