package ru.medyannikov.newsapi.core.data.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences

class HeaderInterceptor(private val appPrefs: AppPreferences) : Interceptor {

    private val apiKey get() = appPrefs.apiKey

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val requestBuilder = original.newBuilder()
                .addHeader("X-Device-Platform", "Android")
        apiKey?.let { requestBuilder.addHeader("X-Api-Key", it) }

        return chain.proceed(requestBuilder.build())
    }
}