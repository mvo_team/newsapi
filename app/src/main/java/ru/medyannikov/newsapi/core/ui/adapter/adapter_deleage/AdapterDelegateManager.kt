package ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage

import android.util.SparseArray
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.util.*



class AdapterDelegateManager<T> {

  private val delegates: SparseArray<AdapterDelegate<T>> = SparseArray()

  fun addDelegate(delegate: AdapterDelegate<T>): AdapterDelegateManager<T> {
    delegates.put(delegates.size(), delegate)
    return this
  }

  fun getItemViewType(items: T, position: Int): Int {
    for (i in 0 until delegates.size()) {
      val delegate = delegates.valueAt(i)
      if (delegate.isForViewType(items, position)) {
        return delegates.keyAt(i)
      }
    }
    throw NoSuchElementException(
        "No AdapterDelegate added that matches position=$position in data source")
  }

  fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    return getDelegateForViewType(viewType).onCreateViewHolder(parent)
  }

  fun onBindViewHolder(item: T, position: Int, viewHolder: RecyclerView.ViewHolder) {
    getDelegateForViewType(viewHolder.itemViewType).onBindViewHolder(item, position, viewHolder)
  }

  fun onViewRecycled(viewHolder: RecyclerView.ViewHolder) {
    getDelegateForViewType(viewHolder.itemViewType).onViewRecycled(viewHolder)
  }

  private fun getDelegateForViewType(viewType: Int): AdapterDelegate<T> = delegates.get(viewType)
}