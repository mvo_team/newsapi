package ru.medyannikov.newsapi.core.data.network.api.everything.provider.impl

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.api.everything.EverythingApi
import ru.medyannikov.newsapi.core.data.network.api.everything.provider.EverythingNetworkProvider
import ru.medyannikov.newsapi.core.data.network.response.ArticlesResponse
import ru.medyannikov.newsapi.feed.everything.domain.params.EverythingParams
import javax.inject.Inject

class EverythingNetworkProviderImpl
@Inject constructor(private val api: EverythingApi) : EverythingNetworkProvider {

  override fun getEverythingArticles(page: Int, params: EverythingParams): Single<ArticlesResponse> {
    return api.getEveryting(page, params.toMap())
  }
}