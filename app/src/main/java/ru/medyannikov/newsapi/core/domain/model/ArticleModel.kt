package ru.medyannikov.newsapi.core.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ru.medyannikov.newsapi.core.data.network.model.ArticleDTO

@Parcelize
data class ArticleModel(
    val publishedAt: String? = null,
    val author: String? = null,
    val urlToImage: String? = null,
    val description: String? = null,
    val source: SourceModel? = null,
    val title: String? = null,
    val url: String? = null,
    val content: String? = null,
    var isFavourite: Boolean = false) : Parcelable

fun ArticleDTO.toModel(): ArticleModel = ArticleModel(publishedAt = this.publishedAt,
    title = this.title,
    description = this.description,
    source = this.source?.toModel(),
    author = this.author,
    url = this.url,
    urlToImage = this.urlToImage,
    content = this.content)