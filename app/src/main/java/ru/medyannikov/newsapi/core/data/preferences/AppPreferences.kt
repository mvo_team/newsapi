package ru.medyannikov.newsapi.core.data.preferences

interface AppPreferences {

    var userLanguage: String?

    var apiKey: String?
}