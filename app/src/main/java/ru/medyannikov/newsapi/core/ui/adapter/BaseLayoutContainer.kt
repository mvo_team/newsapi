package ru.medyannikov.newsapi.core.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

abstract class BaseLayoutContainer(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer