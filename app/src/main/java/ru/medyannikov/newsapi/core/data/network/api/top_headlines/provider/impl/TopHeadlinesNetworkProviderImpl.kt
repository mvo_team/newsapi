package ru.medyannikov.newsapi.core.data.network.api.top_headlines.provider.impl

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.api.top_headlines.TopHeadlinesApi
import ru.medyannikov.newsapi.core.data.network.api.top_headlines.provider.TopHeadlinesNetworkProvider
import ru.medyannikov.newsapi.core.data.network.response.ArticlesResponse
import javax.inject.Inject

class TopHeadlinesNetworkProviderImpl
@Inject constructor(private val api: TopHeadlinesApi) : TopHeadlinesNetworkProvider {

    override fun getTopHeadlines(page: Int, params: Map<String, String>): Single<ArticlesResponse> {
        return api.getHeadLines(page, params)
    }
}