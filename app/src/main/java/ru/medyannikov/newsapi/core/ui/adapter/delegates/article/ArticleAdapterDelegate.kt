package ru.medyannikov.newsapi.core.ui.adapter.delegates.article

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.AdapterDelegate

class ArticleAdapterDelegate(private val listener: (item: ArticleModel) -> Unit) : AdapterDelegate<Any>() {

    override fun isForViewType(item: Any, position: Int): Boolean = item is ArticleModel

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_article_card, parent, false)
        return ArticleCardViewHolder(view, listener)
    }

    override fun onBindViewHolder(item: Any, position: Int, holder: RecyclerView.ViewHolder) {
        item as ArticleModel
        holder as ArticleCardViewHolder

        holder.onBind(item)
    }
}