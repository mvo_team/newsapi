package ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class AdapterDelegate<in T> {

  abstract fun isForViewType(item: T, position: Int): Boolean

  abstract fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

  abstract fun onBindViewHolder(item: T, position: Int, holder: RecyclerView.ViewHolder)

  open fun onViewRecycled(holder: RecyclerView.ViewHolder) {

  }

}