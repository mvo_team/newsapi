package ru.medyannikov.newsapi.core.data.network.response

import com.google.gson.annotations.SerializedName
import ru.medyannikov.newsapi.core.data.network.model.FullSourceDTO

data class SourceReponse(
        @SerializedName("status")
        val status: String?,
        @SerializedName("totalResults")
        val totalResults: Int = 0,
        @SerializedName("sources")
        val sources: List<FullSourceDTO>
)