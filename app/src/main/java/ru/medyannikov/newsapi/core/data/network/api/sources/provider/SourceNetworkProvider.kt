package ru.medyannikov.newsapi.core.data.network.api.sources.provider

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.response.SourceReponse

interface SourceNetworkProvider {

    fun getAvailableSources(params: Map<String, String>): Single<SourceReponse>
}