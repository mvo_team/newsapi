package ru.medyannikov.newsapi.core.data.network.api.everything.provider

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.response.ArticlesResponse
import ru.medyannikov.newsapi.feed.everything.domain.params.EverythingParams

interface EverythingNetworkProvider {

  fun getEverythingArticles(page: Int = 1, params: EverythingParams) : Single<ArticlesResponse>
}