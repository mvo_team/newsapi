package ru.medyannikov.newsapi.core.ui.adapter.delegates.article

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.AdapterDelegate

class ErrorAdapterDelegate: AdapterDelegate<Any>() {
    override fun isForViewType(item: Any, position: Int): Boolean = item is ErrorItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_not_found, parent, false)
        return object : RecyclerView.ViewHolder(view) {}
    }

    override fun onBindViewHolder(item: Any, position: Int, holder: RecyclerView.ViewHolder) {
        // nothing
    }
}

object ErrorItem