package ru.medyannikov.newsapi.core.domain.model

data class SearchEvent(val query: String? = null)