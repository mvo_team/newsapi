package ru.medyannikov.newsapi.core.domain.error.impl

import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.error.ErrorMessageFactory
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.net.ssl.SSLException

class SystemErrorMessageFactory
@Inject constructor(private val localeManager: LocaleManager) : ErrorMessageFactory {

    override fun getErrorMessage(throwable: Throwable?): String {
        return when (throwable) {
            null -> null
            is SocketTimeoutException -> localeManager.getString(R.string.error_socket_timeout_message)
            is SSLException -> localeManager.getString(R.string.error_ssl_exception_message)
            is HttpException -> handleHttpException(throwable)
            is UnknownHostException -> localeManager.getString(R.string.error_unknown_host_message)
            else -> throwable.message
        } ?: localeManager.getString(R.string.error_common_message)
    }

    private fun handleHttpException(error: HttpException): String? {
        val body = error.response().errorBody()
        val error500 = get4xx5xxErrorMessage(body, error)
        return error500 ?: try {
            JSONObject(body?.string()).getString("message")
        } catch (e: IOException) {
            error.message
        } catch (e: JSONException) {
            error.message
        }
    }

    private fun get4xx5xxErrorMessage(body: ResponseBody?, exception: HttpException): String? {
        if (exception.code() >= 400) {
            var errorMessage: String?
            errorMessage = try {
                val jsonObject = JSONObject(body?.string())
                val message = jsonObject.getString("message")
                when (jsonObject.getString("code")) {
                    "parametersMissing" -> return localeManager.getString(R.string.error_wrong_request)
                    "parametersIncompatible" -> return localeManager.getString(R.string.error_incompatible_request)
                    else -> message
                }
            } catch (e: IOException) {
                exception.message
            } catch (e: JSONException) {
                exception.message
            }

            val friendlyMessage = getUserFriendlyMessage(exception.code())
            if (friendlyMessage != null && errorMessage == null) {
                errorMessage = friendlyMessage
            }

            return errorMessage
        }
        return null
    }

    private fun getUserFriendlyMessage(code: Int): String? {
        return when (code) {
            400 -> localeManager.getString(R.string.error_400)
            401 -> localeManager.getString(R.string.error_401)
            403 -> localeManager.getString(R.string.error_403)
            404 -> localeManager.getString(R.string.error_404)
            500, 502 -> localeManager.getString(R.string.error_500)
            503 -> localeManager.getString(R.string.error_503)
            504 -> localeManager.getString(R.string.error_504)
            else -> null
        }
    }
}