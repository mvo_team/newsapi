package ru.medyannikov.newsapi.launch

import android.os.Bundle
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.core.ui.android.BaseActivity

class LaunchActivity : BaseActivity(), Injectable {

    lateinit var vm: LaunchViewModel

    override fun getLayoutId(): Int = R.layout.activity_launch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = provideVm()

        if (savedInstanceState == null) {
            vm.openMainScreen()
        }
    }
}