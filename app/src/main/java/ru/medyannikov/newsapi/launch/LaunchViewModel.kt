package ru.medyannikov.newsapi.launch

import io.reactivex.Single
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LaunchViewModel
@Inject constructor(private val router: MainRouter,
                    private val schedulerProvider: SchedulerProvider) : BaseViewModel() {

    fun openMainScreen() {
        Single.timer(2, TimeUnit.SECONDS)
            .observeOn(schedulerProvider.ui())
            .baseSubscribe { router.openMainScreen() }
    }
}