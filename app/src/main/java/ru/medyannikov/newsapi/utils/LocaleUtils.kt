package ru.medyannikov.newsapi.utils

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*

fun Configuration.getCurrentLocale(): Locale? {
  return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    getSystemLocale()
  } else {
    getSystemLocaleLegacy()
  }
}

fun Configuration.setSystemLocale(language: String?) {
  val locale = Locale(language)
  Locale.setDefault(locale)
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    setSystemLocale(locale)
  } else {
    setSystemLocaleLegacy(locale)
  }
}

fun Context.getUserLanguage(): String? {
  val prefs = getSharedPreferences(Const.PREFS_NAME, Context.MODE_PRIVATE)
  return prefs.getString("user_language", "en")
}

private fun Configuration.getSystemLocaleLegacy(): Locale {
  return locale
}

@RequiresApi(Build.VERSION_CODES.N)
private fun Configuration.getSystemLocale(): Locale {
  return locales.get(0)
}

private fun Configuration.setSystemLocaleLegacy(locale: Locale) {
  this.locale = locale
}

@TargetApi(Build.VERSION_CODES.N)
private fun Configuration.setSystemLocale(locale: Locale) {
  setLocale(locale)
}