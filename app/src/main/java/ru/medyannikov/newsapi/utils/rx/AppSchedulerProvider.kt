package ru.medyannikov.newsapi.utils.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.schedulers.SingleScheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppSchedulerProvider @Inject constructor() : SchedulerProvider {
    override fun ui(): Scheduler = AndroidSchedulers.mainThread()
    override fun io() = Schedulers.io()
    override fun computation() = Schedulers.computation()
    override fun newThread() = Schedulers.newThread()
    override fun single() = SingleScheduler()
    override fun commonSingle() = Schedulers.single()
}