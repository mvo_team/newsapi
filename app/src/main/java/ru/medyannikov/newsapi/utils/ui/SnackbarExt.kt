package ru.medyannikov.newsapi.utils.ui

import android.app.Activity
import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import ru.medyannikov.newsapi.R


fun View?.snackbar(message: CharSequence?, @Snackbar.Duration duration: Int = Snackbar.LENGTH_SHORT) = this?.let { view ->
    message?.let { text -> Snackbar.make(view, text, duration).show() }
}

fun View?.snackbar(@StringRes textRes: Int, @Snackbar.Duration duration: Int = Snackbar.LENGTH_SHORT) =
        this?.let { Snackbar.make(it, textRes, duration).show() }

/** Ordinary info snack */
fun Activity?.snackbar(@StringRes msg: Int) = this?.let {
    Snackbar.make(this.window.decorView, msg, Snackbar.LENGTH_LONG).show()
}

fun Activity?.snackbar(msg: String) = this?.let {
    Snackbar.make(this.findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG).show()
}

fun Activity.showErrorSnack(throwable: Throwable) {
    showErrorSnack(throwable, this)
}

private fun showErrorSnack(throwable: Throwable,
                           activity: Activity,
                           msgRes: Int = R.string.error_network,
                           action: Int = R.string.ok,
                           actionListener: (() -> Unit) = {}, // dismiss by default
                           snackLength: Int = Snackbar.LENGTH_INDEFINITE) {
    Snackbar.make(activity.findViewById(android.R.id.content),
                  throwable.message ?: activity.getString(msgRes),
                  snackLength).run {
        setAction(action) { actionListener.invoke() }
        show()
    }
}