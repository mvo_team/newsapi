package ru.medyannikov.newsapi.utils.ui

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.annotation.IntDef
import androidx.annotation.StringRes
import ru.medyannikov.newsapi.utils.ui.DurationToast.LENGTH_LONG
import ru.medyannikov.newsapi.utils.ui.DurationToast.LENGTH_SHORT

fun View?.toast(text: CharSequence?, @Duration duration: Int = LENGTH_SHORT) = this?.context?.toast(text, duration)

fun View?.toast(@StringRes text: Int, @Duration duration: Int = LENGTH_SHORT) = this?.context?.toast(text, duration)

fun Context?.toast(text: CharSequence?, @Duration duration: Int = LENGTH_SHORT) = this?.let { ctx ->
    text?.let { message ->
        Toast.makeText(ctx, message, duration).show()
    }
}

fun Context?.toast(@StringRes text: Int, @Duration duration: Int = LENGTH_SHORT) = this?.let {
    Toast.makeText(it, text, duration).show()
}

@IntDef(LENGTH_SHORT, LENGTH_LONG)
@Retention(AnnotationRetention.SOURCE)
annotation class Duration

object DurationToast {
    const val LENGTH_LONG = Toast.LENGTH_LONG
    const val LENGTH_SHORT = Toast.LENGTH_SHORT
}