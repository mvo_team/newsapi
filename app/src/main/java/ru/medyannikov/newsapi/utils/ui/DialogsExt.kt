package ru.medyannikov.newsapi.utils.ui

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import ru.medyannikov.newsapi.BuildConfig.VERSION_CODE
import ru.medyannikov.newsapi.BuildConfig.VERSION_NAME
import ru.medyannikov.newsapi.R

@Throws(Exception::class)
fun Context.sendRateUsIntent() {
    try {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
    } catch (e: ActivityNotFoundException) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=$packageName"))
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        } else {
            throw Exception()
        }
    }
}

fun Context.sendShareIntent(title: String, message: String) {
    val intent = Intent(Intent.ACTION_SEND)
            .putExtra(Intent.EXTRA_TEXT, message)
            .setType("text/plain")

    startActivity(Intent.createChooser(intent, title))
}

fun Context.sendEmailWithThemeIntent(): Intent {
    val sb = StringBuilder()
    sb.append("\n").append(getString(R.string.mail_info_developer)).append("\n")
    if (Build.BRAND != null) {
        sb.append(Build.BRAND).append(' ')
    } else if (Build.MANUFACTURER != null) {
        sb.append(Build.MANUFACTURER).append(' ')
    }
    if (Build.DEVICE != null) {
        sb.append(Build.DEVICE).append('\n')
    } else if (Build.PRODUCT != null) {
        sb.append(Build.PRODUCT).append('\n')
    } else if (Build.MODEL != null) {
        sb.append(Build.MODEL).append('\n')
    }
    if (Build.VERSION.RELEASE != null) {
        sb.append("Android ").append(Build.VERSION.RELEASE).append('\n')
    }
    sb.append("App version ")
            .append(VERSION_NAME)
            .append(" (")
            .append(VERSION_CODE)
            .append(")")
            .append("\n\n")
            .append("\n")
            .append(getString(R.string.mail_your_message))
            .append("\n\n")

    val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "mvo.developer@gmail.com", null))
    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.support_theme))
    intent.putExtra(Intent.EXTRA_TEXT, sb.toString())
    return Intent.createChooser(intent, getString(R.string.send_email))
}