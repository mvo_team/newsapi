package ru.medyannikov.newsapi.utils.rx

import io.reactivex.Scheduler

interface SchedulerProvider {
    fun ui(): Scheduler
    fun io(): Scheduler
    fun computation(): Scheduler
    fun newThread(): Scheduler
    fun single(): Scheduler
    fun commonSingle(): Scheduler
}