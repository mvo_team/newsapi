package ru.medyannikov.newsapi.utils.ui

/**
 * Интерфейс обработчика нажатий на кнопку Back.
 */
interface OnBackPressedHandler {
    /**
     * Метод вызывается, когда нажимается клавиша Back
     *
     * @return True, если хендлер обработал событие, иначе - false.
     */
    fun onBackPressed(): Boolean
}