package ru.medyannikov.newsapi.utils.date

import android.util.Log
import org.threeten.bp.DateTimeException
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit

object DateHelper {

    private const val DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    private const val HOUR_IN_MILLIS = (60 * 60 * 1000).toLong()
    private const val DAY_IN_MILLIS = 24 * HOUR_IN_MILLIS

    private const val DASH_DATE_FORMAT = "yyyy-MM-dd"
    private const val DOTTED_DATE_FORMAT = "dd.MM.yyyy"
    private const val READABLE_DATE_FORMAT = "dd MMM, yyyy"
    private const val DOTTED_SHORT_DATE_FORMAT = "dd.MM"
    private const val DOTTED_TIME_FORMAT = "HH:mm"

    private val RUSSIAN_LOCALE = Locale("ru", "RU")
    private val EN_LOCALE = Locale("en", "EN")

    private val tag: String = javaClass.simpleName

    var dateFormatDottedPattern = DateTimeFormatter.ofPattern(DOTTED_DATE_FORMAT).withLocale(EN_LOCALE)
    var readableDateFormatter = DateTimeFormatter.ofPattern(READABLE_DATE_FORMAT).withLocale(EN_LOCALE)
    var dateTimeFormatFull = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT).withLocale(EN_LOCALE)
    var dateFormatShortDottedPattern = DateTimeFormatter.ofPattern(DOTTED_SHORT_DATE_FORMAT).withLocale(EN_LOCALE)
    var dateFormatDashPattern = DateTimeFormatter.ofPattern(DASH_DATE_FORMAT).withLocale(EN_LOCALE)
    var dateTimeFormatDashPattern = DateTimeFormatter.ofPattern(DOTTED_TIME_FORMAT).withLocale(EN_LOCALE)


    fun timeDiffToDays(timeDiff: Long): Long {
        return timeDiff / DAY_IN_MILLIS
    }

    fun getFullFormatDate(localDate: LocalDate?): String? {
        val localDateTime = LocalDateTime.of(localDate, LocalTime.now())
        return localDateTime?.format(dateTimeFormatFull)
    }

    fun parseFullFormatDate(dateString:String): LocalDateTime {
        return LocalDateTime.parse(dateString, dateTimeFormatFull)
    }

    fun parseDate(text: String?, formatter: DateTimeFormatter = dateFormatDottedPattern): LocalDate? {
        return try {
            LocalDate.parse(text, formatter)
        } catch (e: DateTimeException) {
            Log.v(tag, "Parse date exception", e)
            null
        }
    }

    /**
     * Форматирование для вывода затраченного времени в формате "минуты:секунды"
     *
     * @param time затраченное время в миллисекундах
     * @return отформатированная строка
     */
    fun formatTotalTimeMMSS(time: Long): String {
        return formatTotalTimeMMSS(Date(time))
    }

    /**
     * Форматирование для вывода затраченного времени в формате "минуты:секунды"
     *
     * @param date затраченное время
     * @return отформатированная строка
     */
    private fun formatTotalTimeMMSS(date: Date?): String {
        if (date == null) {
            return "00:00"
        }
        var millis = date.time
        val minutes = TimeUnit.MILLISECONDS.toMinutes(millis)
        millis -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(millis)
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
    }

    /**
     * Сранивает только год, месяц и день
     *
     * @param a первая дата
     * @param b вторая дата
     * @return результат в виде compareTo() (-1, 0, 1)
     */
    private fun compareDate(a: Calendar, b: Calendar): Int {
        if (a.get(Calendar.YEAR) < b.get(Calendar.YEAR)) return -1
        if (a.get(Calendar.YEAR) > b.get(Calendar.YEAR)) return +1
        if (a.get(Calendar.MONTH) < b.get(Calendar.MONTH)) return -1
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH)) return +1
        if (a.get(Calendar.DAY_OF_MONTH) < b.get(Calendar.DAY_OF_MONTH)) return -1
        return if (a.get(Calendar.DAY_OF_MONTH) > b.get(Calendar.DAY_OF_MONTH)) +1 else 0
    }
}