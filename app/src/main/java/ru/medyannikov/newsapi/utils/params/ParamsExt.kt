package ru.medyannikov.newsapi.utils.params

fun MutableMap<String, String>.putNotNull(key: String, value: Boolean?) = putNotNull(key) { value?.toString() }

fun MutableMap<String, String>.putNotNull(key: String, value: Number?) = putNotNull(key) { value?.toString() }

fun MutableMap<String, String>.putNotNull(key: String, value: String?) = putNotNull(key) { value }

fun MutableMap<String, String>.putNotNull(key: String,
    list: List<*>?) = putNotNull(key) { list?.takeIf { it.isNotEmpty() }?.joinToString(",") }

fun MutableMap<String, String>.putNotNull(key: String,
    valueProvider: () -> String?) = valueProvider()?.let { put(key, it) }
