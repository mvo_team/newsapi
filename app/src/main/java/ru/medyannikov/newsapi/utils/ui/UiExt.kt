package ru.medyannikov.newsapi.utils.ui

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Spinner
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import com.jakewharton.rxbinding3.widget.itemSelections
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.BaseAdapter
import ru.medyannikov.newsapi.core.ui.adapter.delegates.article.ErrorItem
import ru.medyannikov.newsapi.core.ui.adapter.delegates.article.NotFoundItem

object UiUtils {
    val drawableCrossFadeFactory: DrawableCrossFadeFactory = DrawableCrossFadeFactory.Builder()
            .setCrossFadeEnabled(true).build()
}

@Deprecated("Use Android KTX .isVisible")
infix fun View.visibleBy(predicate: Boolean) = apply { visibility = if (predicate) View.VISIBLE else View.GONE }

internal inline fun <reified I> Spinner.onItemSelected(crossinline onSelected: (item: I) -> Unit) =
        this.itemSelections()
                .filter { it != AdapterView.INVALID_POSITION }
                .subscribe {
                    val item = selectedItem as? I
                    item?.let { onSelected(it) }
                }

fun ImageView.loadImage(imageUri: String?) {
    val options = this.context.requestOptions()
        .error(R.drawable.ic_placeholder)
        .placeholder(R.drawable.ic_placeholder)
        .transform(CenterCrop())

    loadImageByParams(imageUri, options)
}


fun ImageView.loadImageRoundedCorners(imageUri: String?) {
    val options = this.context.requestOptions()
            .error(R.drawable.ic_placeholder)
            .placeholder(R.drawable.ic_placeholder)
            .transform(CenterCrop(), RoundedCorners(16))

    loadImageByParams(imageUri, options)
}

private fun ImageView.loadImageByParams(imageUri: String?, options: RequestOptions) {
    Glide.with(this)
        .load(imageUri)
        .apply(options)
        .transition(DrawableTransitionOptions.withCrossFade(UiUtils.drawableCrossFadeFactory))
        .into(this)
}


private fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()

fun Context.requestOptions(): RequestOptions {
    return RequestOptions()
}

fun Activity?.hideKeyboard() {
    this?.apply {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        currentFocus?.apply { imm.hideSoftInputFromWindow(this.windowToken, 0) }
    }
}

fun View?.hideKeyboard() {
    this?.apply {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}

fun <T> LiveData<T>.observe(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, Observer { observer(it) })
}

fun Context?.getLayoutManager(adapter: BaseAdapter<Any>? = null,
                              predicateFullRow: ((item: Any?) -> Boolean)? = { item -> item is ErrorItem || item is NotFoundItem }): RecyclerView.LayoutManager {
    return if (this?.resources?.getBoolean(R.bool.isTablet) != true) {
        LinearLayoutManager(this)
    } else {
        GridLayoutManager(this, 2).apply {
            adapter?.let { adapter ->
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        val item = adapter.getItemByPosition(position)
                        return if (predicateFullRow != null && predicateFullRow(item)) {
                            this@getLayoutManager.resources.getInteger(R.integer.span_size_row)
                        } else {
                            this@getLayoutManager.resources.getInteger(R.integer.span_size_normal)
                        }
                    }
                }
            }
        }
    }
}