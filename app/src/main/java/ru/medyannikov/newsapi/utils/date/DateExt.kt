package ru.medyannikov.newsapi.utils.date

import android.util.Log
import org.threeten.bp.Clock
import org.threeten.bp.DateTimeException
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime

/** Formatting full date strings */
fun String.toReadableDate(): String {
    return try {
        DateHelper.parseFullFormatDate(this).format(DateHelper.readableDateFormatter)
    } catch (e: DateTimeException) {
        Log.v("String.toReadableDate", "Parse date exception", e)
        ""
    }
}

fun LocalDateTime.formatFull(): String {
    return format(DateHelper.dateTimeFormatFull)
}

fun LocalDate.formatFull(): String {
    return format(DateHelper.dateTimeFormatFull)
}

fun LocalDate.formatSimple(): String {
    return format(DateHelper.dateFormatDashPattern)
}

fun nowUtcF(): String {
    return LocalDateTime.now(Clock.systemUTC()).formatFull()
}