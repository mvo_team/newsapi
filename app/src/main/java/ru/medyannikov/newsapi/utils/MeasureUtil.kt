package ru.medyannikov.newsapi.utils

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import kotlin.math.roundToInt

fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()

fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Configuration.setupDensity(ctx: Context) {
  val density = calculateDensity(ctx)
  fontScale = 1f
  densityDpi = density?.toInt() ?: densityDpi
}

private fun calculateDensity(contextWrapper: Context?): Int? {
  return try {
    val metrics = contextWrapper?.resources?.displayMetrics ?: return 0
    val hp = metrics.heightPixels.toDouble()
    val wp = metrics.widthPixels.toDouble()
    val wi = wp / metrics.xdpi.toDouble()
    val hi = hp / metrics.ydpi.toDouble()
    val x = Math.pow(wi, 2.0)
    val y = Math.pow(hi, 2.0)
    val screenInches = Math.sqrt(x + y)
    val density = (Math.sqrt((wp * wp) + (hp * hp)) / screenInches).roundToInt()

    return density + 15
  } catch (e: Exception) {
    null
  }
}