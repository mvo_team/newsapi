package ru.medyannikov.newsapi.settings.domain

import io.reactivex.Completable
import io.reactivex.Single

interface SettingsInteractor {

    fun getLanguage(): String?

    fun setLanguage(language: String): Single<String>

    fun getApiKey(): String?

    fun setApiKey(apiKey: String?): Completable
}