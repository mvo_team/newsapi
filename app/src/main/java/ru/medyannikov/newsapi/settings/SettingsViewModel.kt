package ru.medyannikov.newsapi.settings

import android.content.Context
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.utils.ui.sendRateUsIntent
import javax.inject.Inject

class SettingsViewModel
@Inject constructor(private val mainRouter: MainRouter,
                    private val localeManager: LocaleManager,
                    private val navigationMenuController: NavigationMenuController,
                    private val context: Context) : BaseViewModel() {

    fun openLanguageSelector() {
        mainRouter.openLanguageSelectorScreen()
    }

    fun openNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.SETTINGS)
    }

    fun openShareApp() {
        mainRouter.openShare(title = localeManager.getString(R.string.title_share),
                message = localeManager.getString(R.string.share_message))
    }

    fun openRateApp() {
        try {
            context.sendRateUsIntent()
        } catch (e: Exception) {
            errorMessage.value = localeManager.getString(R.string.error_common_message)
        }
    }

    fun openAgreement() = mainRouter.openAgreementScreen()

    fun openPrivacy() = mainRouter.openPrivacyScreen()

    fun sendSupportEmail() = mainRouter.sendSupportEmail()

    fun openAboutScreen() = mainRouter.openAboutScreen()

    fun openApiKeyScreen() = mainRouter.openApiKeyScreen()
}