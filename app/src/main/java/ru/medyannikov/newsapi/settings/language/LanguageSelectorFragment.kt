package ru.medyannikov.newsapi.settings.language

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_select_language.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.launch.LaunchActivity
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged
import ru.medyannikov.newsapi.core.ui.android.BaseFragment

class LanguageSelectorFragment : BaseFragment<LanguageSelectorViewModel>(), Injectable {

    override fun getLayoutId(): Int = R.layout.fragment_select_language

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intiViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        initToolbar()
        initViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        vm.hideNavigationMenu()
    }

    private fun intiViewModel() {
        vm = provideVm()
        vm.init()
    }


    private fun initViews() {
        swipeRefresh.isEnabled = false
    }

    private fun observeViewModel() {
        vm.successChanged.observe(this, Observer {
            it?.let {
                val i = Intent(context, LaunchActivity::class.java)
                startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            }
        })

        vm.currentLanguage.observe(this, Observer {
            it?.let { showUserLanguage(it) }
        })
    }

    private fun showUserLanguage(lan: String) {
        when (lan) {
            "ru" -> selectRussian()
            "en" -> selectEnglish()
            "es" -> selectSpain()
        }
        radioGroup.setOnCheckedChangeListener { _, checkedId -> vm.selectLanguage(checkedId) }
    }

    private fun selectSpain() {
        rbSpain.isChecked = true
    }

    private fun selectEnglish() {
        rbEnglish.isChecked = true
    }

    private fun selectRussian() {
        rbRussian.isChecked = true
    }

    private fun initToolbar(title: String? = null) {
        if (activity is OnActivityConfigurationChanged) {
            (activity as OnActivityConfigurationChanged).setActivityConfiguration(
                ActivityConfiguration(
                    title = title ?: getString(R.string.title_language),
                    hasBackButton = true
                ), toolbar
            )
        }
    }

    companion object {

        fun newInstance() = LanguageSelectorFragment()
    }
}
