package ru.medyannikov.newsapi.settings.agreement

import androidx.lifecycle.MutableLiveData
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import javax.inject.Inject

class AgreementViewModel
@Inject constructor(private val localeManager: LocaleManager,
                    private val navigationMenuController: NavigationMenuController,
                    private val mainRouter: MainRouter) : BaseViewModel() {

    val agreementPath by lazy { MutableLiveData<String>() }

    fun init(page: AgreementFragment.AgreementPage?) {
        val resPath = when (page) {
            AgreementFragment.AgreementPage.AGREEMENT -> localeManager.getString(R.string.agreement_page)
            AgreementFragment.AgreementPage.PRIVACY -> localeManager.getString(R.string.privacy_page)
            else -> {
                 mainRouter.exit()
                return
            }
        }

        agreementPath.value = resPath
    }

    fun hideNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }

    fun pageLoaded() {
        isLoading.value = false
    }

    fun startLoading() {
        isLoading.value = true
    }
}