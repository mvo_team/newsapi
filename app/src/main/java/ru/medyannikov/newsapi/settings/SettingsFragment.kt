package ru.medyannikov.newsapi.settings

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_settings.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged

class SettingsFragment : BaseFragment<SettingsViewModel>(), Injectable {

    override fun getLayoutId(): Int = R.layout.fragment_settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = provideVm()
        vm.attachBaseListeners()
    }

    override fun onStart() {
        super.onStart()
        vm.openNavigationMenu()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(configuration = ActivityConfiguration(
                title = getString(R.string.title_settings), hasBackButton = false), toolbar = toolbar)

        tvApiKey.setOnClickListener { vm.openApiKeyScreen() }
        tvLanguage.setOnClickListener { vm.openLanguageSelector() }

        tvRecommendFriend.setOnClickListener { vm.openShareApp() }
        tvRateApp.setOnClickListener { vm.openRateApp() }

        tvAgreement.setOnClickListener { vm.openAgreement() }
        tvPrivacy.setOnClickListener { vm.openPrivacy() }
        tvAbout.setOnClickListener { vm.openAboutScreen() }
        tvSupport.setOnClickListener { vm.sendSupportEmail() }
    }

    companion object {

        fun newInstance() = SettingsFragment()
    }
}