package ru.medyannikov.newsapi.settings.about

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.medyannikov.newsapi.BuildConfig
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import javax.inject.Inject

class AboutViewModel
@Inject constructor(private val navigationMenuController: NavigationMenuController) : BaseViewModel() {

    private val versionName by lazy { MutableLiveData<String>() }

    init {
        initVm()
    }

    fun getVersion(): LiveData<String> = versionName

    fun hideNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }

    private fun initVm() {
        versionName.value = BuildConfig.VERSION_NAME
    }
}