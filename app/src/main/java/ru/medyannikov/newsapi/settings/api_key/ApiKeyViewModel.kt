package ru.medyannikov.newsapi.settings.api_key

import androidx.lifecycle.LiveData
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.settings.domain.SettingsInteractor
import ru.medyannikov.newsapi.utils.aac.SingleLiveEvent
import javax.inject.Inject

class ApiKeyViewModel
@Inject constructor(private val interactor: SettingsInteractor,
                    private val mainRouter: MainRouter,
                    private val navigationMenuController: NavigationMenuController) : BaseViewModel() {

    private val apiKey by lazy { SingleLiveEvent<String?>() }

    init {
        loadApiKey()
    }

    fun getApiKey(): LiveData<String?> = apiKey

    fun saveApiKey(apiKey: String) {
        interactor.setApiKey(apiKey)
                .baseSubscribe { mainRouter.exit() }
    }

    private fun loadApiKey() {
        apiKey.value = interactor.getApiKey()
    }

    fun hideNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }
}