package ru.medyannikov.newsapi.settings.language

import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.Single
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.settings.domain.SettingsInteractor
import ru.medyannikov.newsapi.utils.aac.SingleLiveEvent
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject

class LanguageSelectorViewModel
@Inject constructor(private val interactor: SettingsInteractor,
                    private val schedulerProvider: SchedulerProvider,
                    private val navigationMenuController: NavigationMenuController,
                    private val appPreference: AppPreferences) : BaseViewModel() {

    private val language get() = appPreference.userLanguage

    val successChanged = SingleLiveEvent<Unit>()
    val currentLanguage = MutableLiveData<String>()

    fun init() {
        currentLanguage.value = language
    }

    fun hideNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }

    fun selectLanguage(checkedId: Int) {
        val lang = language ?: "en"
        Observable.just(true)
            .doOnSubscribe { isLoading.postValue(true) }
            .flatMapSingle {
                when (checkedId) {
                    R.id.rbEnglish -> {
                        changeLanguage("en")
                    }
                    R.id.rbSpain -> {
                        changeLanguage("es")
                    }
                    R.id.rbRussian -> {
                        changeLanguage("ru")
                    }
                    else -> {
                        Single.just("en")
                    }
                }
            }
            .observeOn(schedulerProvider.ui())
            .doOnTerminate { isLoading.value = false }
            .doOnNext { isLoading.value = false }
            .subscribe(
                {
                    successChanged.value = Unit
                },
                {
                    currentLanguage.value = lang
                }).addToDisposable()
    }
    private fun changeLanguage(lang: String): Single<String> = interactor.setLanguage(lang)
}