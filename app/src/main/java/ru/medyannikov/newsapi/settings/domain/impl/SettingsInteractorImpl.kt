package ru.medyannikov.newsapi.settings.domain.impl

import io.reactivex.Completable
import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.settings.domain.SettingsInteractor
import javax.inject.Inject

class SettingsInteractorImpl
    @Inject constructor(private val appPreferences: AppPreferences,
                        private val localeManager: LocaleManager): SettingsInteractor {

    override fun getLanguage(): String? = localeManager.language

    override fun setLanguage(language: String) = Single.fromCallable { localeManager.setNewLocale(language) }

    override fun getApiKey(): String? = appPreferences.apiKey

    override fun setApiKey(apiKey: String?) = Completable.fromCallable { appPreferences.apiKey = apiKey; Unit }
}