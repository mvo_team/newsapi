package ru.medyannikov.newsapi.settings.agreement

import android.content.Context
import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.webkit.*
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.fragment_agreement.*
import kotlinx.android.synthetic.main.layout_error.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged

class AgreementFragment : BaseFragment<AgreementViewModel>(), Injectable {

    override fun getLayoutId(): Int = R.layout.fragment_agreement

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViewModel(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        vm.hideNavigationMenu()
    }

    private fun initViewModel(savedInstanceState: Bundle?) {
        vm = provideVm()
        vm.attachBaseListeners()
        if (savedInstanceState == null) {
            val page = arguments?.getParcelable<AgreementPage>(AGREEMENT_KEY)
            vm.init(page)
        }
    }

    private fun observeViewModel() {
        vm.agreementPath.observe(this, Observer {
            it?.let { showAgreement(it) }
        })

        vm.getLoading().observe(this, Observer {
            swipeRefresh.isRefreshing = it
        })
    }

    private fun showAgreement(url: String) {
        webView.loadUrl(url)
    }

    private fun initViews() {
        swipeRefresh.isEnabled = false
        btnRetry.setOnClickListener { webView.reload() }
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String) {
                super.onPageFinished(view, url)
                vm.pageLoaded()
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                vm.startLoading()
            }

            override fun onLoadResource(view: WebView?, url: String?) {
                super.onLoadResource(view, url)
                view?.isVisible = true
                errorWrapper?.isVisible = false
            }

            override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
                view?.isVisible = false
                errorWrapper?.isVisible = true
            }

            override fun onReceivedHttpError(view: WebView?, request: WebResourceRequest?,
                                             errorResponse: WebResourceResponse?) {
                view?.isVisible = false
                errorWrapper?.isVisible = true
            }

            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                view?.isVisible = false
                errorWrapper?.isVisible = true
                handler?.proceed()
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                view?.isVisible = false
                errorWrapper?.isVisible = true
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                return checkUrlToRedirect(url)
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val url = request?.url.toString()
                return checkUrlToRedirect(url)
            }
        }
        webView.setup()
    }

    private fun checkUrlToRedirect(url: String?): Boolean {
        context?.let { context ->
            context.openUrl(url)
        }
        return true
    }

    private fun initToolbar(title: String? = null) {
        if (activity is OnActivityConfigurationChanged) {
            (activity as OnActivityConfigurationChanged).setActivityConfiguration(
                    ActivityConfiguration(
                            title = title ?: getString(R.string.title_agreement),
                            hasBackButton = true
                    ), toolbar
            )
        }
    }

    private fun Context.openUrl(url: String?) {
/*        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            if (intent.resolveActivity(packageManager) != null) {
                ContextCompat.startActivity(this, intent, null)
            } else {
                showError(getString(R.string.message_error_loading))
            }
        } catch (e: Exception) {
            showError(getString(R.string.message_error_loading))
            e.log()
        }*/
    }

    private fun WebView.setup() {
        val webSettings = settings
        webSettings.javaScriptEnabled = true
        webSettings.builtInZoomControls = true
        webSettings.displayZoomControls = true
        webSettings.setSupportZoom(true)
        webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        invokeZoomPicker()
    }

    companion object {

        private const val AGREEMENT_KEY = "agreement_key"

        fun newInstance(page: AgreementPage) = AgreementFragment().apply {
            arguments = bundleOf(AGREEMENT_KEY to page)
        }
    }

    @Parcelize
    enum class AgreementPage : Parcelable {
        PRIVACY, AGREEMENT
    }
}