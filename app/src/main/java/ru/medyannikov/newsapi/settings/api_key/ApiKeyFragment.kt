package ru.medyannikov.newsapi.settings.api_key

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_api_key.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged

class ApiKeyFragment : BaseFragment<ApiKeyViewModel>(), Injectable {

    override fun getLayoutId(): Int = R.layout.fragment_api_key

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = provideVm()
        vm.attachBaseListeners()
    }

    override fun onStart() {
        super.onStart()
        vm.hideNavigationMenu()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initViews()
        observeViewModel()
    }

    private fun initViews() {
        btnApiKeySave.setOnClickListener { vm.saveApiKey(etApiKey.text.toString()) }
    }

    private fun observeViewModel() {
        vm.getApiKey().observe(this, Observer {
            etApiKey.setText(it)
        })
    }

    private fun initToolbar() {
        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(
                ActivityConfiguration(title = getString(R.string.options_api_key), hasBackButton = true), toolbar)
    }

    companion object {

        fun newInstance() = ApiKeyFragment()
    }
}