package ru.medyannikov.newsapi.settings.about

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_about.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged

class AboutFragment : BaseFragment<AboutViewModel>(), Injectable {

    override fun getLayoutId(): Int = R.layout.fragment_about

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = provideVm()
        vm.attachBaseListeners()
    }

    override fun onStart() {
        super.onStart()
        vm.hideNavigationMenu()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        observeViewModel()
    }

    private fun observeViewModel() {
        vm.getVersion().observe(this, Observer {
            tvAboutVersion.text = it
        })
    }

    private fun initToolbar() {
        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(configuration = ActivityConfiguration(
                hasBackButton = true), toolbar = toolbar)
    }

    companion object {

        fun newInstance() = AboutFragment()
    }
}