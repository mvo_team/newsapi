package ru.medyannikov.newsapi.di.subcomponent

import dagger.Component
import ru.medyannikov.newsapi.di.SessionScope

@Component(modules = [SessionModule::class])
@SessionScope
interface SessionComponent {

    @Component.Builder
    interface Builder {

        fun build(): SessionComponent
    }
}