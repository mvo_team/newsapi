package ru.medyannikov.newsapi.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.medyannikov.newsapi.core.data.db.NewsDatabase
import javax.inject.Singleton

@Module
class DbModule {

  @Provides
  @Singleton
  fun providerDatabase(context: Context): NewsDatabase = NewsDatabase.getInstance(context)
}