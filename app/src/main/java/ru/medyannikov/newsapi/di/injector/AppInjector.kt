package ru.medyannikov.newsapi.di.injector

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import ru.medyannikov.newsapi.NewsApp
import ru.medyannikov.newsapi.di.AppComponent
import ru.medyannikov.newsapi.di.DaggerAppComponent
import ru.medyannikov.newsapi.di.subcomponent.SessionComponent

object AppInjector {

    lateinit var appComponent: AppComponent

    @Synchronized
    fun recreateGraph(app: NewsApp) {
        appComponent = DaggerAppComponent.builder().application(app).build()
        appComponent.inject(app)
    }

    @Synchronized
    fun init(app: NewsApp) {
        recreateGraph(app)

        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {
                //nothing
            }

            override fun onActivityResumed(activity: Activity?) {
                //nothing
            }

            override fun onActivityStarted(activity: Activity?) {
                //nothing
            }

            override fun onActivityDestroyed(activity: Activity?) {
                //nothing
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
                //nothing
            }

            override fun onActivityStopped(activity: Activity?) {
                //nothing
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                if (activity is Injectable) {
                    AndroidInjection.inject(activity)
                    if (activity is FragmentActivity) {
                        activity.supportFragmentManager.registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
                            override fun onFragmentPreCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
                                super.onFragmentPreCreated(fm, f, savedInstanceState)
                                if (f is Injectable) {
                                    AndroidSupportInjection.inject(f)
                                }
                            }
                        }, true)
                    }
                }
            }
        })
    }

    fun getOrInitSessionComponent(): SessionComponent? {
        return null
    }
}