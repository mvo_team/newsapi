package ru.medyannikov.newsapi.di.module

import dagger.Module
import dagger.Provides
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Singleton

@Module
class NavigationModule {

    @Provides
    @Singleton
    fun mainRouter(cicerone: MainRouter): Cicerone<MainRouter> = Cicerone.create(cicerone)

    @Provides
    @Singleton
    fun mainNavigatorHolder(cicerone: Cicerone<@JvmSuppressWildcards MainRouter>): MainNavigatorHolder =
        MainNavigatorHolder(cicerone.navigatorHolder)
}

class MainNavigatorHolder(delegate: NavigatorHolder) : NavigatorHolder by delegate