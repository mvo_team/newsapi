package ru.medyannikov.newsapi.di.module

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import ru.medyannikov.newsapi.NewsApp
import ru.medyannikov.newsapi.core.domain.error.ErrorMessageFactory
import ru.medyannikov.newsapi.core.domain.error.impl.SystemErrorMessageFactory
import ru.medyannikov.newsapi.utils.Const
import ru.medyannikov.newsapi.utils.rx.AppSchedulerProvider
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideContext(app: NewsApp): Context = app.applicationContext

    @Singleton
    @Provides
    fun providePreferences(context: Context): SharedPreferences = context.getSharedPreferences(Const.PREFS_NAME,
                                                                                               Context.MODE_PRIVATE)
    @Provides
    @Singleton
    fun provideSchedulerProvider(impl: AppSchedulerProvider): SchedulerProvider = impl

    @Provides
    @Singleton
    fun provideErrorMessageFactory(impl: SystemErrorMessageFactory): ErrorMessageFactory = impl

    @Provides
    @Singleton
    fun gson() = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            .serializeNulls()
            .create()
}