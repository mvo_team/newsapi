package ru.medyannikov.newsapi.di.dagger

import ru.medyannikov.newsapi.di.AppComponent

fun <T> injectSingleton(func: AppComponent.() -> T) = lazy { appComponent.func() }