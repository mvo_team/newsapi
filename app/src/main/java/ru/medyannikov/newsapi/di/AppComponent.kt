package ru.medyannikov.newsapi.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ru.medyannikov.newsapi.NewsApp
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences
import ru.medyannikov.newsapi.di.module.*
import ru.medyannikov.newsapi.core.domain.error.ErrorMessageFactory
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        NavigationModule::class,
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        DataModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        ActivityInjectorModule::class
    )
)
interface AppComponent : AndroidInjector<NewsApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: NewsApp): Builder

        fun build(): AppComponent
    }

    val mainNavigatorHolder: MainNavigatorHolder
    val appPrefs: AppPreferences
    val localeManager: LocaleManager
    val errorMessageFactory: ErrorMessageFactory
}