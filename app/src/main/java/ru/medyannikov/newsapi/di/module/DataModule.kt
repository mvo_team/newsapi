package ru.medyannikov.newsapi.di.module

import dagger.Binds
import dagger.Module
import ru.medyannikov.newsapi.core.data.db.provider.SourceDbProvider
import ru.medyannikov.newsapi.core.data.db.provider.impl.SourceDbProviderImpl
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences
import ru.medyannikov.newsapi.core.data.preferences.impl.AppPreferenceProvider
import ru.medyannikov.newsapi.feed.everything.data.EverythingRepositoryImpl
import ru.medyannikov.newsapi.feed.everything.domain.EverythingRepository
import ru.medyannikov.newsapi.feed.everything.domain.interactor.EverythingInteractor
import ru.medyannikov.newsapi.feed.everything.domain.interactor.impl.EverythingInteractorImpl
import ru.medyannikov.newsapi.feed.top_headlines.data.TopHeadlineRepositoryImpl
import ru.medyannikov.newsapi.feed.top_headlines.domain.TopHeadlineRepository
import ru.medyannikov.newsapi.feed.top_headlines.domain.interactor.TopHeadlinesInteractor
import ru.medyannikov.newsapi.feed.top_headlines.domain.interactor.impl.TopHeadlinesInteractorImpl
import ru.medyannikov.newsapi.filter.core.domain.provider.category.AvailableCategoryProvider
import ru.medyannikov.newsapi.filter.core.domain.provider.category.impl.AvailableCategoryProviderImpl
import ru.medyannikov.newsapi.filter.core.domain.provider.country.AvailableCountryProvider
import ru.medyannikov.newsapi.filter.core.domain.provider.country.impl.AvailableCountryProviderImpl
import ru.medyannikov.newsapi.filter.core.domain.provider.language.AvailableLanguageProvider
import ru.medyannikov.newsapi.filter.core.domain.provider.language.impl.AvailableLanguageProviderImpl
import ru.medyannikov.newsapi.filter.everything_filter.domain.interactor.EverythingFilterInteractor
import ru.medyannikov.newsapi.filter.everything_filter.domain.interactor.impl.EverythingFilterInteractorImpl
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.EverythingFilterProvider
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.impl.EverythingFilterProviderImpl
import ru.medyannikov.newsapi.filter.top_headlines.domain.interactor.TopHeadlinesFilterInteractor
import ru.medyannikov.newsapi.filter.top_headlines.domain.interactor.impl.TopHeadlinesFilterInteractorImpl
import ru.medyannikov.newsapi.filter.top_headlines.domain.provider.TopHeadlinesFilterProvider
import ru.medyannikov.newsapi.filter.top_headlines.domain.provider.impl.TopHeadlinesFilterProviderImpl
import ru.medyannikov.newsapi.news_source.data.SourceRepositoryImpl
import ru.medyannikov.newsapi.news_source.domain.SourceRepository
import ru.medyannikov.newsapi.news_source.domain.interactor.SourceInteractor
import ru.medyannikov.newsapi.news_source.domain.interactor.impl.SourceInteractorImpl
import ru.medyannikov.newsapi.settings.domain.SettingsInteractor
import ru.medyannikov.newsapi.settings.domain.impl.SettingsInteractorImpl
import javax.inject.Singleton

@Module(includes = [DbModule::class])
interface DataModule {

    @Singleton
    @Binds
    fun providePrefs(impl: AppPreferenceProvider): AppPreferences

    @Singleton
    @Binds
    fun providerEverythingRepository(impl: EverythingRepositoryImpl): EverythingRepository

    @Singleton
    @Binds
    fun providerEverythingInteractor(impl: EverythingInteractorImpl): EverythingInteractor

    @Singleton
    @Binds
    fun providerTopHeadlineRepository(impl: TopHeadlineRepositoryImpl): TopHeadlineRepository

    @Singleton
    @Binds
    fun providerSourceRepository(impl: SourceRepositoryImpl): SourceRepository

    @Singleton
    @Binds
    fun providerSourceInteractor(impl: SourceInteractorImpl): SourceInteractor

    @Singleton
    @Binds
    fun providerTopHeadlinesInteractor(impl: TopHeadlinesInteractorImpl): TopHeadlinesInteractor

    @Singleton
    @Binds
    fun providerEverythingFilterInteractor(impl: EverythingFilterInteractorImpl): EverythingFilterInteractor

    @Singleton
    @Binds
    fun providerTopHeadlinesFilterInteractor(impl: TopHeadlinesFilterInteractorImpl): TopHeadlinesFilterInteractor

    @Singleton
    @Binds
    fun providerAvailableLanguageProvider(impl: AvailableLanguageProviderImpl): AvailableLanguageProvider

    @Singleton
    @Binds
    fun providerAvailableCategoryProvider(impl: AvailableCategoryProviderImpl): AvailableCategoryProvider

    @Singleton
    @Binds
    fun providerTopHeadlinesFilterProvider(impl: TopHeadlinesFilterProviderImpl): TopHeadlinesFilterProvider

    @Singleton
    @Binds
    fun providerEverythingFilterProvider(impl: EverythingFilterProviderImpl): EverythingFilterProvider

    @Singleton
    @Binds
    fun providerAvailableCountryProvider(impl: AvailableCountryProviderImpl): AvailableCountryProvider

    @Singleton
    @Binds
    fun providerSettingsInteractor(impl: SettingsInteractorImpl): SettingsInteractor

    @Singleton
    @Binds
    fun provideSourceProvider(sourceDbProviderImpl: SourceDbProviderImpl): SourceDbProvider
}
