package ru.medyannikov.newsapi.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.medyannikov.newsapi.arcticle_detail.ArticleDetailViewModel
import ru.medyannikov.newsapi.domains.SelectedDomainsViewModel
import ru.medyannikov.newsapi.feed.everything.ui.EverythingFeedViewModel
import ru.medyannikov.newsapi.feed.top_headlines.ui.TopHeadlinesFeedViewModel
import ru.medyannikov.newsapi.filter.everything_filter.ui.EverythingFilterViewModel
import ru.medyannikov.newsapi.filter.top_headlines.ui.TopHeadlinesFilterViewModel
import ru.medyannikov.newsapi.launch.LaunchViewModel
import ru.medyannikov.newsapi.main.MainViewModel
import ru.medyannikov.newsapi.news_source.ui.everything.EverythingSourceViewModel
import ru.medyannikov.newsapi.news_source.ui.top_headlines.TopHeadlinesSourceViewModel
import ru.medyannikov.newsapi.settings.SettingsViewModel
import ru.medyannikov.newsapi.settings.about.AboutViewModel
import ru.medyannikov.newsapi.settings.agreement.AgreementViewModel
import ru.medyannikov.newsapi.settings.api_key.ApiKeyViewModel
import ru.medyannikov.newsapi.settings.language.LanguageSelectorViewModel

@Module
interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(vm: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LaunchViewModel::class)
    abstract fun bindLaunchViewModel(vm: LaunchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EverythingFeedViewModel::class)
    abstract fun bindFeedViewModel(vm: EverythingFeedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EverythingFilterViewModel::class)
    abstract fun bindEverythingFilterViewModel(vm: EverythingFilterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectedDomainsViewModel::class)
    abstract fun bindSelectedDomainsViewModel(vm: SelectedDomainsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TopHeadlinesFeedViewModel::class)
    abstract fun bindTopHeadlinesFeedViewModel(vm: TopHeadlinesFeedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TopHeadlinesFilterViewModel::class)
    abstract fun bindTopHeadlinesFilterViewModel(vm: TopHeadlinesFilterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun bindSettingsViewModel(vm: SettingsViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(LanguageSelectorViewModel::class)
    abstract fun bindLanguageSelectorViewModel(vm: LanguageSelectorViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AgreementViewModel::class)
    abstract fun bindAgreementViewModel(vm: AgreementViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AboutViewModel::class)
    abstract fun bindAboutViewModel(vm: AboutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ApiKeyViewModel::class)
    abstract fun bindApiKeyViewModel(vm: ApiKeyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ArticleDetailViewModel::class)
    abstract fun bindArticleDetailViewModel(vm: ArticleDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EverythingSourceViewModel::class)
    abstract fun bindEverythingSourceViewModel(vm: EverythingSourceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TopHeadlinesSourceViewModel::class)
    abstract fun bindTopHeadlinesSourceViewModel(vm: TopHeadlinesSourceViewModel): ViewModel
}