package ru.medyannikov.newsapi.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.medyannikov.newsapi.arcticle_detail.ArticleDetailFragment
import ru.medyannikov.newsapi.domains.SelectorDomainsFragment
import ru.medyannikov.newsapi.feed.everything.ui.EverythingFeedFragment
import ru.medyannikov.newsapi.feed.top_headlines.ui.TopHeadlineFragment
import ru.medyannikov.newsapi.filter.everything_filter.ui.EverythingFilterFragment
import ru.medyannikov.newsapi.filter.top_headlines.ui.TopHeadlinesFilterFragment
import ru.medyannikov.newsapi.news_source.ui.everything.EverythingSourceFragment
import ru.medyannikov.newsapi.news_source.ui.top_headlines.TopHeadlinesSourceFragment
import ru.medyannikov.newsapi.settings.SettingsFragment
import ru.medyannikov.newsapi.settings.about.AboutFragment
import ru.medyannikov.newsapi.settings.agreement.AgreementFragment
import ru.medyannikov.newsapi.settings.api_key.ApiKeyFragment
import ru.medyannikov.newsapi.settings.language.LanguageSelectorFragment


@Module
abstract class FragmentInjectorModule {

    @ContributesAndroidInjector
    abstract fun contributeFeedFragment(): EverythingFeedFragment

    @ContributesAndroidInjector
    abstract fun contributeEverythingFilterFragment(): EverythingFilterFragment

    @ContributesAndroidInjector
    abstract fun contributeSelectorDomainsFragment(): SelectorDomainsFragment

    @ContributesAndroidInjector
    abstract fun contributeTopHeadlineFragment(): TopHeadlineFragment

    @ContributesAndroidInjector
    abstract fun contributeTopHeadlinesFilterFragment(): TopHeadlinesFilterFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    abstract fun contributeLanguageSelectorFragment(): LanguageSelectorFragment

    @ContributesAndroidInjector
    abstract fun contributeAgreementFragment(): AgreementFragment

    @ContributesAndroidInjector
    abstract fun contributeAboutFragment(): AboutFragment

    @ContributesAndroidInjector
    abstract fun contributeApiKeyFragment(): ApiKeyFragment

    @ContributesAndroidInjector
    abstract fun contributeArticleDetailFragment(): ArticleDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeEverythingSourceFragment(): EverythingSourceFragment

    @ContributesAndroidInjector
    abstract fun contributeTopHeadlinesSourceFragment(): TopHeadlinesSourceFragment
}