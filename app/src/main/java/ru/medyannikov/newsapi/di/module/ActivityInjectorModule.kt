package ru.medyannikov.newsapi.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.medyannikov.newsapi.launch.LaunchActivity
import ru.medyannikov.newsapi.main.MainActivity

@Module
abstract class ActivityInjectorModule {

    @ContributesAndroidInjector(modules = [FragmentInjectorModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeLaunchActivity(): LaunchActivity

}