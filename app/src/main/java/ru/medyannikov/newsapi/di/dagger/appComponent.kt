package ru.medyannikov.newsapi.di.dagger

import ru.medyannikov.newsapi.di.injector.AppInjector


val appComponent get() = AppInjector.appComponent