package ru.medyannikov.newsapi.di.subcomponent

import android.util.Log
import androidx.annotation.MainThread
import ru.medyannikov.newsapi.di.injector.AppInjector
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SessionComponentHolder @Inject constructor() {
    @Volatile
    var component: SessionComponent? = null
        private set

    @MainThread
    fun createComponent(): Boolean {
        if (component != null) {
            Log.e("SESSION", "Сначала нужно вызвать destroy() для уничтожения SessionComponent")
            return false
        }
        component = AppInjector.getOrInitSessionComponent()
        return true
    }

    @MainThread
    fun destroyComponent(): Boolean {
        if (component == null) {
            Log.w("SESSION", "Попытка уничтожить несуществующий SessionComponent")
            return false
        }
        //component!!.destroyedState().value = true
        component = null
        return true
    }
}