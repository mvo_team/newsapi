package ru.medyannikov.newsapi.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import ru.medyannikov.newsapi.BuildConfig
import ru.medyannikov.newsapi.core.data.network.api.everything.EverythingApi
import ru.medyannikov.newsapi.core.data.network.api.sources.SourcesApi
import ru.medyannikov.newsapi.core.data.network.api.top_headlines.TopHeadlinesApi
import ru.medyannikov.newsapi.core.data.network.interceptor.HeaderInterceptor
import ru.medyannikov.newsapi.core.data.preferences.AppPreferences
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideEverythingApi(gson: Gson, client: OkHttpClient): EverythingApi = generateApi(gson, client)

    @Singleton
    @Provides
    fun provideTopHeadlinesApi(gson: Gson, client: OkHttpClient): TopHeadlinesApi = generateApi(gson, client)

    @Singleton
    @Provides
    fun provideSourcesApi(gson: Gson, client: OkHttpClient): SourcesApi = generateApi(gson, client)

    @Provides
    fun provideDefaultClient(interceptor: Interceptor): OkHttpClient = createClient(interceptor).build()

    @Provides
    fun providerDefaultHeaderInterceptor(appPreference: AppPreferences): Interceptor = HeaderInterceptor(appPreference)

    private fun createClient(interceptor: Interceptor?): OkHttpClient.Builder {
        var builder = OkHttpClient.Builder()
            .readTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)

        if (interceptor != null) {
            builder = builder.addInterceptor(interceptor)
        }

        return builder.addInterceptor(
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            } else {
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
            }
        )
    }


    private inline fun <reified T> generateApi(gson: Gson, client: OkHttpClient): T {
        val dateConverter = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create()
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create(dateConverter))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(T::class.java)
    }

    companion object {

        const val DEFAULT_SCOPE = "DEFAULT_SCOPE"
        const val EXTERNAL_SCOPE = "NO_SCOPE" // Для обращений к внешним сервисам, без добавления заголовков
    }
}