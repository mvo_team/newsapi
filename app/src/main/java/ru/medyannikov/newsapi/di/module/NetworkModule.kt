package ru.medyannikov.newsapi.di.module

import dagger.Binds
import dagger.Module
import ru.medyannikov.newsapi.core.data.network.api.everything.provider.EverythingNetworkProvider
import ru.medyannikov.newsapi.core.data.network.api.everything.provider.impl.EverythingNetworkProviderImpl
import ru.medyannikov.newsapi.core.data.network.api.sources.provider.SourceNetworkProvider
import ru.medyannikov.newsapi.core.data.network.api.sources.provider.impl.SourceNetworkProviderImpl
import ru.medyannikov.newsapi.core.data.network.api.top_headlines.provider.TopHeadlinesNetworkProvider
import ru.medyannikov.newsapi.core.data.network.api.top_headlines.provider.impl.TopHeadlinesNetworkProviderImpl
import javax.inject.Singleton

@Module(includes = [ApiModule::class])
interface NetworkModule {

    @Singleton
    @Binds
    fun provideEverythingNetworkProvider(provider: EverythingNetworkProviderImpl): EverythingNetworkProvider

    @Singleton
    @Binds
    fun provideTopHeadlinesNetworkProvider(provider: TopHeadlinesNetworkProviderImpl): TopHeadlinesNetworkProvider

    @Singleton
    @Binds
    fun provideSourceNetworkProvider(provider: SourceNetworkProviderImpl): SourceNetworkProvider

}