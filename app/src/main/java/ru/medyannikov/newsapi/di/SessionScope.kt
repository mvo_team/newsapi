package ru.medyannikov.newsapi.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SessionScope