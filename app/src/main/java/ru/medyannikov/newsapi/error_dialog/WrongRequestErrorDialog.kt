package ru.medyannikov.newsapi.error_dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.layout_dialog_feed_wrong_request.*
import ru.medyannikov.newsapi.R

class WrongRequestErrorDialog(private var text: String?, private val clickAction: () -> Unit) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_dialog_feed_wrong_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvWrongRequestDescription.text = text

        btnWrongRequestSettings.setOnClickListener {
            clickAction()
            dismiss()
        }
    }

    fun setDescription(text: String?) {
        this.text = text
    }

    companion object {

        fun newInstance(text: String?, clickAction: () -> Unit): WrongRequestErrorDialog {
            return WrongRequestErrorDialog(text, clickAction)
        }
    }
}