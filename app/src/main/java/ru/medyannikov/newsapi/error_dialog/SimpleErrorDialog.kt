package ru.medyannikov.newsapi.error_dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.layout_dialog_simple.*
import ru.medyannikov.newsapi.R

class SimpleErrorDialog
private constructor(private val title: String?,
                    private val message: String?,
                    private val description: String?,
                    private val actionText: String?,
                    private val action: (() -> Unit)?) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_dialog_simple, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvSimpleDialogTitle.text = title
        tvSimpleDialogMessage.text = message
        tvSimpleDialogDescription.text = description
        btnSimpleDialogAction.text = actionText
        btnSimpleDialogAction.setOnClickListener { action?.invoke() }
    }

    internal class Builder {
        private var title: String? = null
        private var message: String? = null
        private var description: String? = null
        private var actionText: String? = null
        private var action: (() -> Unit)? = null

        fun setTitle(title: String?): Builder {
            this.title = title
            return this
        }

        fun setMessage(message: String?): Builder {
            this.message = message
            return this
        }

        fun setDescription(description: String?): Builder {
            this.description = description
            return this
        }

        fun setActionText(actionText: String?): Builder {
            this.actionText = actionText
            return this
        }

        fun setAction(action: (() -> Unit)?): Builder {
            this.action = action
            return this
        }

        fun build() = SimpleErrorDialog(title, message, description, actionText, action)
    }

    companion object {

        fun newInstance(title: String,
                        message: String,
                        description: String,
                        actionText: String,
                        action: () -> Unit
        ): SimpleErrorDialog {
            return SimpleErrorDialog(title, message, description, actionText, action)
        }
    }
}