package ru.medyannikov.newsapi.feed.top_headlines.domain.interactor.impl

import io.reactivex.Single
import ru.medyannikov.newsapi.feed.top_headlines.domain.TopHeadlineRepository
import ru.medyannikov.newsapi.feed.top_headlines.domain.interactor.TopHeadlinesInteractor
import ru.medyannikov.newsapi.feed.top_headlines.domain.params.TopHeadlinesParams
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.filter.core.domain.model.TopHeadlinesFilterParams
import javax.inject.Inject

class TopHeadlinesInteractorImpl
  @Inject constructor(private val topHeadlineRepository: TopHeadlineRepository): TopHeadlinesInteractor {

  override fun getTopHeadlinesArticles(page: Int, filterParams: TopHeadlinesFilterParams): Single<PagingData<ArticleModel>> {
    val params = TopHeadlinesParams.fromFilterParams(filterParams)
    val mapParams = params.toMap()
    return topHeadlineRepository.getTopHeadlinesArticles(page, mapParams)
  }
}