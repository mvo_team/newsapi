package ru.medyannikov.newsapi.feed.top_headlines.domain.params

import ru.medyannikov.newsapi.filter.core.domain.model.TopHeadlinesFilterParams
import ru.medyannikov.newsapi.utils.params.putNotNull

data class TopHeadlinesParams(val country: String? = null,
                              val category: String? = null,
                              val sources: List<String>? = null,
                              val query: String? = null,
                              val pageSize: Int = 20,
                              val page: Int = 1) {

    fun toMap(): Map<String, String> {
        return mutableMapOf<String, String>().apply {
            putNotNull("country", country)
            putNotNull("category", category)
            putNotNull("sources", sources)
            putNotNull("q", query)
            putNotNull("pageSize", pageSize)
            putNotNull("page", page)
        }
    }

    companion object {

        fun fromFilterParams(filterParams: TopHeadlinesFilterParams): TopHeadlinesParams {
            return TopHeadlinesParams(country = filterParams.county?.isoValue,
                    category = filterParams.category?.value,
                    query = filterParams.query,
                    sources = filterParams.sources?.map { it.id ?: "" }?.sorted())
        }
    }
}