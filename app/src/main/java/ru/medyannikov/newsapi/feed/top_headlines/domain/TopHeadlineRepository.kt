package ru.medyannikov.newsapi.feed.top_headlines.domain

import io.reactivex.Single
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData

interface TopHeadlineRepository {

  fun getTopHeadlinesArticles(page: Int = 1, params: Map<String, String>): Single<PagingData<ArticleModel>>
}