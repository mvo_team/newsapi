package ru.medyannikov.newsapi.feed.top_headlines.domain.interactor

import io.reactivex.Single
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.filter.core.domain.model.TopHeadlinesFilterParams

interface TopHeadlinesInteractor {

  fun getTopHeadlinesArticles(page: Int = 1, filterParams: TopHeadlinesFilterParams): Single<PagingData<ArticleModel>>
}