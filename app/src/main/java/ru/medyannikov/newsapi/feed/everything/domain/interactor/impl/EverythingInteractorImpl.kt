package ru.medyannikov.newsapi.feed.everything.domain.interactor.impl

import io.reactivex.Single
import ru.medyannikov.newsapi.feed.everything.domain.EverythingRepository
import ru.medyannikov.newsapi.feed.everything.domain.interactor.EverythingInteractor
import ru.medyannikov.newsapi.feed.everything.domain.params.EverythingParams
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.filter.core.domain.model.EverythingFilterParams
import javax.inject.Inject

class EverythingInteractorImpl
@Inject constructor(private val everythingRepository: EverythingRepository) : EverythingInteractor {

    override fun getEverythingArticles(page: Int, params: EverythingFilterParams): Single<PagingData<ArticleModel>> {
        val everythingParams = EverythingParams.fromFilterParams(params)
        return everythingRepository.getEverythingArticles(page, everythingParams)
    }
}