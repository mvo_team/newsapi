package ru.medyannikov.newsapi.feed.everything.data

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.api.everything.provider.EverythingNetworkProvider
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.core.domain.model.toModel
import ru.medyannikov.newsapi.core.domain.repostories.common.CacheRequestRepository
import ru.medyannikov.newsapi.feed.everything.domain.EverythingRepository
import ru.medyannikov.newsapi.feed.everything.domain.params.EverythingParams
import javax.inject.Inject

class EverythingRepositoryImpl
@Inject constructor(private val apiEverything: EverythingNetworkProvider) : CacheRequestRepository<Int, PagingData<ArticleModel>>(), EverythingRepository {

    override fun getEverythingArticles(page: Int, params: EverythingParams): Single<PagingData<ArticleModel>> {
        val keyParams = params.hashCode() + page
        val cachedItems = getCached(keyParams)
        return if (cachedItems != null) {
            Single.just(cachedItems)
        } else {
            apiEverything.getEverythingArticles(page, params)
                    .map { response ->
                        val items = response.articles.map { articleDTO -> articleDTO.toModel() }
                        return@map PagingData(
                                items = items,
                                page = page,
                                totalCount = response.totalResults)
                    }
        }.doOnSuccess { putData(keyParams, it) }
    }
}