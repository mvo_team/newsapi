package ru.medyannikov.newsapi.feed.everything.domain.params

import androidx.annotation.StringRes
import ru.medyannikov.newsapi.R

/**
 * Define ordered articles
 *
 * RELEVANCY = articles more closely related to q come first.
 * POPULARITY = articles from popular sources and publishers come first.
 * PUBLISHED_AT = newest articles come first.
 */
enum class SortBy(val sortName: String, @StringRes val title: Int) {
  RELEVANCY("relevancy", R.string.sort_relevancy),
  POPULARITY("popularity", R.string.sort_popularity),
  PUBLISHED_AT("publishedAt", R.string.sort_published_at);

  companion object {
    val values = listOf(PUBLISHED_AT, POPULARITY, RELEVANCY)
  }
}