package ru.medyannikov.newsapi.feed.top_headlines.data

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.network.api.top_headlines.provider.TopHeadlinesNetworkProvider
import ru.medyannikov.newsapi.feed.top_headlines.domain.TopHeadlineRepository
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.core.domain.model.toModel
import ru.medyannikov.newsapi.core.domain.repostories.common.CacheRequestRepository
import javax.inject.Inject

class TopHeadlineRepositoryImpl
@Inject constructor(private val apiTopHeadlines: TopHeadlinesNetworkProvider) : CacheRequestRepository<Int, PagingData<ArticleModel>>(), TopHeadlineRepository {

    override fun getTopHeadlinesArticles(page: Int, params: Map<String, String>): Single<PagingData<ArticleModel>> {
        val keyParams = params.hashCode() + page
        val cachedItems = getCached(keyParams)
        return if (cachedItems != null) {
            Single.just(cachedItems)
        } else {
            apiTopHeadlines.getTopHeadlines(page, params)
                    .map { response ->
                        val items = response.articles.map { articleDTO -> articleDTO.toModel() }
                        return@map PagingData(
                                items = items,
                                page = page,
                                totalCount = response.totalResults)
                    }
        }.doOnSuccess { putData(keyParams, it) }
    }
}