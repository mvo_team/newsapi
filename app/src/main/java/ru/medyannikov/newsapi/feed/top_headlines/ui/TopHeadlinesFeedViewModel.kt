package ru.medyannikov.newsapi.feed.top_headlines.ui

import ru.medyannikov.newsapi.core.domain.error.ErrorMessageFactory
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.feed.common.BaseFeedViewModel
import ru.medyannikov.newsapi.feed.top_headlines.domain.interactor.TopHeadlinesInteractor
import ru.medyannikov.newsapi.filter.core.domain.model.TopHeadlinesFilterParams
import ru.medyannikov.newsapi.filter.top_headlines.domain.provider.impl.TopHeadlinesFilterProviderImpl
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject

class TopHeadlinesFeedViewModel
@Inject constructor(private val interactor: TopHeadlinesInteractor,
                    private val mainRouter: MainRouter,
                    private val filterProvider: TopHeadlinesFilterProviderImpl,
                    private val errorMessageFactory: ErrorMessageFactory,
                    private val navigationMenuController: NavigationMenuController,
                    private val schedulerProvider: SchedulerProvider) : BaseFeedViewModel(mainRouter, schedulerProvider) {

    init {
        subscribeChangeFilterParams()
    }

    override fun loadArticles() {
        filterParamsAsync { params ->
            loadArticleByParams(params)
        }
    }

    override fun openFilter() {
        mainRouter.openTopHeadlineFilterScreen()
    }

    override fun openNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.TOP_HEADLINES)
    }

    override fun onActionSearch(query: String?) {
        filterProvider.setQuery(query)
    }

    override fun loadNextPage() {
        if (isLoading.value == false) {
            isLoading.value = true
            filterParamsAsync { params ->
                listArticles.value?.let { item ->
                    val nextPage = item.page + 1
                    if (nextPage <= item.totalPage && !item.isLast) {
                        interactor.getTopHeadlinesArticles(page = nextPage, filterParams = params)
                                .observeOn(schedulerProvider.ui())
                                .withProgress()
                                .subscribe(
                                        { onLoadedPagingItems(it) },
                                        { onErrorLoading(it) })
                                .addToDisposable()
                    }
                }
            }
        }
    }

    private fun loadArticleByParams(params: TopHeadlinesFilterParams) {
        interactor.getTopHeadlinesArticles(filterParams = params)
                .observeOn(schedulerProvider.ui())
                .withProgress()
                .subscribe(
                        { onLoadedArticles(it) },
                        {
                            onErrorLoading(it)
                            listArticles.value = null
                        })
                .addToDisposable()
    }

    private fun onErrorLoading(it: Throwable?) {
        parseThrowable(it) {
            errorMessage.value = errorMessageFactory.getErrorMessage(it)
        }
    }

    private fun onLoadedArticles(list: PagingData<ArticleModel>) {
        listArticles.value = list
    }

    private fun subscribeChangeFilterParams() {
        filterProvider.currentState().baseSubscribe { loadArticles() }
    }

    private fun filterParamsAsync(action: (TopHeadlinesFilterParams) -> Unit) = filterProvider.filterParamsAsync()
            .observeOn(schedulerProvider.ui())
            .withProgress()
            .subscribe { params -> action(params) }
            .addToDisposable()
}