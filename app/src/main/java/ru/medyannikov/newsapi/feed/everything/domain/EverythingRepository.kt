package ru.medyannikov.newsapi.feed.everything.domain

import io.reactivex.Single
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.feed.everything.domain.params.EverythingParams

interface EverythingRepository {

  fun getEverythingArticles(page: Int = 1, params: EverythingParams): Single<PagingData<ArticleModel>>
}