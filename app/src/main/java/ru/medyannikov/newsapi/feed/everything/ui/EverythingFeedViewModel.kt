package ru.medyannikov.newsapi.feed.everything.ui

import ru.medyannikov.newsapi.core.domain.error.ErrorMessageFactory
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.feed.common.BaseFeedViewModel
import ru.medyannikov.newsapi.feed.everything.domain.interactor.EverythingInteractor
import ru.medyannikov.newsapi.filter.core.domain.model.EverythingFilterParams
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.EverythingFilterProvider
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject

class EverythingFeedViewModel
@Inject constructor(
        private val interactor: EverythingInteractor,
        private val mainRouter: MainRouter,
        private val filterProvider: EverythingFilterProvider,
        private val errorMessageFactory: ErrorMessageFactory,
        private val navigationMenuController: NavigationMenuController,
        private val schedulerProvider: SchedulerProvider
) : BaseFeedViewModel(mainRouter, schedulerProvider) {

    init {
        subscribeChangeFilterParams()
    }

    override fun loadArticles() {
        filterParamsAsync { params ->
            loadArticleByParams(params)
        }
    }

    override fun openFilter() {
        mainRouter.openEverythingFilterScreen()
    }

    override fun openNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.EVERYTHING)
    }

    override fun onActionSearch(query: String?) {
        filterProvider.setQuery(query)
    }

    override fun loadNextPage() {
        if (isLoading.value == false) {
            isLoading.value = true
            filterParamsAsync { params ->
                listArticles.value?.let { item ->
                    val nextPage = item.page + 1
                    if (nextPage <= item.totalPage && !item.isLast) {
                        interactor.getEverythingArticles(page = nextPage, params = params)
                                .observeOn(schedulerProvider.ui())
                                .withProgress()
                                .subscribe(
                                        { onLoadedPagingItems(it) },
                                        { onErrorLoading(it) })
                                .addToDisposable()
                    }
                }
            }
        }
    }

    private fun loadArticleByParams(params: EverythingFilterParams) {
        interactor.getEverythingArticles(params = params)
            .observeOn(schedulerProvider.ui())
            .withProgress()
            .subscribe(
                { onLoadedArticles(it) },
                {
                    onErrorLoading(it)
                    listArticles.value = null
                })
            .addToDisposable()
    }

    private fun onErrorLoading(it: Throwable?) {
        parseThrowable(it) {
            errorMessage.value = errorMessageFactory.getErrorMessage(it)
        }
    }

    private fun onLoadedArticles(list: PagingData<ArticleModel>) {
        listArticles.value = list
    }

    private fun subscribeChangeFilterParams() {
        filterProvider.currentState().baseSubscribe { loadArticles() }
    }

    private fun filterParamsAsync(action: (EverythingFilterParams) -> Unit) = filterProvider.filterParamsAsync()
        .observeOn(schedulerProvider.ui())
        .withProgress()
        .subscribe { params -> action(params) }
        .addToDisposable()
}