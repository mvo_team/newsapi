package ru.medyannikov.newsapi.feed.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.core.domain.model.SearchEvent
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import java.util.concurrent.TimeUnit

abstract class BaseFeedViewModel(private val mainRouter: MainRouter,
                                 private val schedulerProvider: SchedulerProvider) : BaseViewModel() {

    protected val listArticles by lazy { MutableLiveData<PagingData<ArticleModel>?>() }
    protected val searchSubject by lazy { PublishSubject.create<SearchEvent>() }

    init {
        subscribeSearch()
    }

    fun getArticles(): LiveData<PagingData<ArticleModel>?> = listArticles

    fun setScrollerObservable(observable: Observable<Int>) = observable.baseSubscribe { loadNextPage() }

    fun onRefresh() = loadArticles()

    fun openSettings() = mainRouter.openSettingsScreen()

    fun onSearch(query: String?) = searchSubject.onNext(SearchEvent(query))

    fun tapingQuery(query: String) = searchSubject.onNext(SearchEvent(query))

    protected fun onLoadedPagingItems(pagingData: PagingData<ArticleModel>) {
        val originalList = listArticles.value
        val originalItems = originalList?.items
        val newItems = originalItems?.plus(pagingData.items)
                ?: pagingData.items
        val copy = pagingData.copy(
                page = pagingData.page,
                totalCount = pagingData.totalCount,
                items = newItems
        )
        listArticles.value = copy
    }

    abstract fun loadArticles()

    abstract fun loadNextPage()

    abstract fun openNavigationMenu()

    abstract fun openFilter()

    abstract fun onActionSearch(query: String?)

    private fun subscribeSearch() {
        searchSubject
                .subscribeOn(schedulerProvider.computation())
                .debounce(1L, TimeUnit.SECONDS)
                .distinctUntilChanged()
                .observeOn(schedulerProvider.ui())
                .baseSubscribe { onActionSearch(it.query) }
    }

    fun onClickArticle(model: ArticleModel) {
        mainRouter.openArticleDetailScreen(model)
    }
}