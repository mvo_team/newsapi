package ru.medyannikov.newsapi.feed.top_headlines.ui

import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.feed.common.BaseFeedFragment

class TopHeadlineFragment : BaseFeedFragment<TopHeadlinesFeedViewModel>(), Injectable {

    override fun getViewModel(): TopHeadlinesFeedViewModel = provideVm()

    override fun getTitle(): String = getString(R.string.title_top_headlines)

    companion object {

        fun newInstance(): TopHeadlineFragment = TopHeadlineFragment()
    }
}