package ru.medyannikov.newsapi.feed.common

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import kotlinx.android.synthetic.main.fragment_feed.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.RecyclerScrollListener
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.BaseAdapter
import ru.medyannikov.newsapi.core.ui.adapter.delegates.article.*
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.filter.core.ui.decorator.BetweenItemsDividerDecoration
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged
import ru.medyannikov.newsapi.utils.ui.getLayoutManager
import ru.medyannikov.newsapi.utils.ui.hideKeyboard

abstract class BaseFeedFragment<T : BaseFeedViewModel> : BaseFragment<T>(), Injectable {

    private val adapter: BaseAdapter<Any> by lazy { buildAdapter() }
    private val scrollerListener by lazy { RecyclerScrollListener() }

    override fun getLayoutId(): Int = R.layout.fragment_feed

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = getViewModel()
        vm.attachBaseListeners()
        setHasOptionsMenu(true)
    }

    abstract fun getViewModel(): T

    abstract fun getTitle(): String

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_filter -> vm.openFilter()
        }

        return true
    }

    override fun onStart() {
        super.onStart()
        vm.openNavigationMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_filter, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(configuration = ActivityConfiguration(
                title = getTitle()), toolbar = toolbar)


        initViews()
        observeViewModel()
    }

    override fun openFilter() {
        vm.openFilter()
    }

    override fun openSettings() {
        vm.openSettings()
    }

    private fun initViews() {
        initSearchView()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = requireContext().getLayoutManager(adapter)
        recyclerView.addItemDecoration(BetweenItemsDividerDecoration(requireContext(), DividerItemDecoration.VERTICAL, R.drawable.spacing_divider))
        recyclerView.addOnScrollListener(scrollerListener)

        vm.setScrollerObservable(scrollerListener.scrolledToTheEndObservable)
        refreshLayout.setOnRefreshListener { vm.onRefresh() }
    }

    private fun initSearchView() {
        searchView.imeOptions = EditorInfo.IME_ACTION_SEARCH
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                vm?.onSearch(query)
                activity?.hideKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                vm?.tapingQuery(newText)
                return true
            }
        })
    }

    private fun observeViewModel() {
        vm.getArticles().observe(this, Observer {
            it?.let { data ->
                if (data.isEmpty()) {
                    adapter.setData(listOf(NotFoundItem))
                } else {
                    adapter.setData(data.items)
                }
            } ?: run {
                adapter.setData(listOf(ErrorItem))
            }
        })

        vm.getLoading().observe(this, Observer { refreshLayout.isRefreshing = it })
    }

    private fun buildAdapter(): BaseAdapter<Any> {
        return BaseAdapter<Any>().apply {
            addDelegate(ArticleAdapterDelegate { vm.onClickArticle(it) })
            addDelegate(NotFoundAdapterDelegate())
            addDelegate(ErrorAdapterDelegate())
        }
    }
}