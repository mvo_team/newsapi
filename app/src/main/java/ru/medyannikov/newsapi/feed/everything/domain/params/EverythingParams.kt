package ru.medyannikov.newsapi.feed.everything.domain.params

import org.threeten.bp.LocalDate
import ru.medyannikov.newsapi.filter.core.domain.model.EverythingFilterParams
import ru.medyannikov.newsapi.utils.date.formatSimple
import ru.medyannikov.newsapi.utils.params.putNotNull

data class EverythingParams(val query: String? = null,
                            val sources: List<String>? = null,
                            val domains: List<String>? = null,
                            val excludeDomains: List<String>? = null,
                            val from: LocalDate? = null,
                            val to: LocalDate? = null,
                            val language: String? = null,
                            val sortBy: SortBy = SortBy.PUBLISHED_AT) {

    fun toMap(): Map<String, String> {
        return mutableMapOf<String, String>().apply {
            putNotNull("q", query)
            putNotNull("sources", sources)
            putNotNull("domains", domains)
            putNotNull("excludeDomains", excludeDomains)
            putNotNull("from", from?.formatSimple())
            putNotNull("to", to?.formatSimple())
            putNotNull("language", language)
            putNotNull("sortBy", sortBy.sortName)
        }
    }

    companion object {

        fun fromFilterParams(filterParams: EverythingFilterParams): EverythingParams {
            return EverythingParams(query = filterParams.query,
                    language = filterParams.language?.isoValue,
                    sources = filterParams.sources?.map { it.id ?: "" }?.sorted(),
                    domains = filterParams.domains?.sorted(),
                    excludeDomains = filterParams.excludeDomains?.sorted(),
                    sortBy = filterParams.sortBy,
                    to = filterParams.to,
                    from = filterParams.from)
        }
    }
}