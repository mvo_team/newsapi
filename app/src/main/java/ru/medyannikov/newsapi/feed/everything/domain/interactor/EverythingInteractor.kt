package ru.medyannikov.newsapi.feed.everything.domain.interactor

import io.reactivex.Single
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.PagingData
import ru.medyannikov.newsapi.filter.core.domain.model.EverythingFilterParams

interface EverythingInteractor {

    fun getEverythingArticles(page: Int = 1, params: EverythingFilterParams): Single<PagingData<ArticleModel>>
}