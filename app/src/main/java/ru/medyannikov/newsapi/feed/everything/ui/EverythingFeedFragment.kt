package ru.medyannikov.newsapi.feed.everything.ui

import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.feed.common.BaseFeedFragment

class EverythingFeedFragment : BaseFeedFragment<EverythingFeedViewModel>(), Injectable {

    override fun getViewModel(): EverythingFeedViewModel = provideVm()

    override fun getTitle(): String = getString(R.string.title_everything)

    companion object {

        fun newInstance(): EverythingFeedFragment = EverythingFeedFragment()
    }
}