package ru.medyannikov.newsapi.arcticle_detail

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.fragment_article_detail.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.image_view.ImageViewDialog
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged
import ru.medyannikov.newsapi.utils.date.toReadableDate
import ru.medyannikov.newsapi.utils.ui.loadImage
import kotlin.math.absoluteValue

class ArticleDetailFragment : BaseFragment<ArticleDetailViewModel>(),
    Injectable {

    private var isShow: Boolean = false
    private var appBarOffsetListener: AppBarLayout.OnOffsetChangedListener? = null

    override fun getLayoutId(): Int = R.layout.fragment_article_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = provideVm()
        vm.attachBaseListeners()

        if (savedInstanceState == null) {
            val articleModel = arguments?.getParcelable<ArticleModel>(ARTICLE_KEY)
            vm.initArticle(articleModel)
        }
    }

    override fun onStart() {
        super.onStart()
        vm.hideNavigationMenu()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initViews()
        observeViewModel()
    }

    private fun initToolbar() {
        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(
            ActivityConfiguration(
                title = getString(R.string.title_article_detail),
                hasBackButton = true
            ), toolbar
        )

        if (appBarOffsetListener == null) {
            appBarOffsetListener = object : AppBarLayout.OnOffsetChangedListener {
                private var scrollRange = -1

                override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.totalScrollRange
                    }
                    val alpha = verticalOffset.absoluteValue.toFloat() / scrollRange
                    if (alpha > 0.5f) {
                        toolbar?.toolbar_title?.alpha = alpha
                    } else {
                        toolbar?.toolbar_title?.alpha = 0f
                    }
                    if (scrollRange + verticalOffset == 0) {
                        if (!isShow) {
                            isShow = true
                        }
                    } else if (isShow) {
                        isShow = false
                    }
                }
            }
        }
        appBarLayout.addOnOffsetChangedListener(appBarOffsetListener)
    }

    private var dialog: ImageViewDialog? = null

    private fun observeViewModel() {
        vm.getArticle().observe(this, Observer { item ->
            ivArticlePreview.loadImage(item.urlToImage)

            tvArticleTitle.text = item.title
            tvArticleDescription.text = item.content
            tvArticleAuthor.text = item.author
            tvArticlePublishDate.text = item.publishedAt?.toReadableDate()

            btnOpenArticle.setOnClickListener { vm.openArticle(item) }

            ivArticlePreview.setOnClickListener {
                if (dialog == null) {
                    dialog = ImageViewDialog.newInstance(item.urlToImage)
                }
                if (dialog?.isVisible == false) {
                    dialog?.show(childFragmentManager, "SHOW_IMAGE")
                }
            }
        })
    }

    private fun initViews() {

    }

    companion object {

        private const val ARTICLE_KEY = "article_key"

        fun newInstance(articleModel: ArticleModel) = ArticleDetailFragment().apply {
            arguments = bundleOf(ARTICLE_KEY to articleModel)
        }
    }
}