package ru.medyannikov.newsapi.arcticle_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import javax.inject.Inject

class ArticleDetailViewModel
    @Inject constructor(private val navigationMenuController: NavigationMenuController,
                        private val mainRouter: MainRouter): BaseViewModel() {

    private val article by lazy { MutableLiveData<ArticleModel>() }

    fun initArticle(model: ArticleModel?) {
        if (model == null) {
            mainRouter.exit()
            return
        }
        article.value = model
    }

    fun getArticle(): LiveData<ArticleModel> = article

    fun hideNavigationMenu() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }

    fun openArticle(item: ArticleModel) {
        item.url?.let { mainRouter.openArticle(it) }
    }
}