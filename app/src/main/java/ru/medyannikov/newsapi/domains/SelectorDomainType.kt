package ru.medyannikov.newsapi.domains

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class SelectorDomainType : Parcelable {
    INCLUDE, EXCLUDE
}