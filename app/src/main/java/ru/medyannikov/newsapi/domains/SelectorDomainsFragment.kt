package ru.medyannikov.newsapi.domains

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_domain_selector.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.BaseAdapter
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.filter.core.ui.selected.SelectedAdapterDelegate
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged

class SelectorDomainsFragment : BaseFragment<SelectedDomainsViewModel>(), Injectable {

    private val adapterDomains: BaseAdapter<String> by lazy { buildAdapter() }

    override fun getLayoutId(): Int = R.layout.fragment_domain_selector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = provideVm()
        vm.attachBaseListeners()

        if (savedInstanceState == null) {
            val preSelectedItems = arguments?.get(PRE_SELECTEd_KEY) as? List<String> ?: arrayListOf()
            vm.setPreselectedItems(preSelectedItems)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val type = arguments?.getParcelable<SelectorDomainType>(TYPE_KEY) ?: throw IllegalArgumentException("Type not defied. TYPE_KEY is null")
        vm.setType(type)
        initViews()
        observeVm()
    }

    private fun observeVm() {
        vm.getDomainList().observe(this, Observer {
            adapterDomains.setData(it)
            adapterDomains.notifyDataSetChanged()
        })

        vm.getType().observe(this, Observer {
            val title: String = when (it) {
                SelectorDomainType.EXCLUDE -> getString(R.string.title_exclude_domain)
                else -> getString(R.string.title_select_domain)
            }
            initToolbar(title)
        })

        vm.isButtonVisible().observe(this, Observer { btnApplySelector.isVisible = it })
    }

    private fun initToolbar(title: String) {
        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(
                configuration = ActivityConfiguration(
                        title = title, hasBackButton = true
                ), toolbar = toolbar
        )
    }

    private fun initViews() {
        initListView()

        btnAddDomain.setOnClickListener {
            val domain = etDomain.text
            vm.onClickAddDomain(domain)
            etDomain.text.clear()
        }

        btnApplySelector.setOnClickListener { vm.onClickSelect(adapterDomains.getItems()) }
    }

    private fun initListView() {
        recyclerView.adapter = adapterDomains
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun buildAdapter(): BaseAdapter<String> {
        return BaseAdapter<String>().apply {
            addDelegate(
                SelectedAdapterDelegate<String>(
                    listener = { vm.onRemoveItem(it) },
                    renderParams = SelectedAdapterDelegate.SelectedRenderParams.getRenderParams(
                        resources,
                        R.dimen.indent_16,
                        R.dimen.indent_0
                    )
                )
            )
        }
    }

    companion object {

        private const val TYPE_KEY = "type_key"
        private const val PRE_SELECTEd_KEY = "preselected_key"

        fun newInstance(
            preSelectedItems: List<String>,
            type: SelectorDomainType
        ): SelectorDomainsFragment {
            return SelectorDomainsFragment().apply {
                arguments = bundleOf(
                    TYPE_KEY to type,
                    PRE_SELECTEd_KEY to preSelectedItems
                )
            }
        }
    }
}