package ru.medyannikov.newsapi.domains

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.locale.LocaleManager
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.EverythingFilterProvider
import ru.medyannikov.newsapi.main.navigation.MainRouter
import javax.inject.Inject

class SelectedDomainsViewModel
@Inject constructor(private val mainRouter: MainRouter,
                    private val filterProvider: EverythingFilterProvider,
                    private val localeManager: LocaleManager) : BaseViewModel() {

    private val domainList by lazy { MutableLiveData<List<String>>() }
    private val isButtonVisible by lazy { MutableLiveData<Boolean>() }
    private val type by lazy { MutableLiveData<SelectorDomainType>() }
    private val preSelectedItems: MutableList<String> = mutableListOf()

    fun getDomainList(): LiveData<List<String>> = domainList

    fun getType(): LiveData<SelectorDomainType> = type

    fun isButtonVisible(): LiveData<Boolean> = isButtonVisible

    fun onClickSelect(items: List<String>) {
        val type = this.type.value ?: run {
            mainRouter.exit()
            return
        }
        when (type) {
            SelectorDomainType.EXCLUDE -> filterProvider.setExcludeDomains(items)
            else -> filterProvider.setDomains(items)
        }

        mainRouter.exit()
    }

    fun onClickAddDomain(text: CharSequence) {
        val emptyText = text.trim().toString()
        if (containsItem(emptyText)) {
            errorMessage.value = localeManager.getString(R.string.error_domain_contains)
            return
        }

        val newList = domainList.value?.toMutableSet() ?: mutableSetOf()
        newList.add(emptyText)
        domainList.value = newList.toList()

        updateButtonState()
    }

    fun setPreselectedItems(preSelectedItems: List<String>) {
        this.preSelectedItems.clear()
        this.preSelectedItems.addAll(preSelectedItems)

        domainList.value = preSelectedItems.toList()
    }

    fun onRemoveItem(item: String) {
        val newList = domainList.value?.toMutableList() ?: mutableListOf()
        newList.remove(item)
        domainList.value = newList

        updateButtonState()
    }

    fun setType(type: SelectorDomainType) {
        this.type.value = type
    }

    private fun updateButtonState() {
        val selectedItems = domainList.value ?: emptyList()
        isButtonVisible.value = preSelectedItems != selectedItems
    }

    private fun containsItem(text: String): Boolean {
        return domainList.value?.contains(text) == true
    }
}