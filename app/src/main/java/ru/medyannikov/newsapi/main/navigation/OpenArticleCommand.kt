package ru.medyannikov.newsapi.main.navigation

import ru.terrakok.cicerone.commands.Command

class OpenArticleCommand(val url: String): Command