package ru.medyannikov.newsapi.main.navigation

import android.net.Uri
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.di.dagger.injectSingleton
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class MainScreenNavigator(
    private val activity: AppCompatActivity,
    @IdRes containerId: Int
) : SupportAppNavigator(activity, containerId) {

    private val appPrefs by injectSingleton { appPrefs }

    override fun applyCommand(command: Command?) {
        when(command) {
            is OpenArticleCommand -> { openArticle(command.url) }
            else ->  super.applyCommand(command)
        }
    }

    private fun openArticle(url: String) {
        CustomTabsIntent.Builder().addDefaultShareMenuItem()
            .enableUrlBarHiding()
            .addDefaultShareMenuItem()
            .setShowTitle(true)
            .setToolbarColor(ContextCompat.getColor(activity, R.color.color_white))
            .setInstantAppsEnabled(false)
            .build()
            .launchUrl(activity, Uri.parse(url))
    }
}