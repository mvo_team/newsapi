package ru.medyannikov.newsapi.main.navigation

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigationMenuController @Inject constructor() {

    private val menuState = BehaviorSubject.create<NavigationItem>()

    /**
     * Текущее состояние навигационного меню
     */
    fun getMenuState(): Observable<NavigationItem> = menuState

    /**
     * Указать состояние навигационного меню
     */
    fun setMenuState(item: NavigationItem) {
        menuState.onNext(item)
    }
}