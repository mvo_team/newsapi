package ru.medyannikov.newsapi.main.toolbar

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import ru.medyannikov.newsapi.R

class NewsApiToolbar
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1) : Toolbar(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.layout_toolbar, this)
    }

    override fun setTitle(resId: Int) {
        toolbar_title.setText(resId)
    }

    override fun setTitle(title: CharSequence?) {
        toolbar_title.text = title
    }
}