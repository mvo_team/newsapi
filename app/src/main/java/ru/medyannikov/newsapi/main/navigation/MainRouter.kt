package ru.medyannikov.newsapi.main.navigation

import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.domains.SelectorDomainType
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRouter @Inject constructor() : Router() {

    fun openMainScreen() = newRootScreen(Screens.mainScreen())
    fun openFeedScreen() = newRootScreen(Screens.everythingFeedScreen())
    fun openEverythingFilterScreen() = navigateTo(Screens.everythingFilterFragment())
    fun openTopHeadlineFilterScreen() = navigateTo(Screens.topHeadlineFilterScreen())
    fun openTopHeadlinesScreen() = newRootScreen(Screens.topHeadlinesScreen())
    fun openLanguageSelectorScreen() = navigateTo(Screens.languageSelectorScreen())
    fun openSettingsScreen() = newRootScreen(Screens.settingsScreen())
    fun openShare(title: String, message: String) = navigateTo(Screens.shareApp(title, message))

    fun openEverythingSourceSelector(preSelectedItems: List<SourceModel>) = navigateTo(Screens.everythingSourceSelector(preSelectedItems))
    fun openTopHeadlinesSourceSelector(preSelectedItems: List<SourceModel>) = navigateTo(Screens.topHeadlinesSourceSelector(preSelectedItems))

    fun openDomainSelector(preSelectedItems: List<String>,
                           type: SelectorDomainType) = navigateTo(Screens.domainSelector(preSelectedItems, type))

    fun openPrivacyScreen() = navigateTo(Screens.privacyScreen())
    fun openAgreementScreen() = navigateTo(Screens.agreementScreen())
    fun sendSupportEmail() = navigateTo(Screens.sendSupportEmail())
    fun openAboutScreen() = navigateTo(Screens.aboutScreen())
    fun openApiKeyScreen() = navigateTo(Screens.apiKeyScreen())
    fun openArticleDetailScreen(model: ArticleModel) = navigateTo(Screens.articleDetail(model))
    fun openArticle(url: String) = executeCommands(OpenArticleCommand(url))
}