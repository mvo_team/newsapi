package ru.medyannikov.newsapi.main

import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.MainRouter
import javax.inject.Inject

class MainViewModel
@Inject constructor(private val mainRouter: MainRouter) : BaseViewModel() {

    fun onBackPressed() {
        mainRouter.exit()
    }

    fun openEverything() {
        mainRouter.openFeedScreen()
    }

    fun openTopHeadlines() {
        mainRouter.openTopHeadlinesScreen()
    }

    fun openSettings() {
        mainRouter.openSettingsScreen()
    }
}