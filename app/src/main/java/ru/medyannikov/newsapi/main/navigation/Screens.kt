package ru.medyannikov.newsapi.main.navigation

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import ru.medyannikov.newsapi.arcticle_detail.ArticleDetailFragment
import ru.medyannikov.newsapi.core.domain.model.ArticleModel
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.domains.SelectorDomainType
import ru.medyannikov.newsapi.domains.SelectorDomainsFragment
import ru.medyannikov.newsapi.feed.everything.ui.EverythingFeedFragment
import ru.medyannikov.newsapi.feed.top_headlines.ui.TopHeadlineFragment
import ru.medyannikov.newsapi.filter.everything_filter.ui.EverythingFilterFragment
import ru.medyannikov.newsapi.filter.top_headlines.ui.TopHeadlinesFilterFragment
import ru.medyannikov.newsapi.main.MainActivity
import ru.medyannikov.newsapi.news_source.ui.everything.EverythingSourceFragment
import ru.medyannikov.newsapi.news_source.ui.top_headlines.TopHeadlinesSourceFragment
import ru.medyannikov.newsapi.settings.SettingsFragment
import ru.medyannikov.newsapi.settings.about.AboutFragment
import ru.medyannikov.newsapi.settings.agreement.AgreementFragment
import ru.medyannikov.newsapi.settings.api_key.ApiKeyFragment
import ru.medyannikov.newsapi.settings.language.LanguageSelectorFragment
import ru.medyannikov.newsapi.utils.ui.sendEmailWithThemeIntent
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    fun mainScreen() = activityScreen(configurator = { Intent(it, MainActivity::class.java) })
    fun everythingFeedScreen() = fragmentScreen { EverythingFeedFragment.newInstance() }
    fun topHeadlinesScreen() = fragmentScreen { TopHeadlineFragment.newInstance() }
    fun settingsScreen() = fragmentScreen { SettingsFragment.newInstance() }
    fun everythingFilterFragment() = fragmentScreen { EverythingFilterFragment.newInstance() }
    fun topHeadlineFilterScreen() = fragmentScreen { TopHeadlinesFilterFragment.newInstance() }
    fun languageSelectorScreen() = fragmentScreen { LanguageSelectorFragment.newInstance() }
    fun everythingSourceSelector(preSelectedItems: List<SourceModel>) = fragmentScreen { EverythingSourceFragment.newInstance(preSelectedItems) }
    fun topHeadlinesSourceSelector(preSelectedItems: List<SourceModel>) = fragmentScreen { TopHeadlinesSourceFragment.newInstance(preSelectedItems) }
    fun privacyScreen() = fragmentScreen { AgreementFragment.newInstance(AgreementFragment.AgreementPage.PRIVACY) }
    fun agreementScreen() = fragmentScreen { AgreementFragment.newInstance(AgreementFragment.AgreementPage.AGREEMENT) }
    fun aboutScreen() = fragmentScreen { AboutFragment.newInstance() }
    fun apiKeyScreen() = fragmentScreen { ApiKeyFragment.newInstance() }
    fun articleDetail(model: ArticleModel) = fragmentScreen { ArticleDetailFragment.newInstance(model) }

    fun domainSelector(preSelectedItems: List<String>,
                       type: SelectorDomainType) = fragmentScreen { SelectorDomainsFragment.newInstance(preSelectedItems, type) }

    fun shareApp(title: String, message: String) = activityScreen(configurator = {
        val intent = Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, message)
                .setType("text/plain")

        Intent.createChooser(intent, title)
    })

    fun sendSupportEmail() = activityScreen(configurator = {
        it.sendEmailWithThemeIntent()
    })

    private fun fragmentScreen(fragmentCreator: () -> Fragment): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return fragmentCreator()
            }
        }
    }

    private inline fun activityScreen(crossinline configurator: (context: Context) -> Intent): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getActivityIntent(context: Context): Intent {
                return configurator(context)
            }
        }
    }
}