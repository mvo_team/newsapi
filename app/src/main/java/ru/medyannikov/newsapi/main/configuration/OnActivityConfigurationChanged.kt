package ru.medyannikov.newsapi.main.configuration

import androidx.appcompat.widget.Toolbar

interface OnActivityConfigurationChanged {
  fun setActivityConfiguration(configuration: ActivityConfiguration, toolbar: Toolbar? = null)
}