package ru.medyannikov.newsapi.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*
import ru.medyannikov.bubblebottomnavigation.listener.BubbleBottomNavigationListener
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.ui.android.BaseActivity
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.utils.ui.OnBackPressedHandler
import ru.medyannikov.newsapi.utils.ui.hideKeyboard
import javax.inject.Inject

class MainActivity : BaseActivity(), HasSupportFragmentInjector, Injectable, OnActivityConfigurationChanged {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    lateinit var vm: MainViewModel

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = provideVm()
        vm.attachBaseListeners()

        initNavigation()

        if (savedInstanceState == null) {
            vm.openEverything()
        }
    }

    override fun onBackPressed() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.container)
        if (currentFragment is OnBackPressedHandler && currentFragment.onBackPressed()) {
            return
        }
        vm.onBackPressed()
    }

    override fun setActivityConfiguration(configuration: ActivityConfiguration, toolbar: Toolbar?) {
        setSupportActionBar(toolbar)

        supportActionBar?.title = configuration.title

        if (configuration.hasBackButton) {
            supportActionBar?.setHomeAsUpIndicator(configuration.backButtonRes)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        } else {
            supportActionBar?.setHomeAsUpIndicator(configuration.backButtonRes)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    private fun initNavigation() {
        navigationView.setNavigationChangeListener(object : BubbleBottomNavigationListener {
            override fun onNavigationChanged(view: View, position: Int) {
                hideKeyboard()
                when (view.id) {
                    R.id.menu_item_everything -> vm.openEverything()
                    R.id.menu_item_topheadlines -> vm.openTopHeadlines()
                    R.id.menu_item_settings -> vm.openSettings()
                }
            }
        })

        navigationMenuController.getMenuState()
            .subscribe { item -> prepareMenuByItem(item) }
            .addTo(compositeDisposable)
    }

    private fun prepareMenuByItem(item: NavigationItem) {
        navigationView.isVisible = item != NavigationItem.INVISIBLE
        navigationView.selectItemById(item.menuItemId)
    }
}