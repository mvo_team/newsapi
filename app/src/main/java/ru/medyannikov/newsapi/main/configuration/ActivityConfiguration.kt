package ru.medyannikov.newsapi.main.configuration

import ru.medyannikov.newsapi.R


class ActivityConfiguration(val title: String? = null,
                            val hasBackButton: Boolean = false,
                            val backButtonRes: Int = R.drawable.ic_toolbar_back)