package ru.medyannikov.newsapi.main.navigation

import androidx.annotation.IdRes
import ru.medyannikov.newsapi.R

enum class NavigationItem(@IdRes val menuItemId: Int) {
  EVERYTHING(R.id.menu_item_everything),
  TOP_HEADLINES(R.id.menu_item_topheadlines),
  SETTINGS(R.id.menu_item_settings),
  INVISIBLE(0)
}