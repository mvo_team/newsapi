package ru.medyannikov.newsapi.news_source.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_selector_item.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.AdapterDelegate

class SourceAdapterDelegate(private val titleGetter: (item: SourceModel) -> String,
                            private val toggleItem: (item: SourceModel) -> Unit): AdapterDelegate<SelectedItem<SourceModel>>() {

    override fun isForViewType(item: SelectedItem<SourceModel>, position: Int): Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_selector_item, parent, false)
        return SelectorViewHolder(view, titleGetter)
    }

    override fun onBindViewHolder(item: SelectedItem<SourceModel>, position: Int, holder: RecyclerView.ViewHolder) {
        holder as SelectorViewHolder

        holder.onBind(item)
    }

    inner class SelectorViewHolder(override val containerView: View,
                                   private val titleGetter: (item: SourceModel) -> String) :
            RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun onBind(item: SelectedItem<SourceModel>) {
            val isSelected = item.isChecked
            tvTitle.text = titleGetter(item.item)
            cbItem.isChecked = isSelected

            val bgResource = when (isSelected) {
                true -> R.color.color_selected
                else -> android.R.color.white
            }
            clListItemContainer.setBackgroundResource(bgResource)

            clListItemContainer.setOnClickListener { toggleItem(item.item) }
        }
    }
}