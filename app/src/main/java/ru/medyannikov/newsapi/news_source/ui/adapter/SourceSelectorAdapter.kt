/*
package ru.medyannikov.newsapi.news_source.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_selecteble.tvTitle
import kotlinx.android.synthetic.main.layout_selector_item.*
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.core.ui.SelectableAdapter

class SourceSelectorAdapter(private val preSelectedItems: List<SourceModel>,
        private val titleGetter: (item: SourceModel) -> String,
                            private val isChanged: (isCHanged: Boolean) -> Unit) : ListAdapter<SelectedItem<SourceModel>, SourceSelectorAdapter.SelectorViewHolder>(SelectableAdapter.DiffCallback()) {

    private val selectedItems: MutableList<SourceModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectorViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_selector_item, parent, false)
        return SelectorViewHolder(view, titleGetter)
    }

    override fun onBindViewHolder(holder: SelectorViewHolder, position: Int) = holder.onBind(getItem(position))

    fun setPreSelectedItems(items: List<SourceModel>) {
        selectedItems.clear()
        selectedItems.addAll(items.toList())
        notifyDataSetChanged()
    }

    fun toggleItem(item: SourceModel) {
        val itemPosition = currentList.indexOf(item)
        if (selectedItems.contains(item)) {
            selectedItems.remove(item)
        } else {
            selectedItems.add(item)
        }

        isChanged(preSelectedItems != selectedItems)
        notifyItemChanged(itemPosition)
    }

    fun getSelectedItems() = selectedItems

    inner class SelectorViewHolder(override val containerView: View,
                                   private val titleGetter: (item: SourceModel) -> String) :
            RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun onBind(item: SourceModel) {
            val isSelected = selectedItems.contains(item)
            tvTitle.text = titleGetter(item)
            cbItem.isChecked = isSelected

            val bgResource = when (isSelected) {
                true -> R.color.color_selected
                else -> android.R.color.white
            }
            clListItemContainer.setBackgroundResource(bgResource)

            clListItemContainer.setOnClickListener { toggleItem(item) }
        }
    }
}*/
