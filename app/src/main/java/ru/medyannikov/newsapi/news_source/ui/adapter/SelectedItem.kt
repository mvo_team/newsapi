package ru.medyannikov.newsapi.news_source.ui.adapter

data class SelectedItem<T>(val item: T, var isChecked: Boolean = false)