package ru.medyannikov.newsapi.news_source.ui.everything

import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.everything_filter.domain.provider.EverythingFilterProvider
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.news_source.domain.interactor.SourceInteractor
import ru.medyannikov.newsapi.news_source.ui.common.SourceSelectorViewModel
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject

class EverythingSourceViewModel
@Inject constructor(
    sourceInteractor: SourceInteractor,
    mainRouter: MainRouter,
    schedulerProvider: SchedulerProvider,
    navigationMenuController: NavigationMenuController,
    private val filterProvider: EverythingFilterProvider
) : SourceSelectorViewModel(sourceInteractor, mainRouter, schedulerProvider, navigationMenuController) {

    override fun onSaveResult(selectedItems: List<SourceModel>) {
        filterProvider.setSources(selectedItems)
    }
}