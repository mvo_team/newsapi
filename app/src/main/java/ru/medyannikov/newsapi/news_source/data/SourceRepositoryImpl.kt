package ru.medyannikov.newsapi.news_source.data

import io.reactivex.Single
import ru.medyannikov.newsapi.core.data.db.provider.SourceDbProvider
import ru.medyannikov.newsapi.core.data.db.entity.toModel
import ru.medyannikov.newsapi.core.data.network.api.sources.provider.SourceNetworkProvider
import ru.medyannikov.newsapi.news_source.domain.SourceRepository
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.core.domain.model.toEntity
import ru.medyannikov.newsapi.core.domain.model.toModel
import javax.inject.Inject

class SourceRepositoryImpl
@Inject constructor(private val provider: SourceNetworkProvider,
    private val dbProvider: SourceDbProvider) : SourceRepository {

  override fun getAvailableSource(params: Map<String, String>): Single<List<SourceModel>> {
    return dbProvider.getSources()
        .map { it.map { item -> item.toModel() } }
        .flatMap {
          if (it.isEmpty()) {
            fromNetwork(params)
          } else {
            Single.just(it)
          }
        }
  }

  private fun fromNetwork(params: Map<String, String>): Single<List<SourceModel>> {
    return provider.getAvailableSources(params)
        .map { it.sources.map { source -> source.toModel() } }
        .doOnSuccess { sources -> sources.map { it.toEntity() }.let { dbProvider.insertSources(it) } }
  }
}