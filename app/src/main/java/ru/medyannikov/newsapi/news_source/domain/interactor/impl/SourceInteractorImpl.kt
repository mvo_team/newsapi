package ru.medyannikov.newsapi.news_source.domain.interactor.impl

import io.reactivex.Single
import ru.medyannikov.newsapi.news_source.domain.SourceRepository
import ru.medyannikov.newsapi.news_source.domain.interactor.SourceInteractor
import ru.medyannikov.newsapi.news_source.domain.params.SourceParams
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SourceInteractorImpl
@Inject constructor(private val repository: SourceRepository,
                    private val schedulerProvider: SchedulerProvider): SourceInteractor {

    override fun getAvailableSource(params: SourceParams): Single<List<SourceModel>> {
        return repository.getAvailableSource(params.toMap())
            .subscribeOn(schedulerProvider.io())
    }
}