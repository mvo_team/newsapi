package ru.medyannikov.newsapi.news_source.domain.interactor

import io.reactivex.Single
import ru.medyannikov.newsapi.news_source.domain.params.SourceParams
import ru.medyannikov.newsapi.core.domain.model.SourceModel

interface SourceInteractor {

    fun getAvailableSource(params: SourceParams): Single<List<SourceModel>>
}