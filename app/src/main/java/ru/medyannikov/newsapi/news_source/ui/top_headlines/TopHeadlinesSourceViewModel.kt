package ru.medyannikov.newsapi.news_source.ui.top_headlines

import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.filter.top_headlines.domain.provider.TopHeadlinesFilterProvider
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.news_source.domain.interactor.SourceInteractor
import ru.medyannikov.newsapi.news_source.ui.common.SourceSelectorViewModel
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import javax.inject.Inject

class TopHeadlinesSourceViewModel@Inject constructor(
    sourceInteractor: SourceInteractor,
    mainRouter: MainRouter,
    schedulerProvider: SchedulerProvider,
    navigationMenuController: NavigationMenuController,
    private val filterProvider: TopHeadlinesFilterProvider
) : SourceSelectorViewModel(sourceInteractor, mainRouter, schedulerProvider, navigationMenuController) {

    override fun onSaveResult(selectedItems: List<SourceModel>) {
        filterProvider.setSources(selectedItems)
    }
}