package ru.medyannikov.newsapi.news_source.ui.top_headlines

import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.news_source.ui.common.SourceSelectorFragment

class TopHeadlinesSourceFragment: SourceSelectorFragment<TopHeadlinesSourceViewModel>(), Injectable {

    override fun getViewModel(): TopHeadlinesSourceViewModel = provideVm()

    companion object {

        fun newInstance(preSelectedItems: List<SourceModel>) = TopHeadlinesSourceFragment().apply {
            arguments = getArguments(preSelectedItems)
        }
    }
}