package ru.medyannikov.newsapi.news_source.ui.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.subjects.PublishSubject
import ru.medyannikov.newsapi.core.domain.model.SearchEvent
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.core.ui.aac.BaseViewModel
import ru.medyannikov.newsapi.main.navigation.MainRouter
import ru.medyannikov.newsapi.main.navigation.NavigationItem
import ru.medyannikov.newsapi.main.navigation.NavigationMenuController
import ru.medyannikov.newsapi.news_source.domain.interactor.SourceInteractor
import ru.medyannikov.newsapi.news_source.domain.params.SourceParams
import ru.medyannikov.newsapi.news_source.ui.adapter.SelectedItem
import ru.medyannikov.newsapi.utils.aac.SingleLiveEvent
import ru.medyannikov.newsapi.utils.rx.SchedulerProvider
import java.util.concurrent.TimeUnit

abstract class SourceSelectorViewModel(
        private val sourceInteractor: SourceInteractor,
        private val mainRouter: MainRouter,
        private val schedulerProvider: SchedulerProvider,
        private val navigationMenuController: NavigationMenuController) : BaseViewModel() {

    private val items: ArrayList<SelectedItem<SourceModel>> = arrayListOf()
    private val selectorItems by lazy { MutableLiveData<List<SelectedItem<SourceModel>>>() }
    private val isChanged by lazy { MutableLiveData<Boolean>() }
    private val preSelectedItems by lazy { MutableLiveData<List<SourceModel>>() }
    private val onSelectAction by lazy { SingleLiveEvent<List<SourceModel>>() }
    private val searchSubject by lazy { PublishSubject.create<SearchEvent>() }

    init {
        subscribeSearch()
    }

    abstract fun onSaveResult(selectedItems: List<SourceModel>)

    fun getSelectorItems(): LiveData<List<SelectedItem<SourceModel>>> = selectorItems

    fun onSelectAction(): LiveData<List<SourceModel>> = onSelectAction

    fun onSearch(query: String?) = search(query)

    fun applyButton(): LiveData<Boolean> = isChanged

    fun tapingQuery(newText: String) = search(newText)

    fun onStart() {
        navigationMenuController.setMenuState(NavigationItem.INVISIBLE)
    }

    fun onClickAdd() {
        onSaveResult(getSelectedItems())
        closeScreen()
    }

    fun closeScreen() = mainRouter.exit()

    fun onRefresh() = loadSources()

    fun setPreSelectedItems(preSelectedItems: List<SourceModel>) {
        this.preSelectedItems.value = preSelectedItems

        loadSources()
    }

    fun toggleItem(selectedItem: SourceModel) {
        val index = items.indexOfFirst { it.item == selectedItem }

        if (index > -1) {
            items.getOrNull(index)?.let { it.isChecked = !it.isChecked }
        }
        selectorItems.value = items.toList()
        checkChanges()
    }

    private fun search(query: String?) {
        searchSubject.onNext(SearchEvent(query))
    }

    private fun subscribeSearch() {
        searchSubject
                .subscribeOn(schedulerProvider.computation())
                .debounce(350L, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .map { filteredData(it) }
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { isLoading.value = true }
                .doOnNext { isLoading.value = false }
                .baseSubscribe { selectorItems.value = it }
    }

    private fun filteredData(event: SearchEvent) = items.filter {
        it.item.name?.contains(event.query ?: "", true) == true
    }

    private fun loadSources() {
        sourceInteractor.getAvailableSource(SourceParams())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { isLoading.value = true }
                .doOnEvent { _, _ -> isLoading.value = false }
                .baseSubscribe { onLoadedSources(it) }
    }

    private fun onLoadedSources(list: List<SourceModel>) {
        val selectableList = list.map { source -> SelectedItem(source, isChecked = getPreSelectedItems().any { it.id == source.id}) }

        items.clear()
        items.addAll(selectableList)

        selectorItems.value = selectableList
    }

    private fun getSelectedItems() = items.filter { it.isChecked }.map { it.item }

    private fun checkChanges() {
        val preSelected = getPreSelectedItems()
        val selected = getSelectedItems()
        isChanged.value = !preSelected.containsAll(selected) || !selected.containsAll(preSelected)
    }

    private fun getPreSelectedItems() = this.preSelectedItems.value ?: emptyList()
    fun setListener(onSelectedListener: (items: List<SourceModel>) -> Unit) {

    }
}