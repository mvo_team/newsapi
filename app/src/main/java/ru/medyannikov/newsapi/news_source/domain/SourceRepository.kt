package ru.medyannikov.newsapi.news_source.domain

import io.reactivex.Single
import ru.medyannikov.newsapi.core.domain.model.SourceModel

interface SourceRepository {

    fun getAvailableSource(params: Map<String, String>): Single<List<SourceModel>>
}