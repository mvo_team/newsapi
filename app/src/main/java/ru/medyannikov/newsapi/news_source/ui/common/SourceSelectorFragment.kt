package ru.medyannikov.newsapi.news_source.ui.common

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_source_selector.*
import kotlinx.android.synthetic.main.fragment_source_selector.toolbar
import ru.medyannikov.newsapi.R
import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.core.ui.adapter.adapter_deleage.BaseAdapter
import ru.medyannikov.newsapi.core.ui.android.BaseFragment
import ru.medyannikov.newsapi.main.configuration.ActivityConfiguration
import ru.medyannikov.newsapi.main.configuration.OnActivityConfigurationChanged
import ru.medyannikov.newsapi.news_source.ui.adapter.SelectedItem
import ru.medyannikov.newsapi.news_source.ui.adapter.SourceAdapterDelegate
import ru.medyannikov.newsapi.utils.ui.hideKeyboard

abstract class SourceSelectorFragment<T: SourceSelectorViewModel> : BaseFragment<T>() {

    private lateinit var onSelectedListener: (items: List<SourceModel>) -> Unit

    override fun getLayoutId(): Int = R.layout.fragment_source_selector

    private val adapter by lazy { buildAdapter() }

    private fun buildAdapter() = BaseAdapter<SelectedItem<*>>()
            .addDelegate(SourceAdapterDelegate(titleGetter = { item -> item.name ?: "Unknown" },
                    toggleItem = { vm.toggleItem(it) }))


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = getViewModel()
        vm.attachBaseListeners()

        if (savedInstanceState == null) {
            val preSelectedItems: List<SourceModel> = arguments?.get(PRE_SELECTED_KEY) as? List<SourceModel>
                    ?: emptyList()
            vm.setPreSelectedItems(preSelectedItems)
        }
    }

    abstract fun getViewModel(): T

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? OnActivityConfigurationChanged)?.setActivityConfiguration(configuration = ActivityConfiguration(
                title = getString(R.string.title_source_selector), hasBackButton = true), toolbar = toolbar)

        initViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        vm.onStart()
    }

    private fun observeViewModel() {
        vm.getSelectorItems().observe(this, Observer {
            adapter.setData(it)
            adapter.notifyDataSetChanged()
        })

        vm.onSelectAction().observe(this, Observer {
            onSelectedListener(it)
            vm.closeScreen()
        })

        vm.applyButton().observe(this, Observer {
            btnApplyFilter.isVisible = it
        })

        vm.getLoading().observe(this, Observer { refreshLayout.isRefreshing = it })
    }

    private fun initViews() {
        initSearchView()
        initListView()

        refreshLayout.setOnRefreshListener { vm.onRefresh() }

        btnApplyFilter.setOnClickListener { vm.onClickAdd() }
    }

    private fun initListView() {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun initSearchView() {
        searchView.imeOptions = EditorInfo.IME_ACTION_SEARCH
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                vm.onSearch(query)
                activity?.hideKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                vm.tapingQuery(newText)
                return true
            }
        })
    }

    companion object {
        protected const val PRE_SELECTED_KEY = "pre_selected_key"

        fun getArguments(preSelectedItems: List<SourceModel>) = bundleOf(PRE_SELECTED_KEY to preSelectedItems)
    }
}