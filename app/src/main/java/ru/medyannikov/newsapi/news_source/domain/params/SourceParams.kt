package ru.medyannikov.newsapi.news_source.domain.params

import ru.medyannikov.newsapi.utils.params.putNotNull

data class SourceParams(val country: String? = null,
                        val category: String? = null,
                        val language: String? = null) {

    fun toMap(): Map<String, String> {
        return mutableMapOf<String, String>().apply {
            putNotNull("country", country)
            putNotNull("category", category)
            putNotNull("language", language)
        }
    }
}