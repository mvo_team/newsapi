package ru.medyannikov.newsapi.news_source.ui.everything

import ru.medyannikov.newsapi.core.domain.model.SourceModel
import ru.medyannikov.newsapi.di.injector.Injectable
import ru.medyannikov.newsapi.news_source.ui.common.SourceSelectorFragment

class EverythingSourceFragment: SourceSelectorFragment<EverythingSourceViewModel>(), Injectable {

    override fun getViewModel(): EverythingSourceViewModel = provideVm()

    companion object {

        fun newInstance(preSelectedItems: List<SourceModel>) = EverythingSourceFragment().apply {
            arguments = getArguments(preSelectedItems)
        }
    }
}