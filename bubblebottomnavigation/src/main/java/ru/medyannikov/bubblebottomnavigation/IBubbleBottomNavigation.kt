package ru.medyannikov.bubblebottomnavigation

import android.graphics.Typeface

import ru.medyannikov.bubblebottomnavigation.listener.BubbleBottomNavigationListener

interface IBubbleBottomNavigation {

    fun getCurrentActiveItemPosition(): Int
    fun setNavigationChangeListener(navigationListener: BubbleBottomNavigationListener)
    fun setTypeface(typeface: Typeface)
    fun setCurrentActiveItem(position: Int)
}
