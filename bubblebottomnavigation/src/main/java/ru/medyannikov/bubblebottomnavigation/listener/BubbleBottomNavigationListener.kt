package ru.medyannikov.bubblebottomnavigation.listener

import android.view.View

interface BubbleBottomNavigationListener {
    fun onNavigationChanged(view: View, position: Int)
}
