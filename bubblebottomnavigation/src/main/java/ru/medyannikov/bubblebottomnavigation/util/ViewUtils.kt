package ru.medyannikov.bubblebottomnavigation.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.TypedValue
import ru.medyannikov.bubblebottomnavigation.R

object ViewUtils {

    fun getThemeAccentColor(context: Context): Int {
        val value = TypedValue().apply {
            context.theme.resolveAttribute(R.attr.colorAccent, this, true)
        }
        return value.data
    }

    fun updateDrawableColor(drawable: Drawable?, color: Int) {
        if (drawable == null) return
        drawable.setTint(color)
    }
}
