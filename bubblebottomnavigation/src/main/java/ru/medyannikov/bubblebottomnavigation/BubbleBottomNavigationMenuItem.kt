package ru.medyannikov.bubblebottomnavigation

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt

data class BubbleBottomNavigationMenuItem(val icon: Drawable? = null,
                                          val iconActive: Drawable? = null,
                                          val shape: Drawable? = null,
                                          val title: String? = "",
                                          @ColorInt val colorActive: Int = Color.BLUE,
                                          @ColorInt val colorInactive: Int = Color.BLACK,
                                          val shapeColor: Int = Integer.MIN_VALUE,
                                          val titleSize: Float = 0.toFloat(),
                                          val iconWidth: Float = 0.toFloat(),
                                          val iconHeight: Float = 0.toFloat(),
                                          val titlePadding: Int = 0,
                                          val internalPadding: Int = 0,
                                          val font: Typeface = Typeface.DEFAULT)
