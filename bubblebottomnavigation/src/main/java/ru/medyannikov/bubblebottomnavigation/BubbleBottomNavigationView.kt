package ru.medyannikov.bubblebottomnavigation

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import ru.medyannikov.bubblebottomnavigation.listener.BubbleBottomNavigationListener
import java.util.*

class BubbleBottomNavigationView : LinearLayout, View.OnClickListener, IBubbleBottomNavigation {

    private var navItems: ArrayList<BubbleBottomNavigationMenuItemView> = arrayListOf()
    private var navigationListener: BubbleBottomNavigationListener? = null

    private var currentActiveItemPosition = 0
    private var loadPreviousState: Boolean = false

    /**
     * Constructors
     */
    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable("superState", super.onSaveInstanceState())
        bundle.putInt("current_item", currentActiveItemPosition)
        bundle.putBoolean("load_prev_state", true)
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        var newState = state
        if (newState is Bundle) {
            val bundle = newState as Bundle?
            currentActiveItemPosition = bundle!!.getInt("current_item")
            loadPreviousState = bundle.getBoolean("load_prev_state")
            newState = bundle.getParcelable("superState")
        }
        super.onRestoreInstanceState(newState)
    }

    /////////////////////////////////////////
    // PRIVATE METHODS
    /////////////////////////////////////////

    /**
     * Initialize
     *
     * @param context current context
     * @param attrs   custom attributes
     */
    private fun init(context: Context, attrs: AttributeSet?) {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER

        post { updateChildNavItems() }
    }

    /**
     * Finds Child Elements of type [BubbleBottomNavigationMenuItemView] and adds them to [.navItems]
     */
    private fun updateChildNavItems() {
        navItems = ArrayList()
        (0 until childCount)
                .map { getChildAt(it) }
                .forEach {
                    if (it is BubbleBottomNavigationMenuItemView)
                        navItems.add(it)
                    else {
                        Log.w(TAG, "Cannot have child navItems other than BubbleBottomNavigationMenuItemView")
                        return
                    }
                }

        if (navItems.size < MIN_ITEMS) {
            Log.w(TAG, "The navItems list should have at least 2 navItems of BubbleBottomNavigationMenuItemView")
        } else if (navItems.size > MAX_ITEMS) {
            Log.w(TAG, "The navItems list should not have more than 5 navItems of BubbleBottomNavigationMenuItemView")
        }

        setClickListenerForItems()
        setInitialActiveState()
        updateMeasurementForItems()
    }

    /**
     * Makes sure that ONLY ONE child [.navItems] is active
     */
    private fun setInitialActiveState() {

        var foundActiveElement = false

        // find the initial state
        when {
            !loadPreviousState -> {
                navItems.indices.forEach { i ->
                    if (navItems[i].isActive && !foundActiveElement) {
                        foundActiveElement = true
                        currentActiveItemPosition = i
                    } else {
                        navItems[i].setInitialState(false)
                    }
                }
            }
            else -> navItems.indices.forEach { navItems[it].setInitialState(false) }
        }
        //set the active element
        if (!foundActiveElement) {
            navItems[currentActiveItemPosition].setInitialState(true)
        }
    }

    /**
     * Update the measurements of the child components [.navItems]
     */
    private fun updateMeasurementForItems() {
        val numChildElements = navItems.size
        if (numChildElements > 0) {
            val calculatedEachItemWidth = (measuredWidth - (paddingRight + paddingLeft)) / numChildElements
            navItems.forEach { it.updateMeasurements(calculatedEachItemWidth) }
        }
    }

    /**
     * Sets [OnClickListener] for the child views
     */
    private fun setClickListenerForItems() {
        navItems.forEach { it.setOnClickListener(this) }
    }

    /**
     * Gets the Position of the Child from [.navItems] from its id
     *
     * @param id of view to be searched
     * @return position of the Item
     */
    private fun getItemPositionById(id: Int): Int {
        return navItems.indices.firstOrNull { id == navItems[it].id } ?: -1
    }

    ///////////////////////////////////////////
    // PUBLIC METHODS
    ///////////////////////////////////////////

    fun getMenuItems(): List<BubbleBottomNavigationMenuItemView> = navItems

    /**
     * Set the navigation change listener [BubbleBottomNavigationListener]
     *
     * @param navigationListener sets the passed parameters as listener
     */
    override fun setNavigationChangeListener(navigationListener: BubbleBottomNavigationListener) {
        this.navigationListener = navigationListener
    }

    /**
     * Set the [Typeface] for the Text Elements of the View
     *
     * @param typeface to be used
     */
    override fun setTypeface(typeface: Typeface) {
        navItems.forEach { it.setTitleTypeface(typeface) }
    }

    /**
     * Gets the current active position
     *
     * @return active item position
     */
    override fun getCurrentActiveItemPosition(): Int {
        return currentActiveItemPosition
    }

    /**
     * Sets the current active item
     *
     * @param position current position change
     */
    override fun setCurrentActiveItem(position: Int) {

        if (currentActiveItemPosition == position) return

        if (position < 0 || position >= navItems.size)
            return

        val btv = navItems[position]
        btv.performClick()
    }

    override fun onClick(v: View) {
        val changedPosition = getItemPositionById(v.id)
        if (changedPosition >= 0 && changedPosition != currentActiveItemPosition) {
            selectItemByPosition(changedPosition)

            navigationListener?.onNavigationChanged(v, currentActiveItemPosition)
        }
    }

    private fun selectItemByPosition(changedPosition: Int) {
        if (changedPosition == currentActiveItemPosition) {
            val item = navItems.getOrNull(changedPosition)
            item?.activate()
            return
        }
        val currentActiveToggleView = navItems.getOrNull(currentActiveItemPosition)
        val newActiveToggleView = navItems.getOrNull(changedPosition)
        currentActiveToggleView?.toggle()
        newActiveToggleView?.toggle()

        //changed the current active position
        if (changedPosition != -1) {
            currentActiveItemPosition = changedPosition
        }
    }

    fun selectItemById(menuItemId: Int) {
        val position = getItemPositionById(menuItemId)
        selectItemByPosition(position)
    }

    companion object {

        //constants
        private const val TAG = "BNLView"
        private const val MIN_ITEMS = 2
        private const val MAX_ITEMS = 5
    }
}
