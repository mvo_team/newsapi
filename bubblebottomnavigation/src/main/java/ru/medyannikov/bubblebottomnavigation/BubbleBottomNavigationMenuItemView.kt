package ru.medyannikov.bubblebottomnavigation

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.TransitionDrawable
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.ViewCompat
import ru.medyannikov.bubblebottomnavigation.util.ViewUtils

class BubbleBottomNavigationMenuItemView : RelativeLayout {

    private lateinit var menuItem: BubbleBottomNavigationMenuItem

    /**
     * Get the current state of the view
     *
     * @return the current state
     */
    var isActive = false
        private set

    private lateinit var iconView: ImageView
    private lateinit var titleView: TextView

    private var animationDuration: Int = 0
    private var showShapeAlways: Boolean = false

    private var maxTitleWidth: Float = 0.toFloat()
    private var measuredTitleWidth: Float = 0.toFloat()

    /**
     * Constructors
     */
    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    /////////////////////////////////////
    // PRIVATE METHODS
    /////////////////////////////////////

    /**
     * Initialize
     *
     * @param context current context
     * @param attrs   custom attributes
     */
    private fun init(context: Context, attrs: AttributeSet?) {
        //initialize default component
        var title: String? = "Title"
        var icon: Drawable? = null
        var iconActive: Drawable? = null
        var shape: Drawable? = null
        var shapeColor = Integer.MIN_VALUE
        var colorActive = ViewUtils.getThemeAccentColor(context)
        var colorInactive = ContextCompat.getColor(context, R.color.default_inactive_color)
        var titleSize = context.resources.getDimension(R.dimen.default_nav_item_text_size)
        maxTitleWidth = context.resources.getDimension(R.dimen.default_nav_item_title_max_width)
        var iconWidth = context.resources.getDimension(R.dimen.default_icon_size)
        var iconHeight = context.resources.getDimension(R.dimen.default_icon_size)
        var internalPadding = context.resources.getDimension(R.dimen.default_nav_item_padding).toInt()
        var titlePadding = context.resources.getDimension(R.dimen.default_nav_item_text_padding).toInt()
        var defaultFont = Typeface.DEFAULT

        if (attrs != null) {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.BubbleBottomNavigation, 0, 0)
            try {
                icon = ta.getDrawable(R.styleable.BubbleBottomNavigation_bt_icon)
                iconActive = ta.getDrawable(R.styleable.BubbleBottomNavigation_bt_icon_active)
                iconWidth = ta.getDimension(R.styleable.BubbleBottomNavigation_bt_iconWidth, iconWidth)
                iconHeight = ta.getDimension(R.styleable.BubbleBottomNavigation_bt_iconHeight, iconHeight)
                shape = ta.getDrawable(R.styleable.BubbleBottomNavigation_bt_shape)
                shapeColor = ta.getColor(R.styleable.BubbleBottomNavigation_bt_shapeColor, shapeColor)
                showShapeAlways = ta.getBoolean(R.styleable.BubbleBottomNavigation_bt_showShapeAlways, false)
                title = ta.getString(R.styleable.BubbleBottomNavigation_bt_title)
                titleSize = ta.getDimension(R.styleable.BubbleBottomNavigation_bt_titleSize, titleSize)
                colorActive = ta.getColor(R.styleable.BubbleBottomNavigation_bt_colorActive, colorActive)
                colorInactive = ta.getColor(R.styleable.BubbleBottomNavigation_bt_colorInactive, colorInactive)
                isActive = ta.getBoolean(R.styleable.BubbleBottomNavigation_bt_active, false)
                animationDuration = ta.getInteger(R.styleable.BubbleBottomNavigation_bt_duration, DEFAULT_ANIM_DURATION)
                val fontResource = ta.getResourceId(R.styleable.BubbleBottomNavigation_bt_font, -1)
                if (fontResource != -1) {
                    defaultFont = ResourcesCompat.getFont(context, fontResource)
                }
                internalPadding = ta.getDimension(
                    R.styleable.BubbleBottomNavigation_bt_padding,
                    internalPadding.toFloat()
                )
                    .toInt()
                titlePadding = ta.getDimension(
                    R.styleable.BubbleBottomNavigation_bt_titlePadding,
                    titlePadding.toFloat()
                )
                    .toInt()
            } finally {
                ta.recycle()
            }
        }

        //set the default icon
        if (icon == null) {
            icon = ContextCompat.getDrawable(context, R.drawable.ic_default_icon)
        }
        if (iconActive == null) {
            iconActive = icon
        }

        //set the default shape
        if (shape == null) {
            shape = ContextCompat.getDrawable(context, R.drawable.transition_background_drawable)
        }

        //create a default item
        menuItem = BubbleBottomNavigationMenuItem(
            icon = icon,
            iconActive = iconActive,
            shape = shape,
            title = title,
            titleSize = titleSize,
            titlePadding = titlePadding,
            shapeColor = shapeColor,
            colorActive = colorActive,
            colorInactive = colorInactive,
            iconWidth = iconWidth,
            iconHeight = iconHeight,
            internalPadding = internalPadding,
            font = defaultFont
        )

        //set the gravity
        gravity = Gravity.CENTER

        //set the internal padding
        val padding = menuItem.internalPadding
        post { setPadding(padding, padding, padding, padding) }

        createMenuItemView(context)
        setInitialState(isActive)
    }

    /**
     * Create the components of the item view [.iconView] and [.titleView]
     *
     * @param context current context
     */
    private fun createMenuItemView(context: Context) {
        iconView = generateImageView(context)
        titleView = generateTextView(context)
        addView(iconView)
        addView(titleView)

        //set the initial state
        setInitialState(isActive)
    }

    private fun generateTextView(context: Context): TextView {
        return TextView(context).apply {
            val lpTitle = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
            )
            lpTitle.addRule(CENTER_VERTICAL, TRUE)
            lpTitle.addRule(END_OF, iconView.id)
            this.layoutParams = lpTitle
            this.setSingleLine(true)
            this.setTextColor(menuItem.colorActive)
            this.text = menuItem.title
            this.typeface = menuItem.font
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, menuItem.titleSize)
            //get the current measured title width
            this.visibility = View.VISIBLE
            //update the margin of the text view
            this.setPadding(menuItem.titlePadding, 0, menuItem.titlePadding, 0)
            //measure the content width
            this.measure(0, 0)       //must call measure!
            measuredTitleWidth = this.measuredWidth.toFloat()  //get width
            //limit measured width, based on the max width
            if (measuredTitleWidth > maxTitleWidth)
                measuredTitleWidth = maxTitleWidth

            //change the visibility
            this.visibility = View.GONE
        }
    }

    private fun generateImageView(context: Context): ImageView {
        return ImageView(context).apply {
            id = ViewCompat.generateViewId()
            val lpIcon = LayoutParams(
                menuItem.iconWidth.toInt(),
                menuItem.iconHeight.toInt()
            )
            lpIcon.addRule(CENTER_VERTICAL, TRUE)
            layoutParams = lpIcon
            setImageDrawable(menuItem.icon)
        }
    }

    /////////////////////////////////
    // PUBLIC METHODS
    ////////////////////////////////

    /**
     * Updates the Initial State
     *
     * @param isActive current state
     */
    fun setInitialState(isActive: Boolean) {
        //set the background
        background = menuItem.shape

        if (isActive) {
            iconView.setImageDrawable(menuItem.iconActive)
            this.isActive = true
            titleView.visibility = View.VISIBLE
            if (background is TransitionDrawable) {
                val trans = background as TransitionDrawable
                trans.startTransition(0)
            } else {
                if (!showShapeAlways && menuItem.shapeColor != Integer.MIN_VALUE)
                    ViewUtils.updateDrawableColor(menuItem.shape, menuItem.shapeColor)
            }
        } else {
            iconView.setImageDrawable(menuItem.icon)
            this.isActive = false
            titleView.visibility = View.GONE
            if (!showShapeAlways) {
                if (background !is TransitionDrawable) {
                    background = null
                } else {
                    val trans = background as TransitionDrawable
                    trans.resetTransition()
                }
            }
        }
    }

    /**
     * Toggles between Active and Inactive state
     */
    fun toggle() {
        if (!isActive)
            activate(true)
        else
            deactivate(true)
    }

    /**
     * Set Active state
     */
    fun activate(withAnimation: Boolean = false) {
        iconView.setImageDrawable(menuItem.iconActive)
        isActive = true
        titleView.visibility = View.VISIBLE

        if (withAnimation) {
            val animator = ValueAnimator.ofFloat(0f, 1f).apply {
                duration = animationDuration.toLong()
                addUpdateListener { animation ->
                    val value = animation.animatedValue as Float
                    titleView.width = (measuredTitleWidth * value).toInt()
                    //end of animation
                    if (value >= 1.0f) {
                        setVisibleTitle()
                        //do something
                    }
                }
            }

            animator.start()
            setBackgroundItem()

        } else {
            setVisibleTitle()
            setBackgroundItem()
        }
    }

    private fun setBackgroundItem() {
        if (background is TransitionDrawable) {
            val trans = background as TransitionDrawable
            trans.startTransition(animationDuration)
        } else {

            //if not showing Shape Always and valid shape color present, use that as tint
            if (!showShapeAlways && menuItem.shapeColor != Integer.MIN_VALUE) {
                ViewUtils.updateDrawableColor(menuItem.shape, menuItem.shapeColor)
            }
            background = menuItem.shape
        }
    }

    private fun setVisibleTitle() {
        titleView.ellipsize = TextUtils.TruncateAt.END
        titleView.width = measuredTitleWidth.toInt()
        titleView.visibility = View.VISIBLE
    }

    /**
     * Set Inactive State
     */
    fun deactivate(withAnimation: Boolean = false) {
        iconView.setImageDrawable(menuItem.icon)
        isActive = false
        titleView.ellipsize = null
        val animator = ValueAnimator.ofFloat(1f, 0f).apply {
            duration = animationDuration.toLong()
            addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                titleView.width = (measuredTitleWidth * value).toInt()
                //end of animation
                if (value <= 0.0f)
                    titleView.visibility = View.GONE
            }
        }
        animator.start()

        if (background is TransitionDrawable) {
            val trans = background as TransitionDrawable
            trans.reverseTransition(animationDuration)
        } else {
            if (!showShapeAlways) background = null
        }
    }

    /**
     * Sets the [Typeface] of the [.titleView]
     *
     * @param typeface to be used
     */
    fun setTitleTypeface(typeface: Typeface) {
        titleView.typeface = typeface
    }

    /**
     * Updates the measurements and fits the view
     *
     * @param maxWidth in pixels
     */
    fun updateMeasurements(maxWidth: Int) {
        var marginLeft = 0
        var marginRight = 0
        val titleViewLayoutParams = titleView.layoutParams
        if (titleViewLayoutParams is LayoutParams) {
            marginLeft = titleViewLayoutParams.rightMargin
            marginRight = titleViewLayoutParams.leftMargin
        }

        val newTitleWidth = ((maxWidth
                - (paddingRight + paddingLeft)
                - (marginLeft + marginRight)
                - menuItem.iconWidth.toInt())
                + titleView.paddingRight + titleView.paddingLeft)

        //if the new calculate title width is less than current one, update the titleView specs
        if (newTitleWidth > 0 && newTitleWidth < measuredTitleWidth) {
            measuredTitleWidth = titleView.measuredWidth.toFloat()
        }
    }

    companion object {

        private val TAG = "BNI_View"
        private const val DEFAULT_ANIM_DURATION = 300
    }
}
